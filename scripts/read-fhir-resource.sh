#!/bin/bash

#set -o errexit # exit script when command fails
#set -o pipefail # exit status of the last command that threw a non-zero exit code is returned
#set -o nounset # exit when script tries to use undeclared variables

function usage()
{
    echo "Read data from a FHIR service"
    echo
    echo "Usage:"
    echo "read-fhir-resource.sh [-a|--address <service-address>] [--auth-server-url <auth-server-url>] [-h|help] [-k|-keycloak] [--keycloak-client-name <keycloak-client-name>] [--keycloak-realm <keycloak-realm>] [-p|-password] [-r|--repeat-request <number-of-times>] [-u|--user <username>]"
    echo
}

REPEAT=1 #default to executing request once

# Read commandline arguments:
while :; do
    case $1 in
        -a|--address)
            if [ -n "$2" ]; then
                ADDRESS="$2"
                shift
            fi
            ;;
        --auth-server-url)
            if [ -n "$2" ]; then
                AUTH_SERVER_URL="$2"
                shift
            fi
            ;;
        -h|--help)
            usage
            exit 1
            ;;
        -k|--keycloak)
            ENABLEKEYCLOAK="true"
            ;;
        --keycloak-client-name)
            if [ -n "$2" ]; then
                KEYCLOAK_ClIENT_NAME="$2"
                shift
            fi
            ;;
        --keycloak-realm)
            if [ -n "$2" ]; then
                KEYCLOAK_REALM="$2"
                shift
            fi
            ;;
        -p|--password)
            if [ -n "$2" ]; then
                PASS="$2"
                shift
            fi
            ;;
        -r|--repeat-request)
            if [ -n "$2" ]; then
                REPEAT="$2"
                shift
            fi
            ;;
        -u|--user)
            if [ -n "$2" ]; then
                USER="$2"
                shift
            fi
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: No more options, so break out of the loop.
            break
    esac
    shift # past argument or value
done

echo

# We need an address...
if [ -z $ADDRESS ]; then
    echo ERROR Service address not specified. Please specify using -a option 1>&2
    exit 1
fi

if [ -z $ENABLEKEYCLOAK ]; then
    echo "Not using Keycloak authentication"

    ###################################################################
    # GET data
        echo REPEAT = $REPEAT
    if [ $REPEAT -gt 1 ]; then
        start=`date +%s`
        while [[ $i -le $REPEAT ]]
        do
            echo "$i. call of GET via curl"
            RESULT=$(curl -s -S -X GET --header "Content-Type:application/json" ${ADDRESS}) &
            ((i = i + 1))
        done
        end=`date +%s`
        runtime=$((end-start))
        echo "runtime: $runtime"
    else
        RESULT=$(curl -X GET --header "Content-Type:application/json" ${ADDRESS})
        echo "${RESULT}"
    fi
else
    echo "Using Keycloak authentication"

    # Get settings from service.env file
    if [ -z $AUTH_SERVER_URL ]; then
        AUTH_SERVER_URL_LINE=$(grep  'AUTH_SERVER_URL=' service.env)
        echo AUTH_SERVER_URL_LINE = "${AUTH_SERVER_URL_LINE}"
        AUTH_SERVER_URL=${AUTH_SERVER_URL_LINE:16}
    fi
    echo AUTH_SERVER_URL = "${AUTH_SERVER_URL}"
    echo

    if [ -z $KEYCLOAK_ClIENT_NAME ]; then
        KEYCLOAK_ClIENT_NAME_LINE=$(grep 'KEYCLOAK_CLIENT_NAME' service.env)
        echo KEYCLOAK_ClIENT_NAME_LINE = "${KEYCLOAK_ClIENT_NAME_LINE}"
        KEYCLOAK_ClIENT_NAME=${KEYCLOAK_ClIENT_NAME_LINE:21}
    fi
    echo KEYCLOAK_ClIENT_NAME = "${KEYCLOAK_ClIENT_NAME}"

    if [ -z $KEYCLOAK_REALM ]; then
        KEYCLOAK_REALM_LINE=$(grep 'KEYCLOAK_REALM' service.env)
        echo KEYCLOAK_REALM_LINE = "${KEYCLOAK_REALM_LINE}"
        KEYCLOAK_REALM=${KEYCLOAK_REALM_LINE:15}
    fi
    echo KEYCLOAK_REALM = "${KEYCLOAK_REALM}"

    # Get access token from Keycloak
    OIDRESULT=$(curl --data "grant_type=password&client_id=${KEYCLOAK_ClIENT_NAME}&username=${USER}&password=${PASS}" ${AUTH_SERVER_URL}realms/${KEYCLOAK_REALM}/protocol/openid-connect/token)
    echo OIDRESULT = "${OIDRESULT}"
    TOKEN=$(echo ${OIDRESULT} | sed 's/.*access_token":"//g' | sed 's/".*//g')
    echo TOKEN = $TOKEN

    ###################################################################
    # GET data

    echo REPEAT = $REPEAT
    if [ $REPEAT -gt 1 ]; then
        start=`date +%s`
        while [[ $i -le $REPEAT ]]
        do
            echo "$i. call of GET via curl"
            RESULT=$(curl -s -S -X GET -H "Authorization: Bearer $TOKEN" --header "Content-Type:application/json" ${ADDRESS}) &
            ((i = i + 1))
        done
        end=`date +%s`
        runtime=$((end-start))
        echo "runtime: $runtime"
    else
        RESULT=$(curl -X GET -H "Authorization: Bearer $TOKEN" --header "Content-Type:application/json" ${ADDRESS})
        echo "${RESULT}"
    fi
fi