#!/bin/bash

set -o errexit # exit script when command fails
set -o pipefail # exit status of the last command that threw a non-zero exit code is returned
#set -o nounset # exit when script tries to use undeclared variables

function usage()
{
    echo "Test if file is newer than a specified number of seconds"
    echo
    echo "Usage:"
    echo "health.sh [-f|--filename <filname>] [-t|--time-seconds <time-in-seconds>] [-h|help]"
    echo
}

# Read commandline arguments:
while :; do
    case $1 in
        -h|--help)
            usage
            exit 1
            ;;
        -f|--filename)
            if [ -n "$2" ]; then
            filename="$2"
                shift
            fi
            ;;
        -t|--time-seconds)
            if [ -n "$2" ]; then
                timeseconds="$2"
                shift
            fi
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: No more options, so break out of the loop.
            break
    esac
    shift # past argument or value
done

if [[ $(find "$filename" -mtime -"$timeseconds" -print) ]]; then
  echo "file is newer than $timeseconds seconds"
  exit 0
else
  echo "file is older than $timeseconds seconds"
  exit 1
fi
