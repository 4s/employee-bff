package dk.s4.microservices.employeebff.servlet;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.StrictErrorHandler;
import ca.uhn.fhir.rest.api.EncodingEnum;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.RestfulServer;
import ca.uhn.fhir.rest.server.interceptor.CorsInterceptor;
import ca.uhn.fhir.rest.server.interceptor.LoggingInterceptor;
import ca.uhn.fhir.rest.server.interceptor.ResponseHighlighterInterceptor;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.employeebff.auditlog.DiasAuditLogInterceptorAdaptor;
import dk.s4.microservices.employeebff.auditlog.DiasSender;
import dk.s4.microservices.employeebff.health.HealthEndpoint;
import dk.s4.microservices.employeebff.provider.*;
import dk.s4.microservices.employeebff.provider.stub.*;
import dk.s4.microservices.employeebff.security.MyAuthorizationInterceptor;
import dk.s4.microservices.employeebff.utils.DescendantCache;
import dk.s4.microservices.employeebff.utils.OrganizationQueryParameter;
import dk.s4.microservices.messaging.MessagingInitializationException;
import dk.s4.microservices.messaging.ReplyEventProcessor;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.kafka.KafkaConsumeAndProcess;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import dk.s4.microservices.microservicecommon.FhirTopics;
import dk.s4.microservices.microservicecommon.fhir.DiasInterceptorAdaptor;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.fhir.MyInterceptorAdaptor;
import dk.s4.microservices.microservicecommon.security.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.cors.CorsConfiguration;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The WebUI Clinician BFF is a "Backend-for-Frontend" service. This service is a
 * FHIR RESTful server without a database.
 * 
 * This services collects and handles the business logic for the employees (i.e. the healthcare professionals) using a
 * web UI.
 *
 */
public class EmployeeBff extends RestfulServer {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(EmployeeBff.class);
	protected LoggingInterceptor loggingInterceptor;

    private static FhirContext fhirContext;

	public static String keycloakClientName = null;

    public static List<Topic> replyTopics;

	private UserContextResolverInterface contextResolver;
	private KafkaEventProducer kafkaCommandProducer;

	/**
	 * Constructor
	 */
	public EmployeeBff() throws KafkaInitializationException, MessagingInitializationException {
		super(getMyFhirContext());
		registerAndCheckEnvironmentVars();
		if (ServiceVariables.ENABLE_DIAS_AUTHENTICATION.isSetToTrue()) {
			contextResolver = new DiasUserContextResolver(ServiceVariables.USER_CONTEXT_SERVICE_URL.getValue());
		} else if (ServiceVariables.ENABLE_KEYCLOAK_GATEKEEPER_AUTHORIZATION.isSetToTrue()) {
			contextResolver = new KeycloakGatekeeperUserContextResolver();
		} else if (ServiceVariables.ENABLE_OAUTH2_PROXY_AUTHORIZATION.isSetToTrue()) {
			contextResolver = new OAuth2ProxyUserContextResolver();
		} else {
			contextResolver = new DefaultUserContextResolver();
		}

		if (ServiceVariables.ENABLE_KAFKA.isSetToTrue())
			initKafka();
	}

	/**
	 * Singleton FhirContext
	 *
	 * @return the FhirContext
	 */
	public static FhirContext getMyFhirContext() {
		if (fhirContext == null) {
			fhirContext = FhirContext.forR4();
			fhirContext.setParserErrorHandler(new StrictErrorHandler());
			int timeout =  Integer.parseInt(ServiceVariables.CLIENT_SOCKET_TIMEOUT_MILLIS.getValue());
			fhirContext.getRestfulClientFactory().setSocketTimeout(timeout);
		}
		return fhirContext;
	}

	public static ReplyEventProcessor replyEventProcessor;
	private KafkaConsumeAndProcess kafkaConsumeAndProcess;
	private Thread kafkaConsumeAndProcessThread;

	@Override
	public void destroy() {
		System.out.println("Shutting down employee-BFF");
		kafkaConsumeAndProcess.stopThread();
		try {
			kafkaConsumeAndProcessThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void registerAndCheckEnvironmentVars() {
		ServiceVariables.registerAndEnsurePresence();
	}

	private void initKafka() throws KafkaInitializationException, MessagingInitializationException {
		//Create producer for asynchonous invocations of create and update commands
		kafkaCommandProducer = new KafkaEventProducer(ServiceVariables.SERVICE_NAME.getValue());
		if (ServiceVariables.ENABLE_DIAS_AUTHENTICATION.isSetToTrue())
			kafkaCommandProducer.setInterceptors(new DiasEventProducerInterceptor());

		replyTopics = new ArrayList<>();
		replyTopics.add(FhirTopics.dataCreated("Patient"));
		replyTopics.add(FhirTopics.processingFailed("Patient"));
		replyTopics.add(FhirTopics.dataUpdated("Patient"));

		replyTopics.add(FhirTopics.dataCreated("Questionnaire"));
		replyTopics.add(FhirTopics.dataUpdated("Questionnaire"));
		replyTopics.add(FhirTopics.processingFailed("Questionnaire"));

		replyTopics.add(FhirTopics.dataCreated("Task"));
		replyTopics.add(FhirTopics.dataUpdated("Task"));
		replyTopics.add(FhirTopics.processingFailed("Task"));

		replyTopics.add(FhirTopics.dataCreated("CarePlan"));
		replyTopics.add(FhirTopics.dataUpdated("CarePlan"));
		replyTopics.add(FhirTopics.processingFailed("CarePlan"));

		replyTopics.add(FhirTopics.dataCreated("PlanDefinition"));
		replyTopics.add(FhirTopics.dataUpdated("PlanDefinition"));
		replyTopics.add(FhirTopics.processingFailed("PlanDefinition"));

		replyTopics.add(FhirTopics.dataCreated("ObservationDefinition"));
		replyTopics.add(FhirTopics.dataUpdated("ObservationDefinition"));
		replyTopics.add(FhirTopics.processingFailed("ObservationDefinition"));

		replyEventProcessor = new ReplyEventProcessor();
		kafkaConsumeAndProcess = new KafkaConsumeAndProcess(replyTopics,null, replyEventProcessor);
		kafkaConsumeAndProcessThread = new Thread(kafkaConsumeAndProcess);
		kafkaConsumeAndProcessThread.start();
		HealthEndpoint.registerKafkaConsumeAndProcess(kafkaConsumeAndProcess);
	}

	private void registerInterceptors(UserContextResolverInterface contextResolver) {
		/*
		 * Enable CORS
		 */
		CorsConfiguration config = new CorsConfiguration();
		CorsInterceptor corsInterceptor = new CorsInterceptor(config);
		config.addAllowedHeader("Origin");
		config.addAllowedHeader("Accept");
		config.addAllowedHeader("Prefer");
		config.addAllowedHeader("X-Requested-With");
		config.addAllowedHeader("Content-Type");
		config.addAllowedHeader("Access-Control-Request-Method");
		config.addAllowedHeader("Access-Control-Request-Headers");
		config.addAllowedHeader("Authorization");
		config.addAllowedHeader("SESSION");
		config.addAllowedOrigin("*");
		config.addExposedHeader("Location");
		config.addExposedHeader("Content-Location");
		config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
		registerInterceptor(corsInterceptor);

		// Register adapter that handles metrics, correlation-ids and authentication
		MyInterceptorAdaptor myInterceptorAdaptor = new MyInterceptorAdaptor(contextResolver);
		registerInterceptor(myInterceptorAdaptor);

		if (ServiceVariables.ENABLE_DIAS_AUTHENTICATION.isSetToTrue()) {
			DiasInterceptorAdaptor diasInterceptorAdaptor = new DiasInterceptorAdaptor(contextResolver);
			registerInterceptor(diasInterceptorAdaptor);
		}

		if (ServiceVariables.ENABLE_AUTH.isSetToTrue()) {
			logger.debug("Initializing MyAuthorizationInterceptor");
			// Register the authorization interceptor
			MyAuthorizationInterceptor authzInterceptor = new MyAuthorizationInterceptor();
			this.registerInterceptor(authzInterceptor);
		}

		// Register HAPI FHIRs built in logging interceptor
		loggingInterceptor = new LoggingInterceptor();
		registerInterceptor(loggingInterceptor);
		loggingInterceptor.setMessageFormat("operationType: ${operationType}\n"
													+ "operationName: ${operationName}\n"
													+ "idOrResourceName: ${idOrResourceName}\n"
													+ "requestParameters: ${requestParameters}\n"
													+ "requestUrl: ${requestUrl}\n"
													+ "requestVerb: ${requestVerb}\n"
													+ "processingTimeMillis: ${processingTimeMillis}"
										   );
		loggingInterceptor.setErrorMessageFormat("ERROR: ${exceptionMessage}\n"
														 + "operationType: ${operationType}\n"
														 + "operationName: ${operationName}\n"
														 + "idOrResourceName: ${idOrResourceName}\n"
														 + "requestParameters: ${requestParameters}\n"
														 + "requestUrl: ${requestUrl}\n"
														 + "requestVerb: ${requestVerb}\n"
														 + "processingTimeMillis: ${processingTimeMillis}"										   );
		loggingInterceptor.setLogger(logger);

		//This interceptor adds some pretty syntax highlighting in responses when a browser is detected
		//Important that this highlighter is added first, and thus invoked last, as it's outgoingReponse method
		//may return false if it doesn't detect browser properly - and it could thus prevent other interceptors'
		//outgoingResponse methods from being called!
		registerInterceptor(new ResponseHighlighterInterceptor());

		if (ServiceVariables.DIAS_AUDIT_ENABLED.isSetToTrue()){
			registerInterceptor(new DiasAuditLogInterceptorAdaptor(contextResolver,
					new DiasSender(ServiceVariables.DIAS_AUDIT_URL.getValue(), ServiceVariables.SERVICE_NAME.getValue())));
		}

	}

	private void initResourceProviders() {

		List<IResourceProvider> providers = new ArrayList<>();
		/*
		 * Add resource providers
		 */
		if (!ServiceVariables.ENABLE_STUBS.isSetToTrue()) {
			MyFhirClientFactory fhirClientFactory = MyFhirClientFactory.getInstance();
			FhirContext fhirContext = getMyFhirContext();
			DescendantCache cache = new DescendantCache(fhirContext, ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue(),
					Integer.parseInt(ServiceVariables.CACHE_SIZE_LIMIT.getValue()), Integer.parseInt(ServiceVariables.DESCENDANT_CACHE_TIMEOUT_MINUTES.getValue()));
			OrganizationQueryParameter parameters = new OrganizationQueryParameter(fhirContext,cache);
			providers.add(new FHIRCarePlanResourceProvider(fhirContext,
					contextResolver,
					fhirClientFactory,
					kafkaCommandProducer,
					replyEventProcessor,
					parameters));
			providers.add(new FHIRObservationResourceProvider(fhirContext,
					contextResolver,
					fhirClientFactory,
					kafkaCommandProducer,
					replyEventProcessor,
					parameters));
			providers.add(new FHIRObservationDefinitionResourceProvider(fhirContext,
					contextResolver,
					fhirClientFactory,
					kafkaCommandProducer,
					replyEventProcessor));
			providers.add(new FHIRPatientResourceProvider(fhirContext,
					contextResolver,
					fhirClientFactory,
					kafkaCommandProducer,
					replyEventProcessor,
					parameters));
			providers.add(new FHIROrganizationResourceProvider(fhirContext,
					contextResolver,
					fhirClientFactory));
			providers.add(new FHIRQuestionnaireResponseResourceProvider(fhirContext,
					contextResolver,
					fhirClientFactory,
					kafkaCommandProducer,
					replyEventProcessor,
					parameters));
			providers.add(new FHIRTaskResourceProvider(fhirContext,
					contextResolver,
					fhirClientFactory,
					kafkaCommandProducer,
					replyEventProcessor,
					parameters));
			providers.add(new FHIRPlanDefinitionResourceProvider(fhirContext,
					contextResolver,
					fhirClientFactory,
					kafkaCommandProducer,
					replyEventProcessor,
					parameters));
			providers.add(new FHIRQuestionnaireResourceProvider(fhirContext,
					contextResolver,
					fhirClientFactory,
					kafkaCommandProducer,
					replyEventProcessor,
					parameters));
			providers.add(new FHIRPractitionerResourceProvider(fhirContext,
					contextResolver,
					fhirClientFactory));
		}
		else {
			try {
				logger.warn("Initializing service with stub versions of resource providers");
				providers.add(new FHIRCarePlanProviderStub());
				providers.add(new FHIROrganizationResourceProviderStub());
				providers.add(new FHIRObservationResourceProviderStub());
				providers.add(new FHIRPatientResourceProviderStub());
				providers.add(new FHIRPlanDefinitionResourceProviderStub());
				providers.add(new FHIRQuestionnaireResourceProviderStub());
				providers.add(new FHIRQuestionnaireResponseResourceProviderStub());
				providers.add(new FHIRTaskResourceProviderStub());
				providers.add(new FHIRThresholdSetProviderStub());
				providers.add(new FHIRPractitionerResourceProviderStub());
			} catch (KafkaInitializationException | IOException e) {
				logger.error("", e);
				destroy();
			}
		}

		setResourceProviders(providers);
	}

	@Override
	public void initialize() throws ServletException {
		super.initialize();

		logger.debug("initialize");

		keycloakClientName = ServiceVariables.KEYCLOAK_CLIENT_NAME.getValue();

		//Default to JSON and pretty printing
		setDefaultPrettyPrint(true);
		setDefaultResponseEncoding(EncodingEnum.JSON);

		initResourceProviders();
		registerInterceptors(contextResolver);

		logger.info("EmployeeBff initialized");
	}
}
