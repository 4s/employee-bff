package dk.s4.microservices.employeebff.utils;
import ca.uhn.fhir.rest.api.server.RequestDetails;
public class CopiedParameters {
    private final String parameters;

    public static CopiedParameters from(RequestDetails requestDetails) {
        if(requestDetails.getCompleteUrl().contains("?")){
            return new CopiedParameters(requestDetails.getCompleteUrl().substring(requestDetails.getCompleteUrl().indexOf("?")));
        } else {
            return new CopiedParameters("");
        }
    }

    public CopiedParameters(String parameters) {
        this.parameters = parameters;
    }

    public String get() {
        return parameters;
    }
}