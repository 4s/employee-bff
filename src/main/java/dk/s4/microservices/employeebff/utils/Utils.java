package dk.s4.microservices.employeebff.utils;

import ca.uhn.fhir.rest.api.server.RequestDetails;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Utils {
    private static final Logger logger = LoggerFactory.getLogger(Utils.class);

    private static ClassLoader classLoader = Utils.class.getClassLoader();


    public static String readFile(String path, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    public static String createResourceString(String relativePath) throws IOException {
        InputStream filepath = classLoader.getResourceAsStream(relativePath);
        return IOUtils.toString(filepath, StandardCharsets.UTF_8);
    }

    public static String constructUrlFromRequestDetails(String hostname, String resource, RequestDetails requestDetails){
        return hostname + "/" + resource + CopiedParameters.from(requestDetails).get();
    }

    public static String addOrganizationParameters(String searchUrl, String parameterName, HttpServletRequest request, OrganizationQueryParameter parameters, MyFhirClientFactory clientFactory, UserContextResolverInterface userContextResolver) throws UserContextResolverInterface.UserContextResolverException {
        String returnUrl = searchUrl;
        if(searchUrl.contains("?")){
            returnUrl = returnUrl + "&";
        } else {
            returnUrl = returnUrl + "?";
        }
        return returnUrl + parameterName + "=" + parameters.createFamilyFrom(clientFactory,userContextResolver.getFHIRUserOrganization(request));
    }
}
