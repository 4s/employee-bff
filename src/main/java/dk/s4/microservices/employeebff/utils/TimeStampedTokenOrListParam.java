package dk.s4.microservices.employeebff.utils;

import ca.uhn.fhir.rest.param.TokenOrListParam;

import java.time.LocalDateTime;

public class TimeStampedTokenOrListParam {

    private TokenOrListParam tokenOrListParam;
    private LocalDateTime lastUpdated;

    public TimeStampedTokenOrListParam(LocalDateTime lastUpdated, TokenOrListParam tokenOrListParam){
        this.lastUpdated = lastUpdated;
        this.tokenOrListParam = tokenOrListParam;
    }

    public TokenOrListParam getTokenOrListParam() {
        return tokenOrListParam;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

}
