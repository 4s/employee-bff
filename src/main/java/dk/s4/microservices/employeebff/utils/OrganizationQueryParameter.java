package dk.s4.microservices.employeebff.utils;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.param.TokenOrListParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.r4.model.Identifier;

import java.util.List;

public class OrganizationQueryParameter {

    private final FhirContext fhirContext;
    private final DescendantCache cache;

    public OrganizationQueryParameter(FhirContext fhirContext, DescendantCache cache) {
        this.fhirContext = fhirContext;
        this.cache = cache;
    }

    /**
     * Constructs a query string parameter containing all descendants of the organization of currently logged in user, and the organization itself
     * @return a query string
     * @throws UserContextResolverInterface.UserContextResolverException
     */
    public String createFamilyFrom(MyFhirClientFactory clientFactory, Identifier identifier) {
        if (identifier == null || identifier.getValue() == null) {
            throw new InternalErrorException("FHIRUserOrganization identifier was null.");
        }
        TokenOrListParam descendantsOrList = cache.queryDescendantsFor(identifier, clientFactory);

        return convertToString(identifier, descendantsOrList);
    }

    /**
     * Converts an Organization and a TokenOrListParam of its descendants into a query string for a http request to an backend service
     * @param parentIdentifier The identifier of the organization
     * @param descendantsOrList the TokenOrList parameter
     * @return a query string
     */

    protected String convertToString(Identifier parentIdentifier, TokenOrListParam descendantsOrList) {
        StringBuilder builder = new StringBuilder();
        List<TokenParam> params = descendantsOrList.getValuesAsQueryTokens();
        for (TokenParam param : params){
            builder.append(param.getValueAsQueryToken(fhirContext));
            builder.append(",");

        }
        builder.append(getTokenParamFromIdentifier(parentIdentifier).getValueAsQueryToken(fhirContext));
        return builder.toString();
    }

    /**
     * Converts an identifier into a Token parameter
     * @param identifier the identifier
     * @return The resulting Token paramter
     */
    protected TokenParam getTokenParamFromIdentifier(Identifier identifier){
        if(identifier.getSystem() == null){
            identifier.setSystem(ServiceVariables.OFFICIAL_ORGANIZATION_IDENTIFIER_SYSTEM.getValue());
        }
        return new TokenParam().setSystem(identifier.getSystem()).setValue(identifier.getValue());
    }
}
