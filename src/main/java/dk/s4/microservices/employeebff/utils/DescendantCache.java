package dk.s4.microservices.employeebff.utils;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.param.TokenOrListParam;
import ca.uhn.fhir.rest.param.TokenParam;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Organization;
import org.hl7.fhir.r4.model.ResourceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DescendantCache {
    private static final Logger logger = LoggerFactory.getLogger(DescendantCache.class);

    private final FhirContext fhirContext;
    private final ReentrantReadWriteLock lock;
    private final int cacheTimeout;
    private final String clientBaseUrl;
    private LinkedHashMap<Identifier, TimeStampedTokenOrListParam> cache;

    /**
     * Initialize the cache, setting the size limit of the cache and the cache timeout which decides when an entry is outdated,
     * and should be refreshed.
     *
     * @param fhirContext  the fhir context
     * @param cacheLimit   the size limit of the cache
     * @param cacheTimeout the timeout in MINUTES - any entries older that this, will be refreshed.
     */
    public DescendantCache(FhirContext fhirContext, String clientBaseUrl, int cacheLimit, int cacheTimeout) {
        this.cacheTimeout = cacheTimeout;
        this.fhirContext = fhirContext;
        this.clientBaseUrl = clientBaseUrl;
        this.cache = initializeCache(cacheLimit);
        lock = new ReentrantReadWriteLock();
    }

    /**
     * Looks up the given identifier in the cache. If it exists, and is not outdated, it returns the cached value.
     * If it doesn't exist, or if the cache is outdated, it queries the organizational server for the descendants of the given identifier,
     * saves it in the cache, and returns it.
     *
     * @param parent the identifier of the parent organization
     * @return the descendants of the parents, contained in a TokenOrListParam.
     */

    public TokenOrListParam queryDescendantsFor(Identifier parent, MyFhirClientFactory clientFactory) {
        if (shouldPutToCache(parent)) {
            updateCache(parent, constructTokenOrListOfDescendants(parent, clientFactory));
        }
        return readCache(parent).getTokenOrListParam();
    }

    /**
     * Checks whether the cache should be updated.
     *
     * @param identifier The identifier key
     * @return
     */

    private boolean shouldPutToCache(Identifier identifier) {
        TimeStampedTokenOrListParam cachedValue = readCache(identifier);
        if (cachedValue != null) {
            LocalDateTime nowMinusTimeout = LocalDateTime.now().minusMinutes(cacheTimeout);
            return cachedValue.getLastUpdated().isBefore(nowMinusTimeout);
        }
        return true;
    }

    /**
     * Constructs a TokenOrListParam containing any descendants of a given organization
     *
     * @param parent        the identifier of the organization
     * @param clientFactory
     * @return the TokenOrListParam
     */
    protected TokenOrListParam constructTokenOrListOfDescendants(Identifier parent, MyFhirClientFactory clientFactory) {
        TokenOrListParam orListParam = new TokenOrListParam();
        Bundle descendantsBundle = retrieveDescendants(parent, clientFactory);
        for (Bundle.BundleEntryComponent entry : descendantsBundle.getEntry()) {
            addToList(entry, orListParam);
        }
        return orListParam;
    }

    /**
     * Retrieves the descendants of an organization from the Organizational service
     *
     * @param parent        The identifier of the organization
     * @param clientFactory
     * @return a Bundle containing any descendants of the organization
     */
    protected Bundle retrieveDescendants(Identifier parent, MyFhirClientFactory clientFactory) {
        try {
        IGenericClient client = clientFactory.getClientFromBaseUrl(clientBaseUrl, fhirContext);
        String searchUrl = ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue() +
                "/Organization?_query=getDescendants&partOfIdentifier=" +
                getTokenParamFromIdentifier(parent).getValueAsQueryToken(fhirContext);

            Bundle bundle = client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
            return bundle;
        } catch (Exception e) {
            String errMsg = "Error in call to " + ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue() + ":" + e.getMessage();
            logger.error(errMsg, e);
            throw ExceptionUtils.fatalInternalErrorException(errMsg, e);
        }
    }

    /**
     * Adds the identifier of a BundleEntryComponent to the given TokenOrListParam. The BundleEntryComponent should always contain an Organization
     *
     * @param entry       the entry component
     * @param orListParam the OrListParam
     */
    protected void addToList(Bundle.BundleEntryComponent entry, TokenOrListParam orListParam) {
        if (entry.getResource().getResourceType() != ResourceType.Organization) {
            throw new IllegalArgumentException("The entry component of the Bundle was not an Organization");
        }
        Organization organization = (Organization) entry.getResource();
        orListParam.add(organization.getIdentifierFirstRep().getSystem(), organization.getIdentifierFirstRep().getValue());
    }

    /**
     * Converts an identifier into a Token parameter
     *
     * @param identifier the identifier
     * @return The resulting Token paramter
     */
    protected TokenParam getTokenParamFromIdentifier(Identifier identifier) {
        return new TokenParam().setSystem(ServiceVariables.OFFICIAL_ORGANIZATION_IDENTIFIER_SYSTEM.getValue()).setValue(identifier.getValue());
    }

    /**
     * Initialize the the cache. The cacheLimit is given as initial size of the LinkedHashMap. The loadFactor given is the default of HashMap.
     * The accessOrder is set to true, meaning the 'EldestEntry' is not the entry that was first entered, but the entry that was last accessed.
     * removeEldestEntry() is overridden to return true when the size of the cache exceeds the cacheLimit,
     * meaning the EldestEntry will then be removed.
     *
     * @param cacheLimit the cache size limit
     * @return the initialized LinkedHashMap
     */
    protected LinkedHashMap<Identifier, TimeStampedTokenOrListParam> initializeCache(int cacheLimit) {
        return new LinkedHashMap<Identifier, TimeStampedTokenOrListParam>(cacheLimit, 0.75f, true) {
            @Override
            protected boolean removeEldestEntry(Map.Entry<Identifier, TimeStampedTokenOrListParam> eldest) {
                return size() > cacheLimit;
            }
        };
    }

    /**
     * Updates the cached value of the given identifier, with the given value timestamped with the current time.
     *
     * @param identifier the identifier key
     * @param newInput   the new value to cache
     */
    private void updateCache(Identifier identifier, TokenOrListParam newInput) {
        try {
            lock.writeLock().lock();
            cache.put(identifier, new TimeStampedTokenOrListParam(LocalDateTime.now(), newInput));
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Reads the cached value for the given identifier
     *
     * @param identifier
     * @return
     */

    private TimeStampedTokenOrListParam readCache(Identifier identifier) {
        try {
            lock.readLock().lock();
            return cache.get(identifier);
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Used for testing
     *
     * @return cache
     */
    @Deprecated
    protected LinkedHashMap<Identifier, TimeStampedTokenOrListParam> getCache() {
        return cache;
    }
}
