package dk.s4.microservices.employeebff.utils;

import ca.uhn.fhir.model.api.annotation.Child;
import ca.uhn.fhir.model.api.annotation.Description;
import ca.uhn.fhir.model.api.annotation.ResourceDef;
import org.hl7.fhir.r4.model.Basic;
import org.hl7.fhir.r4.model.Configuration;
import org.hl7.fhir.r4.model.DateType;
import org.hl7.fhir.r4.model.ResourceType;

import java.util.Date;

@ResourceDef(
        name = "ThresholdSet",
        profile = "https://confluence.alexandra.dk/display/GD/Resource+ThresholdSet"
)

public class ThresholdSet extends Basic {

    private static final long serialVersionUID = 1L;

    @Child(
            name = "approvalDate",
            type = {DateType.class},
            min = 0,
            max = 1
    )
    @Description(
            shortDefinition = "When the threshold set was approved by practitioner",
            formalDefinition = "The date on which the resource content was approved by the practitioner." +
                    "Approval happens once when the content is officially approved for usage."
    )
    protected DateType approvalDate;

    public DateType getApprovalDateElement() {
        if (this.approvalDate == null) {
            if (Configuration.errorOnAutoCreate()) {
                throw new Error("Attempt to auto-create ThresholdSet.approvalDate");
            }

            if (Configuration.doAutoCreate()) {
                this.approvalDate = new DateType();
            }
        }

        return this.approvalDate;
    }

    public boolean hasApprovalDateElement() {
        return this.approvalDate != null && !this.approvalDate.isEmpty();
    }

    public boolean hasApprovalDate() {
        return this.approvalDate != null && !this.approvalDate.isEmpty();
    }

    public ThresholdSet setApprovalDateElement(DateType value) {
        this.approvalDate = value;
        return this;
    }

    public Date getApprovalDate() {
        return this.approvalDate == null ? null : this.approvalDate.getValue();
    }

    public ThresholdSet setApprovalDate(Date value) {
        if (value == null) {
            this.approvalDate = null;
        } else {
            if (this.approvalDate == null) {
                this.approvalDate = new DateType();
            }

            this.approvalDate.setValue(value);
        }

        return this;
    }

    @Override
    public ThresholdSet copy() {
        ThresholdSet copy = (ThresholdSet) super.copy();
        copy.approvalDate = approvalDate;

        return null;
    }

    @Override
    public ResourceType getResourceType() {
        return ResourceType.Basic;
    }
}
