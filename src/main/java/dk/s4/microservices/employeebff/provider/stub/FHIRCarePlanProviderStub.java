package dk.s4.microservices.employeebff.provider.stub;

import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.param.ReferenceParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import dk.s4.microservices.employeebff.servlet.EmployeeBff;
import dk.s4.microservices.employeebff.utils.Utils;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FHIRCarePlanProviderStub implements IResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(FHIRCarePlanProviderStub.class);

    private List<CarePlan> carePlanList = new ArrayList<>();
    private List<Patient> patientList = new ArrayList<>();

    public FHIRCarePlanProviderStub() throws IOException {
        createStubData();
    }

    private void createStubData() throws IOException {
        CarePlan pd1 = new CarePlan().setTitle("Præeklampsi");
        pd1.setId("Test1");
        String uuid = UUID.randomUUID().toString();
        String identifierStr = "urn:ietf:rfc:3986" + "|" + "urn:uuid:" + uuid;
        pd1.addIdentifier().setSystem("urn:ietf:rfc:3986").setValue("urn:uuid:" + uuid);

        CarePlan pd2 = new CarePlan().setTitle("PPROM");
        pd2.setId("Test2");
        uuid = UUID.randomUUID().toString();
        identifierStr = "urn:ietf:rfc:3986" + "|" + "urn:uuid:" + uuid;
        pd2.addIdentifier().setSystem("urn:ietf:rfc:3986").setValue("urn:uuid:" + uuid);

        carePlanList.add(pd1);
        carePlanList.add(pd2);

        String resourceString = Utils.createResourceString("Patient1.json");
        Patient pt1 = EmployeeBff.getMyFhirContext().newJsonParser().parseResource(Patient.class, resourceString);
        patientList.add(pt1);

        resourceString = Utils.createResourceString("Patient2.json");
        Patient pt2 = EmployeeBff.getMyFhirContext().newJsonParser().parseResource(Patient.class, resourceString);
        patientList.add(pt2);
    }

    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<CarePlan> getResourceType() {
        return CarePlan.class;
    }

    /**
     * Creates a CarePlan resource
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
     * @param thePlanDef The CarePlan to create
     * @return MethodOutcome
     */
    @Create
    public MethodOutcome create(HttpServletRequest theRequest,
                                              HttpServletResponse theResponse,
                                              @ResourceParam CarePlan thePlanDef) {
        logger.debug("create a CarePlan");

        String uuid = UUID.randomUUID().toString();
        String identifierStr = "urn:ietf:rfc:3986" + "|" + "urn:uuid:" + uuid;
        thePlanDef.addIdentifier().setSystem("urn:ietf:rfc:3986").setValue("urn:uuid:" + uuid);

        MethodOutcome retVal = new MethodOutcome();
        retVal.setCreated(true);
        retVal.setResource(thePlanDef);

        retVal.setId(new IdType(identifierStr));

        OperationOutcome operationOutcome = new OperationOutcome();
        operationOutcome.addIssue().
                setDiagnostics("Successfully created resource with identifier = " + identifierStr).
                setSeverity(OperationOutcome.IssueSeverity.INFORMATION);
        retVal.setOperationOutcome(operationOutcome);

        theResponse.addHeader("Location", identifierStr);
        theResponse.setStatus(201); //Created

        return retVal;

    }

    /**
     *
     * Updates an existing CarePlan resource.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http request
     * @param theCarePlan CarePlan to update. Must have an identifier.
     * @param conditional Required conditional url pointing to the identifier of the resource to update. Must have form
     *                    {@literal CarePlan?identifier=<system>|<value>}.
     * @return MethodOutcome
     */
    @Update
    public MethodOutcome update(HttpServletRequest theRequest,
                                             HttpServletResponse theResponse,
                                             @ResourceParam CarePlan theCarePlan,
                                             @ConditionalUrlParam String conditional) {
        logger.debug("updateCarePlan, conditional = " + conditional);

        MethodOutcome retVal = new MethodOutcome();
        retVal.setResource(theCarePlan);

        Identifier planIdent = theCarePlan.getIdentifier().stream()
                .filter(identifier -> !identifier.getSystem().isEmpty())
                .findAny()
                .orElse(null);
        if (planIdent == null) {
            throw new UnprocessableEntityException("CarePlan must have an identifier");
        }

        String location = planIdent.getSystem() + "|" + planIdent.getValue();
        retVal.setId(new IdType(location));

        OperationOutcome operationOutcome = new OperationOutcome();
        operationOutcome.addIssue().
                setDiagnostics("Successfully updated ressource with identifier = " + location).
                setSeverity(OperationOutcome.IssueSeverity.INFORMATION);
        retVal.setOperationOutcome(operationOutcome);

        theResponse.addHeader("Location", location);
        theResponse.setStatus(200);

        return retVal;
    }

    /**
     * Retrieves the active CarePlan for Patient that is the subject of this CarePlan.
     *
     * Searches only in CarePlans that belong to the department of the currently logged in employee.
     * If no active CarePlan is found an error is returned.
     *
     * The search must be on the form:
     *
     * {@literal http://example.org/CarePlan?subject:identifier=<system>|<value>}
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
     * @param subject Who care plan is for. Must contain the identifier of Patient (required)
     *
     * @return Active CarePlan for Patient.
     */
    @Search(queryName = "getActiveForPatient")
    public CarePlan getActiveForPatient(HttpServletRequest theRequest,
                                          HttpServletResponse theResponse,
                                          @RequiredParam(name=CarePlan.SP_SUBJECT) ReferenceParam subject) {


        return carePlanList.get(0);
    }

    /**
     * Returns a Bundle of FHIR CarePlans that belong to the department of the currently logged in employee.
     *
     * Search must be on the form:
     *
     * {@literal http://example.org/CarePlan?subject:identifier=<system>|<value>}
     *
     * @param theRequest  Incoming http request
     * @param theResponse Outgoing http response
     * @param subject Filter search by the patient subject (optional)
     * @param status Filter search by RequestStatus. Values can be: draft, active, revoked, completed, entered-in-error (optional)
     *
     * @return Active CarePlan for subject
     *
     * @see <a href="http://hl7.org/fhir/2018Sep/valueset-request-status.html">RequestStatus</a>
     * @see <a href="https://issuetracker4s.atlassian.net/wiki/spaces/FDM/pages/47317007/FHIR+CarePlan">FHIR CarePlan</a>
     *
     */
    @Search(queryName = "search")
    public CarePlan search(HttpServletRequest theRequest,
                                   HttpServletResponse theResponse,
                                   @OptionalParam(name=CarePlan.SP_SUBJECT) ReferenceParam subject,
                                   @OptionalParam(name=CarePlan.SP_STATUS) TokenParam status) {


        return carePlanList.get(0);
    }

}
