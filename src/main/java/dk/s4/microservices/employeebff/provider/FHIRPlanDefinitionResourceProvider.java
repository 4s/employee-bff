package dk.s4.microservices.employeebff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.api.annotation.Description;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.api.SortSpec;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.exceptions.FhirClientConnectionException;
import ca.uhn.fhir.rest.param.*;
import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import ca.uhn.fhir.rest.server.exceptions.NotImplementedOperationException;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.employeebff.utils.CopiedParameters;
import dk.s4.microservices.employeebff.utils.OrganizationQueryParameter;
import dk.s4.microservices.messaging.ReplyEventProcessor;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.fhir.FhirBaseBFFResourceProvider;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface.UserContextResolverException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.PlanDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * Resource provider for the FHIR PlanDefinition resources,
 *
 */
public class FHIRPlanDefinitionResourceProvider extends FhirBaseBFFResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(FHIRPlanDefinitionResourceProvider.class);
    private final OrganizationQueryParameter parameters;

    public FHIRPlanDefinitionResourceProvider(FhirContext fhirContext,
    UserContextResolverInterface userContextResolver,
    MyFhirClientFactory clientFactory,
    KafkaEventProducer kafkaCommandProducer,
    ReplyEventProcessor replyEventProcessor,
                                              OrganizationQueryParameter parameters) {
        super(fhirContext, userContextResolver, clientFactory, kafkaCommandProducer, replyEventProcessor);
        this.parameters = parameters;
    }

    @Override
    protected Identifier identifierForResource(IBaseResource resource) {
        return ((PlanDefinition)resource).getIdentifier().stream()
                .filter(ident -> ident.getSystem().equals(ServiceVariables.OFFICIAL_PLANDEFINITION_IDENTIFIER_SYSTEM.getValue()))
                .findAny()
                .orElse(null);
    }

    /**
	 * Returns the type of resource this provider provides.
	 */
	@Override
	public Class<PlanDefinition> getResourceType() {
		return PlanDefinition.class;
	}

    /**
     * Creates a PlanDefinition resource - a template for the telemedical care for a group of patients sharing a certain
     * set of symptoms or a particular condition.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
     * @param thePlanDefinition The PlanDefinition to create
     * @return MethodOutcome
     */
	@Create
	public MethodOutcome createPlanDefinition(HttpServletRequest theRequest,
									   HttpServletResponse theResponse,
									   @ResourceParam PlanDefinition thePlanDefinition) {
        
        logger.debug("create");
        return createOrUpdate(theRequest, theResponse, thePlanDefinition, Topic.Operation.Create);
	}


    /**
     *
     * Updates an existing PlanDefinition resource.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http request
     * @param thePlanDefinition PlanDefinition to update. Must have an identifier.
     * @return MethodOutcome
     */
    @Update
    public MethodOutcome update(HttpServletRequest theRequest,
                                             HttpServletResponse theResponse,
                                             @ResourceParam PlanDefinition thePlanDefinition) {
        logger.debug("update");
        return createOrUpdate(theRequest, theResponse, thePlanDefinition, Topic.Operation.Update);
    }

    /**
     * Deletes a PlanDefinition based on a conditional url pointing out the identifier of the PlanDefinition to
     * delete. If the PlanDefinition is referred to by any active CarePlans an error is thrown.
     *
     * @param theId Not used, will be ignored.
     * @param theConditionalUrl Must state the identifier of the PlanDefinition to delete on the form
     *                          {@literal PlanDefinition?identifier=<system>|<value>}
     */
    @Delete()
    public MethodOutcome delete(@IdParam IdType theId, @ConditionalUrlParam String theConditionalUrl) {
        logger.debug("delete theConditionalUrl = " + theConditionalUrl);
        throw new NotImplementedOperationException("This operation has not been implemented yet");
    }

	/**
	 * Get PlanDefinitions assigned to department of the employee currently logged in.
	 *
	 * @return a Bundle of PlanDefinition resources
	 */
	@Search(queryName = "getMyPlanDefinitions")
	public Bundle getMyPlanDefinitions(HttpServletRequest theRequest,
                                HttpServletResponse theResponse) {
	    try {
            logger.debug("getMyPlanDefinitions");

            IGenericClient client = clientFactory
                    .getClientFromBaseUrl(ServiceVariables.PLANDEFINITION_SERVICE_URL.getValue(), fhirContext);
            String searchUrl = ServiceVariables.PLANDEFINITION_SERVICE_URL.getValue()
                    + "/PlanDefinition?_query=getPlanDefinitionByOrganization&organization=" +
                                        parameters.createFamilyFrom(clientFactory,userContextResolver.getFHIRUserOrganization(theRequest));

            return client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        } catch (UserContextResolverException e) {
            throw new AuthenticationException(e.getMessage(), e);
        }
    }

    /**
     * Search for PlanDefinition by Identifier.
     * 
     * @param theIdentifier Identifier of a PlanDefinition to search for.
     * @return PlanDefinition with matching Identifier or null if not found
     */
	@Search(queryName = "getByIdentifier")
    public PlanDefinition getByIdentifier(HttpServletRequest theRequest,
                                          HttpServletResponse theResponse,
                                          @RequiredParam(name=PlanDefinition.SP_IDENTIFIER) TokenParam theIdentifier) {
        try {
            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.PLANDEFINITION_SERVICE_URL.getValue(), fhirContext);
            String searchUrl = ServiceVariables.PLANDEFINITION_SERVICE_URL.getValue()+ "/PlanDefinition?_query=getByIdentifier&identifier="+theIdentifier.getValueAsQueryToken(fhirContext)
                            + "&organization="
                            +                     parameters.createFamilyFrom(clientFactory,userContextResolver.getFHIRUserOrganization(theRequest));
            Bundle bundle = client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
            return (PlanDefinition) bundle.getEntryFirstRep().getResource();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        } catch (UserContextResolverException e) {
            throw new AuthenticationException(e.getMessage(), e);
        }
    }

    @Search(queryName = "getByUrl")
    public Bundle getByUrl(HttpServletRequest theRequest,
                           HttpServletResponse theResponse, @RequiredParam(name = PlanDefinition.SP_URL) UriParam url) throws UserContextResolverException {
        try {
            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.PLANDEFINITION_SERVICE_URL.getValue(), fhirContext);
            String searchUrl = ServiceVariables.PLANDEFINITION_SERVICE_URL.getValue()+ "/PlanDefinition?_query=getByUrl&url="+url
                    + "&author="
                    +                     parameters.createFamilyFrom(clientFactory,userContextResolver.getFHIRUserOrganization(theRequest));
            return client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        } catch (UserContextResolverException e) {
            throw new AuthenticationException(e.getMessage(), e);
        }
    }

    @Search(allowUnknownParams=true)
    public Bundle search(HttpServletRequest theServletRequest, HttpServletResponse theServletResponse,
                         RequestDetails theRequestDetails,
                         @Description(shortDefinition = "Search the contents of the resource's data using a fulltext search")
                         @OptionalParam(name = "_content") StringAndListParam theFtContent,
                         @Description(shortDefinition = "Return resources linked to by the given target")
                         @OptionalParam(name = "_has") HasAndListParam theHas,
                         @Description(shortDefinition = "The ID of the resource")
                         @OptionalParam(name = "_id") TokenAndListParam the_id,
                         @Description(shortDefinition = "A PlanDefinition's Identifier")
                         @OptionalParam(name = "identifier") TokenAndListParam theIdentifier,
                         @Description(shortDefinition = "The Canonical identifier for PlanDefinition")
                         @OptionalParam(name = PlanDefinition.SP_URL) UriParam url,
                         @Description(shortDefinition = "The business version of the plan definition")
                         @OptionalParam(name = "version") TokenAndListParam theVersion,
                         @Description(shortDefinition = "The human-friendly name of the plan definition")
                         @OptionalParam(name = "title") StringAndListParam theTitle,
                         @Description(shortDefinition = "The current status of the PlanDefinition")
                         @OptionalParam(name = "status") TokenAndListParam theStatus,
                         @Description(shortDefinition = "Name of the publisher of the plan definition")
                         @OptionalParam(name = "publisher") StringAndListParam thePublisher,
                         @Description(shortDefinition = "Intended jurisdiction for the plan definition")
                         @OptionalParam(name = "jurisdiction") TokenAndListParam theJurisdiction,
                         @Description(shortDefinition = "The description of the plan definition")
                         @OptionalParam(name = "description") StringAndListParam theDescription,
                         @Description(shortDefinition = "The managing organization of the plan definition")
                         @OptionalParam(name = "managing-organization") TokenAndListParam theManagingOrganization,
                         @Description(shortDefinition = "Activity or plan definitions used by plan definition")
                         @OptionalParam(name = "definition", targetTypes = {}) ReferenceAndListParam theDefinition,
                         @Description(shortDefinition = "Only return resources which were last updated as specified by the given range")
                         @OptionalParam(name = "_lastUpdated") DateRangeParam theLastUpdated,

                         @RawParam Map<String, List<String>> theAdditionalRawParams,

                         @Sort SortSpec theSort,
                         @Count Integer theCount) {
        logger.debug("search");
        try {
            String hostname = ServiceVariables.PLANDEFINITION_SERVICE_URL.getValue();
            IGenericClient client = clientFactory.getClientFromBaseUrl(hostname, fhirContext);
            String searchUrl = constructUrlFromRequestDetails(hostname, theRequestDetails);

            return client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        }
    }

    String constructUrlFromRequestDetails(String hostname, RequestDetails requestDetails){
        return hostname + "/PlanDefinition" + CopiedParameters.from(requestDetails).get();
    }

}
