package dk.s4.microservices.employeebff.provider.stub;

import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Organization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

public class FHIROrganizationResourceProviderStub implements IResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(FHIROrganizationResourceProviderStub.class);

    private List<Organization> organizationList = new ArrayList<>();

    public FHIROrganizationResourceProviderStub() {
        createStubData();
    }

    private void createStubData() {
        Organization o1 = new Organization();
        o1.setId("test1");
        o1.addAlias("Test1 alias");
        Organization o2 = new Organization();
        o2.setId("test2");
        o2.addAlias("Test 2 alias");
        organizationList.add(o1);
        organizationList.add(o2);
    }

    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<Organization> getResourceType() {
        return Organization.class;
    }

    /**
     * Returns Bundle of Organizations corresponding to the department and underlying sections of the currently logged
     * in user. 
     * 
     * The currently logged in user should always at least be attached to a department, so Bundle should not be empty.
     * Will throw an error if this happens.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response

     * @return Bundle of Organization resources
     */
    @Search(queryName = "search")
    public Bundle search(HttpServletRequest theRequest,
                         HttpServletResponse theResponse) {
        logger.debug("search");

        Bundle bundle = new Bundle();
        bundle.addEntry().setResource(organizationList.get(0));
        bundle.addEntry().setResource(organizationList.get(1));

        return bundle;

    }

    /**
     * Get Organization resource by Identifier.
     *
     * Example invocation: http://fhir.example.com/Organization?identifier=urn:oid:1.2.208.176.1.1|373241000016009
     *
     * @param identifier Identifier of an Organization to search for (required)
     *
     * @return Organization with matching Identifier or null if not found
     */
    @Search(queryName = "getByIdentifier")
    public Organization getByIdentifier(HttpServletRequest theRequest,
                                        HttpServletResponse theResponse,
                                        @RequiredParam(name=Organization.SP_IDENTIFIER) TokenParam identifier) {
        
        return organizationList.get(0);
    }
}
