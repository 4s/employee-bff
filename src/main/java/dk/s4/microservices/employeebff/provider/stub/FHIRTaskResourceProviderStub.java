package dk.s4.microservices.employeebff.provider.stub;

import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.param.ReferenceParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.OperationOutcome;
import org.hl7.fhir.r4.model.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FHIRTaskResourceProviderStub implements IResourceProvider {

    public enum Encoding {
        JSON
    }

    private static final Logger logger = LoggerFactory.getLogger(FHIRQuestionnaireResourceProviderStub.class);

    private List<Task> taskList = new ArrayList<>();

    public FHIRTaskResourceProviderStub() {
        createStubData();
    }

    private void createStubData() {
        Task t1 = new Task();
        t1.setId("test1");
        t1.addNote().setText("Test1");
        Task t2 = new Task();
        t2.setId("test2");
        t2.addNote().setText("Test2");
        taskList.add(t1);
        taskList.add(t2);
    }

    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<Task> getResourceType() {
        return Task.class;
    }

    /**
     * Creates acknowledgement or notification type (stated via Task.code) Task where Task.for must reference patient
     * to receive the acknowledgement/notification
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
     * @param theTask Task resource to create
     * @return MethodOutcome
     */
    @Create
    public MethodOutcome create(HttpServletRequest theRequest,
                                       HttpServletResponse theResponse,
                                       @ResourceParam Task theTask) {
        logger.debug("create");

        String uuid = UUID.randomUUID().toString();
        String identifierStr = "urn:ietf:rfc:3986" + "|" + "urn:uuid:" + uuid;
        theTask.addIdentifier().setSystem("urn:ietf:rfc:3986").setValue("urn:uuid:" + uuid);

        MethodOutcome retVal = new MethodOutcome();
        retVal.setCreated(true);
        retVal.setResource(theTask);

        retVal.setId(new IdType(identifierStr));

        OperationOutcome operationOutcome = new OperationOutcome();
        operationOutcome.addIssue().
                setDiagnostics("Successfully created ressource with identifier = " + identifierStr).
                setSeverity(OperationOutcome.IssueSeverity.INFORMATION);
        retVal.setOperationOutcome(operationOutcome);

        theResponse.addHeader("Location", identifierStr);
        theResponse.setStatus(201); //Created

        return retVal;
    }

    /**
     * Updates acknowledgement or notification type (stated via Task.code) Task where Task.for must reference patient
     * to receive the acknowledgement/notification
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
     * @param theTask Task resource to update
     * @return MethodOutcome
     */
    @Update
    public MethodOutcome update(HttpServletRequest theRequest,
                                    HttpServletResponse theResponse,
                                    @ResourceParam Task theTask) {
        logger.debug("update");

        String uuid = UUID.randomUUID().toString();
        String identifierStr = "urn:ietf:rfc:3986" + "|" + "urn:uuid:" + uuid;
        theTask.addIdentifier().setSystem("urn:ietf:rfc:3986").setValue("urn:uuid:" + uuid);

        MethodOutcome retVal = new MethodOutcome();
        retVal.setResource(theTask);

        retVal.setId(new IdType(identifierStr));

        OperationOutcome operationOutcome = new OperationOutcome();
        operationOutcome.addIssue().
                setDiagnostics("Successfully updated ressource with identifier = " + identifierStr).
                setSeverity(OperationOutcome.IssueSeverity.INFORMATION);
        retVal.setOperationOutcome(operationOutcome);

        theResponse.addHeader("Location", identifierStr);
        theResponse.setStatus(200);

        return retVal;
    }


    /**
     * Searches Task resources where owner references the Organization of the currently logged in employee.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
     * @param focus What task is acting on, reference to an Observation or a QuestionnaireResponse (optional).
     * @param status Task completion status, TaskStatus code, here with possible values: "requested", "in-progress",
     *               "completed", "entered-in-error"
     * @param code TaskType code, here with possible value "acknowledgement", "review"
     * @param reason_code TaskReason code, here with possible values "missing-information", "triage-alert"
     *
     * @return Bundle of Task resources
     *
     * @see <a href="http://hl7.org/fhir/2018Sep/valueset-task-status.html">TaskStatus</a>
     * @see <a href="https://issuetracker4s.atlassian.net/wiki/spaces/FDM/pages/47251463/FHIR+Task">FHIR Task</a>
     */
    @Search(queryName = "search")
    public Bundle search(HttpServletRequest theRequest,
                           HttpServletResponse theResponse,
                           @OptionalParam(name=Task.SP_FOCUS) ReferenceParam focus,
                           @OptionalParam(name=Task.SP_STATUS)TokenParam status,
                           @OptionalParam(name=Task.SP_CODE) TokenParam code,
                           @OptionalParam(name="reason-code") TokenParam reason_code) {
        logger.debug("search");

        Bundle bundle = new Bundle();
        bundle.addEntry().setResource(taskList.get(0));
        bundle.addEntry().setResource(taskList.get(1));

        return bundle;

    }
}
