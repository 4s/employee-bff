package dk.s4.microservices.employeebff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.exceptions.FhirClientConnectionException;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Organization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FHIROrganizationResourceProvider implements IResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(FHIROrganizationResourceProvider.class);

    protected FhirContext fhirContext;
    protected UserContextResolverInterface userContextResolver;
    protected MyFhirClientFactory clientFactory;

    /**
     * Constructor
     *
     * @param fhirContext The (typically singleton) {@link ca.uhn.fhir.context.FhirContext} instance of the server
     * @param userContextResolver For resolving user fhirContext information.
     * @param clientFactory Factory for getting HAPI FHIR client instances
     */
    public FHIROrganizationResourceProvider(FhirContext fhirContext,
            UserContextResolverInterface userContextResolver,
            MyFhirClientFactory clientFactory) {
        this.fhirContext = fhirContext;
        this.userContextResolver = userContextResolver;
        this.clientFactory = clientFactory;
    }


    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<Organization> getResourceType() {
        return Organization.class;
    }

    /**
     * Returns Bundle of Organizations corresponding to the department and underlying sections of the currently logged
     * in user. 
     * 
     * The currently logged in user should always at least be attached to a department, so Bundle should not be empty.
     * Will throw an error if this happens.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response

     * @return Bundle of Organization resources
     */
    @Search(queryName = "getMyOrganization")
    public Bundle getMyOrganization(HttpServletRequest theRequest,
                         HttpServletResponse theResponse) {
        try {
            logger.debug("getMyOrganizations");

            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue(), fhirContext);
            Identifier identifier = userContextResolver.getFHIRUserOrganization(theRequest).setSystem(ServiceVariables.OFFICIAL_ORGANIZATION_IDENTIFIER_SYSTEM.getValue());
            TokenParam tokenParam = new TokenParam().setSystem(identifier.getSystem()).setValue(identifier.getValue());
            String searchUrl = ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue() +
                    "/Organization?_query=getByIdentifier&identifier=" + tokenParam.getValueAsQueryToken(fhirContext);

            return client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        } catch (UserContextResolverInterface.UserContextResolverException e) {
            logger.error(e.getMessage(), e);
            throw new AuthenticationException(e.getMessage(), e);
        }
}

    /**
     * Get Organization resource by Identifier.
     *
     * Example invocation: http://fhir.example.com/Organization?identifier=urn:oid:1.2.208.176.1.1|373241000016009
     *
     * @param identifier Identifier of an Organization to search for (required)
     *
     * @return Organization with matching Identifier or null if not found
     */
    @Search(queryName = "getByIdentifier")
    public Bundle getByIdentifier(HttpServletRequest theRequest,
                                        HttpServletResponse theResponse,
                                        @RequiredParam(name=Organization.SP_IDENTIFIER) TokenParam identifier) {
        try {
            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue(), fhirContext);
            String searchUrl = ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue() +
                    "/Organization?_query=getByIdentifier&identifier=" + identifier.getValueAsQueryToken(fhirContext);

            return client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        }
    }

    @Search(queryName = "getMyChildren")
    public Bundle getMyChildren(HttpServletRequest theRequest,
                              HttpServletResponse theResponse) {
        try {
            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue(), fhirContext);
            Identifier identifier = userContextResolver.getFHIRUserOrganization(theRequest).setSystem(ServiceVariables.OFFICIAL_ORGANIZATION_IDENTIFIER_SYSTEM.getValue());
            TokenParam tokenParam = new TokenParam().setValue(identifier.getValue()).setSystem(identifier.getSystem());
            String searchUrl = ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue() +
                    "/Organization?_query=getChildren&partOfIdentifier=" + tokenParam.getValueAsQueryToken(fhirContext);

            return client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        } catch (UserContextResolverInterface.UserContextResolverException e) {
            logger.error(e.getMessage(), e);
            throw new AuthenticationException(e.getMessage(), e);
        }
    }

    @Search(queryName = "getChildren")
    public Bundle getChildren(HttpServletRequest theRequest,
                              HttpServletResponse theResponse,
                              @RequiredParam(name=Organization.SP_IDENTIFIER) TokenParam identifier) {
        try {
            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue(), fhirContext);
            String searchUrl = ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue() +
                    "/Organization?_query=getChildren&partOfIdentifier=" + identifier.getValueAsQueryToken(fhirContext);

            return client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        }
    }

    @Search(queryName = "getDescendants")
    public Bundle getDescendants(HttpServletRequest theRequest,
                              HttpServletResponse theResponse,
                              @RequiredParam(name=Organization.SP_IDENTIFIER) TokenParam identifier) {
        try {
            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue(), fhirContext);
            String searchUrl = ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue() +
                    "/Organization?_query=getDescendants&partOfIdentifier=" + identifier.getValueAsQueryToken(fhirContext);

            return client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        }
    }
}
