package dk.s4.microservices.employeebff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.exceptions.FhirClientConnectionException;
import ca.uhn.fhir.rest.param.DateParam;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.TokenOrListParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.employeebff.utils.OrganizationQueryParameter;
import dk.s4.microservices.employeebff.utils.Utils;
import dk.s4.microservices.messaging.ReplyEventProcessor;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.fhir.FhirBaseBFFResourceProvider;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface.UserContextResolverException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FHIRTaskResourceProvider extends FhirBaseBFFResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(FHIRTaskResourceProvider.class);
    private final OrganizationQueryParameter organizationQueryParameter;

    public FHIRTaskResourceProvider(FhirContext fhirContext,
                                    UserContextResolverInterface userContextResolver,
                                    MyFhirClientFactory clientFactory,
                                    KafkaEventProducer kafkaCommandProducer,
                                    ReplyEventProcessor replyEventProcessor,
                                    OrganizationQueryParameter parameters) {
        super(fhirContext, userContextResolver, clientFactory, kafkaCommandProducer, replyEventProcessor);
        this.organizationQueryParameter = parameters;
    }

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return Task.class;
    }

    @Override
    protected Identifier identifierForResource(IBaseResource resource) {
        return ((Task) resource).getIdentifier().stream()
                .filter(identifier -> identifier.getSystem().equals(ServiceVariables.OFFICIAL_TASK_IDENTIFIER_SYSTEM.getValue()))
                .findAny()
                .orElse(null);
    }

    @Create
    public MethodOutcome createTask(HttpServletRequest theRequest,
                                    HttpServletResponse theResponse,
                                    @ResourceParam Task theTask) {
        logger.debug("CreateTask");
        return createOrUpdate(theRequest, theResponse, theTask, Topic.Operation.Create);
    }

    /**
     * Returns a Bundle of tasks that match the search criteria given
     *
     * @param theRequest       the request
     * @param theResponse      the response
     * @param theRequestDetail the details
     * @param status           the status of the task
     * @param code             the code of the task
     * @param focus            the identifier of the focus of the task
     * @param subject          the patient subject of the task
     * @param basedOn          the CarePlan upon which the task is based on
     * @param reasonCode       the reason code of the task
     * @return Bundle containing tasks
     */
    @Search(allowUnknownParams = true)
    public Bundle search(HttpServletRequest theRequest, HttpServletResponse theResponse,
                                         RequestDetails theRequestDetail,
                                         @OptionalParam(name = Task.SP_STATUS) TokenOrListParam status,
                                         @OptionalParam(name = Task.SP_CODE) TokenParam code,
                                         @OptionalParam(name = Task.SP_FOCUS) TokenParam focus,
                                         @OptionalParam(name = Task.SP_SUBJECT) TokenParam subject,
                                         @OptionalParam(name = Task.SP_BASED_ON) TokenParam basedOn,
                                         @OptionalParam(name = "reason-code") TokenParam reasonCode) throws UserContextResolverException {
        logger.debug("search");
        try {
            String hostname = ServiceVariables.OUTCOME_SERVICE_URL.getValue();
            IGenericClient client = clientFactory.getClientFromBaseUrl(hostname, fhirContext);
            String searchUrl = Utils.constructUrlFromRequestDetails(hostname, "Task",theRequestDetail);
            searchUrl = Utils.addOrganizationParameters(searchUrl, "owner",theRequest,organizationQueryParameter,clientFactory,userContextResolver);

            return client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        } catch (UserContextResolverInterface.UserContextResolverException e) {
            logger.error(e.getMessage(), e);
            throw new AuthenticationException(e.getMessage(), e);
        }
    }

    /**
     * Updates a task resource on the OutcomeService-service and returns a MethodOutcome resource indicating whether the
     * action was successful.
     *
     * @param theRequest  The incoming request
     * @param theResponse The outgoing response
     * @param theTask     The updated task to be persisted on the Outcome-service
     * @return A MethodOutcome resource indicating whether the action was successful.
     */
    @Update
    public MethodOutcome updateTask(HttpServletRequest theRequest,
                                    HttpServletResponse theResponse,
                                    @ResourceParam Task theTask) {
        logger.debug("Update Task");
        return createOrUpdate(theRequest, theResponse, theTask, Topic.Operation.Update);
    }

    /**
     * Returns a bundle of tasks with a given code and focus (optional).
     *
     * @param theRequest       the http request
     * @param theResponse      the http response
     * @param theRequestDetail the http request details
     * @param code             the FHIR Task code, either acknowledgement, review or activity
     * @param focusType        the focus of the task
     * @param authored_on      the period
     * @return Bundle containing tasks
     * @see <a href="https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task+type+coding+system">Task type coding system</a>
     */

    @Search(queryName = "getTaskWithFocusTypeAndCode")
    public Bundle getTaskWithFocusAndCode(HttpServletRequest theRequest, HttpServletResponse theResponse,
                                          RequestDetails theRequestDetail,
                                          @RequiredParam(name = Task.SP_CODE) TokenParam code,
                                          @OptionalParam(name = "focusType") TokenParam focusType,
                                          @OptionalParam(name = Task.SP_AUTHORED_ON) DateRangeParam authored_on) throws UserContextResolverInterface.UserContextResolverException {
        try {
            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.OUTCOME_SERVICE_URL.getValue(), fhirContext);
            StringBuilder builder = new StringBuilder(ServiceVariables.OUTCOME_SERVICE_URL.getValue() + "/Task?_query=getTaskWithFocusTypeAndCode" +
                    "&code=https://issuetracker4s.atlassian.net/wiki/spaces/FHIRReg/pages/100794407/Task%2btype%2bcoding%2bsystem|" +
                    code.getValue());

            builder.append("&owner=");
            builder.append(organizationQueryParameter.createFamilyFrom(clientFactory, userContextResolver.getFHIRUserOrganization(theRequest)));

            if (focusType != null) {
                builder.append("&");
                builder.append("focusType");
                builder.append("=");
                builder.append(focusType.getValue());
            }
            if (authored_on != null) {
                // Search for tasks that have been authored in given period
                for (DateParam dateParam : authored_on.getValuesAsQueryTokens()) {
                    builder.append("&");
                    builder.append(Task.SP_AUTHORED_ON);
                    builder.append("=");
                    builder.append(dateParam.getValueAsQueryToken(fhirContext));
                }
            }
            return client.search().byUrl(builder.toString()).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        } catch (UserContextResolverException e) {
            throw new AuthenticationException(e.getMessage(), e);
        }
    }
}
