package dk.s4.microservices.employeebff.provider.stub;

import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.param.DateParam;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.employeebff.servlet.EmployeeBff;
import dk.s4.microservices.employeebff.utils.Utils;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Resource provider for the FHIR Patient resource,
 *
 */
public class FHIRPatientResourceProviderStub implements IResourceProvider {

	public enum Encoding {
		JSON
	}

	private static final Logger logger = LoggerFactory.getLogger(FHIRPatientResourceProviderStub.class);

    private List<Patient> patientList = new ArrayList<>();

	public FHIRPatientResourceProviderStub() throws IOException {
            createStubData();
    }

    private void createStubData() throws IOException {
		String resourceString = Utils.createResourceString("Patient1.json");
        Patient pt1 = EmployeeBff.getMyFhirContext().newJsonParser().parseResource(Patient.class, resourceString);
		patientList.add(pt1);

		resourceString = Utils.createResourceString("Patient2.json");
		Patient pt2 = EmployeeBff.getMyFhirContext().newJsonParser().parseResource(Patient.class, resourceString);
        patientList.add(pt2);
    }

	/**
	 * Returns the type of resource this provider provides.
	 */
	@Override
	public Class<Patient> getResourceType() {
		return Patient.class;
	}

	/**
	 * Creates a Patient resource and an active CarePlan resource with Patient resource as subject. The CarePlan will
	 * have author set to the Organization resource that corresponds to the department of the currently logged in
	 * employee.
	 *
	 * @param theRequest Incoming http request
	 * @param theResponse Outgoing http request
	 * @param thePatient Patient resource to create. Patient is required to have an identifier in Danish CPR system
	 *                      ("urn:oid:1.2.208.176.1.2")
	 * @return MethodOutcome
	 */
	@Create
	public MethodOutcome create(HttpServletRequest theRequest,
								HttpServletResponse theResponse,
								@ResourceParam Patient thePatient) {
        logger.debug("create");

        MethodOutcome retVal = new MethodOutcome();
        retVal.setCreated(true);
        retVal.setResource(thePatient);

        Identifier patientIdentifier = thePatient.getIdentifier().stream()
                .filter(identifier -> identifier.getSystem().equals(ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue()))
                .findAny()
                .orElse(null);
        if (patientIdentifier == null) {
            throw new UnprocessableEntityException("Patient must have an identifier with system = " +
					ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue());
        }

        retVal.setId(new IdType(patientIdentifier.getSystem() + "|" + patientIdentifier.getValue()));

        OperationOutcome operationOutcome = new OperationOutcome();
        operationOutcome.addIssue().
                setDiagnostics("Successfully updated resource with identifier = " +
                        patientIdentifier.getSystem() + "|" + patientIdentifier.getValue()).
                setSeverity(OperationOutcome.IssueSeverity.INFORMATION);
        retVal.setOperationOutcome(operationOutcome);

        theResponse.addHeader("Location", patientIdentifier.getSystem() + "|" + patientIdentifier.getValue());
        theResponse.setStatus(201); //Created

        return retVal;
	}

    /**
     * Update an existing Patient resource.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http request
     * @param thePatient Patient resource to update
     * @param conditional Required conditional url pointing to the identifier of the resource to update. Must have form
     *                    {@literal Patient?identifier=<system>|<value>}.
     * @return MethodOutcome
     */
	@Update
    public MethodOutcome update(HttpServletRequest theRequest,
								HttpServletResponse theResponse,
								@ResourceParam Patient thePatient,
								@ConditionalUrlParam String conditional) {
	    logger.debug("update, conditional = " + conditional);

        MethodOutcome retVal = new MethodOutcome();
        retVal.setResource(thePatient);

        Identifier patientIdentifier = thePatient.getIdentifier().stream()
                .filter(identifier -> identifier.getSystem().equals(ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue()))
                .findAny()
                .orElse(null);
        if (patientIdentifier == null) {
            throw new UnprocessableEntityException("Patient must have an identifier with system = " +
					ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue());
        }

        retVal.setId(new IdType(patientIdentifier.getSystem() + "|" + patientIdentifier.getValue()));

        OperationOutcome operationOutcome = new OperationOutcome();
        operationOutcome.addIssue().
                setDiagnostics("Successfully updated resource with identifier = " +
                        patientIdentifier.getSystem() + "|" + patientIdentifier.getValue()).
                setSeverity(OperationOutcome.IssueSeverity.INFORMATION);
        retVal.setOperationOutcome(operationOutcome);

        theResponse.addHeader("Location", patientIdentifier.getSystem() + "|" + patientIdentifier.getValue());
        theResponse.setStatus(200);

        return retVal;
    }

	/**
	 * Returns patients assigned to the department of the employee currently logged in.
	 *
	 * @param theRequest Incoming http request
	 * @param theResponse Outgoing http request
	 * @param name Filter search by patient name (matches any string fields in HumanName) (optional).
	 * @param phone Filter search by value in phone contact (optional).
	 * @param birthdate Filter search by patient's date of birth (optional).
	 * @param contextCode Filter search by the type of PlanDefinition the Patient is associated with (optional).
	 *                       Matches on PlanDefinition.useContext.​value as CodeableConcept, i.e. as system and code.
	 *                       System is typically ICD10
	 * @param contextText Filter search by the type of PlanDefinition the Patient is associated with (optional).
	 *                       Matches on PlanDefinition.useContext.value as CodeableConcept.text, i.e. as plain text.
	 *
	 * @return Bundle of Patient resources assigned to the department of the employee currently logged in.
	 */
	@Search(queryName = "search")
	public Bundle search(HttpServletRequest theRequest,
								HttpServletResponse theResponse,
								@OptionalParam(name=Patient.SP_NAME) StringParam name,
								@OptionalParam(name=Patient.SP_PHONE) TokenParam phone,
								@OptionalParam(name=Patient.SP_BIRTHDATE) DateParam birthdate,
								@OptionalParam(name="PlanDefinitionContext") TokenParam contextCode,
								@OptionalParam(name="PlanDefinitionContextText") StringParam contextText) {
        logger.debug("search");

	    Bundle bundle = new Bundle();
	    bundle.addEntry().setResource(patientList.get(0));
        bundle.addEntry().setResource(patientList.get(1));

		return bundle;
	}

	/**
	 * Searches for the CarePlans referring to a given PlanDefinition.
	 * Returns the Patients that these CarePlans have as subject.
	 *
	 * @param theRequest Incoming http request
	 * @param theResponse Outgoing http response
	 * @param instantiatesCanonical Canonical url of a PlanDefinition (optional)
	 *
	 * @return Bundle of Patient resources
	 */
	@Search(queryName = "searchByPlanDef")
	public Bundle searchByPlanDef(HttpServletRequest theRequest,
								  HttpServletResponse theResponse,
								  @OptionalParam(name="instantiatesCanonical") StringParam instantiatesCanonical) {
		logger.debug("searchByPlanDef");

		Bundle bundle = new Bundle();
		bundle.addEntry().setResource(patientList.get(0));
		bundle.addEntry().setResource(patientList.get(1));

		return bundle;
	}

	/**
	 * Search for Patient by Identifier.
	 *
	 * Example invocation: http://fhir.example.com/Patient?identifier=urn:oid:1.2.208.176.1.2|2512489996
	 *
	 * @param theRequest Incoming http request
	 * @param theResponse Outgoing http response
	 * @param identifier Identifier of a Patient to search for (required)

	 * @return Patient with matching Identifier or null if not found
	 */
	@Search(queryName = "getPatientByIdentifier")
	public Patient getPatientByIdentifier(HttpServletRequest theRequest,
								   HttpServletResponse theResponse,
								   @RequiredParam(name=Patient.SP_IDENTIFIER) TokenParam identifier) {
		String identifierSystem = identifier.getSystem();
		String identifierValue = identifier.getValue();
		Patient pt = new Patient();
		pt.addIdentifier().setSystem(identifierSystem).setValue(identifierValue);

		return new Patient();
	}
}
