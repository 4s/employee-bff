package dk.s4.microservices.employeebff.provider.stub;

import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class FHIRQuestionnaireResourceProviderStub implements IResourceProvider {

	private static final Logger logger = LoggerFactory.getLogger(FHIRQuestionnaireResourceProviderStub.class);

    private List<Questionnaire> questionnaireList = new ArrayList<>();

	public FHIRQuestionnaireResourceProviderStub() throws KafkaInitializationException {
            createStubData();
    }

    private void createStubData() {
		questionnaireList.add(new Questionnaire().setName("Test1").setStatus(Enumerations.PublicationStatus.ACTIVE));
		questionnaireList.add(new Questionnaire().setName("Test2").setStatus(Enumerations.PublicationStatus.ACTIVE));
		questionnaireList.get(0).setId("test1");
		questionnaireList.get(1).setId("test2");
    }

	/**
	 * Returns the type of resource this provider provides.
	 */
	@Override
	public Class<Questionnaire> getResourceType() {
		return Questionnaire.class;
	}

    /**
     * Updates an existing Questionnaire resource. Only the status field is allowed to change.
     *
     * @param theRequest Incoming http request
     * @param theResponse outgoing http response
     * @param theQuestionnaire Questionnaire resource to update
     * @param conditional {@literal Required conditional url pointing to the identifier of the resource to update. Must have form
     *                    Questionnaire?identifier=<system>|<value>.}
     * @return MethodOutcome
     */
    @Update
    public MethodOutcome update(HttpServletRequest theRequest,
                                       HttpServletResponse theResponse,
                                       @ResourceParam Questionnaire theQuestionnaire,
                                       @ConditionalUrlParam String conditional) {
        logger.debug("update, conditional = " + conditional);

        MethodOutcome retVal = new MethodOutcome();
        retVal.setResource(theQuestionnaire);

        Identifier questIdent = theQuestionnaire.getIdentifier().stream()
                .filter(identifier -> !identifier.getSystem().isEmpty())
                .findAny()
                .orElse(null);
        if (questIdent == null) {
            throw new UnprocessableEntityException("Questionnaire must have an identifier");
        }

        String location = questIdent.getSystem() + "|" + questIdent.getValue();
        retVal.setId(new IdType(location));

        OperationOutcome operationOutcome = new OperationOutcome();
        operationOutcome.addIssue().
                setDiagnostics("Successfully updated ressource with identifier = " + location).
                setSeverity(OperationOutcome.IssueSeverity.INFORMATION);
        retVal.setOperationOutcome(operationOutcome);

        theResponse.addHeader("Location", location);
        theResponse.setStatus(200);

        return retVal;
    }

    /**
     * Returns the Questionnaire resources assigned to the department of the employee currently logged in
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
     * @param status Publication status (optional). Can be either "active" or "retired" from the PublicationStatus
     *               ValueSet
     * @return a Bundle of Questionnaire resources
     * @see <a href="http://hl7.org/fhir/2018Sep/valueset-publication-status.html">PublicationStatus</a>
     */
    @Search(queryName = "search")
    public Bundle search(HttpServletRequest theRequest,
                         HttpServletResponse theResponse,
                         @OptionalParam(name=Questionnaire.SP_STATUS) TokenParam status) {
        logger.debug("search");

	    Bundle bundle = new Bundle();
	    bundle.addEntry().setResource(questionnaireList.get(0));
        bundle.addEntry().setResource(questionnaireList.get(1));

		return bundle;
	}

    /**
     * Search for Questionnaire by Identifier.
     *
     * Example invocation: http://fhir.example.com/Questionnaire?identifier=urn:ietf:rfc:3986|cb0a63f8-b595-4882-a1d2-71eba429d56e
     *
     * @param identifier Identifier of a Questionnaire to search for.
     * @return Questionnaire with matching Identifier or null if not found
     */
    @Search(queryName = "getByIdentifier")
    public Questionnaire getByIdentifier(@RequiredParam(name=Patient.SP_IDENTIFIER) TokenParam identifier) {
        Questionnaire q = questionnaireList.get(0);
        q.addIdentifier().setSystem(identifier.getSystem()).setValue(identifier.getValue());

        return q;
    }
}
