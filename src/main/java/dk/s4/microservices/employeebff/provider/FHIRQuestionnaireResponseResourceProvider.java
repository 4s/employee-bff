package dk.s4.microservices.employeebff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.exceptions.FhirClientConnectionException;
import ca.uhn.fhir.rest.param.DateParam;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.employeebff.utils.OrganizationQueryParameter;
import dk.s4.microservices.messaging.ReplyEventProcessor;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.fhir.FhirBaseBFFResourceProvider;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface.UserContextResolverException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.QuestionnaireResponse;
import org.hl7.fhir.r4.model.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FHIRQuestionnaireResponseResourceProvider extends FhirBaseBFFResourceProvider {
    private static final Logger logger = LoggerFactory.getLogger(FHIRQuestionnaireResponseResourceProvider.class);
    private final OrganizationQueryParameter parameters;

    /**
     * Constructor
     *
     * @param fhirContext The (typically singleton) {@link ca.uhn.fhir.context.FhirContext} instance of the server
     * @param userContextResolver For resolving user fhirContext information.
     * @param clientFactory Factory for getting HAPI FHIR client instances
     */
    public FHIRQuestionnaireResponseResourceProvider(FhirContext fhirContext,
            UserContextResolverInterface userContextResolver,
            MyFhirClientFactory clientFactory, 
            KafkaEventProducer kafkaCommandProducer, 
            ReplyEventProcessor replyEventProcessor,
                                                     OrganizationQueryParameter parameters) {
        super(fhirContext, userContextResolver, clientFactory, kafkaCommandProducer, replyEventProcessor);
        this.parameters = parameters;

    }

    @Override
    protected Identifier identifierForResource(IBaseResource resource) {
        return ((QuestionnaireResponse) resource).getIdentifier();
    }

    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<? extends IBaseResource> getResourceType() { return QuestionnaireResponse.class; }

    /**
     * Updates an existing QuestionnaireResponse resource.  The status field is required to be 'amended'.
     *
     * @param theRequest Incoming http request
     * @param theResponse outgoing http response
     * @param theQuestionnaireResponse QuestionnaireResponse resource to update
     * @return MethodOutcome
     */
    @Update
    public MethodOutcome update(HttpServletRequest theRequest,
                                HttpServletResponse theResponse,
                                @ResourceParam QuestionnaireResponse theQuestionnaireResponse) {
        logger.debug("update QuestionnaireResponse");


        if (!isAmended(theQuestionnaireResponse)){
            throw ExceptionUtils.fatalFormatException("The QuestionnaireResponse must have status a amended");
        }
        MethodOutcome methodOutcome = createOrUpdate(theRequest, theResponse, theQuestionnaireResponse, Topic.Operation.Update);
        return methodOutcome;

    }

    boolean isAmended(QuestionnaireResponse questionnaireResponse){
        QuestionnaireResponse.QuestionnaireResponseStatus status = questionnaireResponse.getStatus();
        return status != null && status.equals(QuestionnaireResponse.QuestionnaireResponseStatus.AMENDED);
    }

    /**
     * Get all QuestionnaireResponse resources that are the focus of the tasks with the provided code and status
     *
     * Example invocation:
     * GET http://fhir.example.com/QuestionnaireResponse?_query=getFocusForTasksWithCodeAndStatus&code=acknowledgement&status=requested
     *
     * @param code Search criteria: the code of the tasks
     * @param status Search criteria: the status of the tasks
     *
     * @return A bundle containing all QuestionnaireResponse resources matching the search criteria
     */
    @Search(queryName = "getFocusForTasksWithCodeAndStatus")
    public Bundle getFocusForTasksWithCodeAndStatus(HttpServletRequest theRequest,
                                                    @RequiredParam(name=Task.SP_CODE) TokenParam code,
                                                    @RequiredParam(name=Task.SP_STATUS) TokenParam status) {
        try {
            logger.trace("get QuestionnaireResponses with code {} and status {}", code.getValue(), status.getValue());

            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.OUTCOME_SERVICE_URL.getValue(), fhirContext);
            String searchUrl = String.format("/QuestionnaireResponse?_query=getFocusForTasksWithCodeAndStatus" +
                                            "&code=%s" +
                                            "&status=%s" +
                                            "&owner=%s",
                                code.getValue(),
                                status.getValue(),
                                        parameters.createFamilyFrom(clientFactory,userContextResolver.getFHIRUserOrganization(theRequest)));
            Bundle result = client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();

            logger.debug("QuestionnaireResponse resources returned from the Outcome service: Number of resources found = {}",
                         result.getEntry().size());

            return result;
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        } catch (UserContextResolverException e) {
            throw new AuthenticationException(e.getMessage(), e);
        }
    }

    @Search(queryName = "searchByBasedOnIdentifier")
    public Bundle searchByBasedOnIdentifier(HttpServletRequest theRequest,
                                 HttpServletResponse theResponse,
                                 @RequiredParam(name="basedOn-identifier") TokenParam basedOn,
                                 @OptionalParam(name = QuestionnaireResponse.SP_AUTHORED) DateRangeParam authored) {
        try{
            logger.debug("searchByBasedOnIdentifier:" +  basedOn.getValueAsQueryToken(fhirContext));
            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.OUTCOME_SERVICE_URL.getValue(), fhirContext);
            StringBuilder builder = new StringBuilder(ServiceVariables.OUTCOME_SERVICE_URL.getValue()+"/QuestionnaireResponse?_query=searchByBasedOnIdentifier&based-on="
                    +basedOn.getValueAsQueryToken(fhirContext));
            if(authored != null && !authored.isEmpty()){
                for (DateParam authoredParam : authored.getValuesAsQueryTokens()){
                    builder.append("&")
                            .append(QuestionnaireResponse.SP_AUTHORED)
                            .append("=")
                            .append(authoredParam.getValueAsQueryToken(fhirContext));
                }
            }
            return client.search().byUrl(builder.toString()).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        }
    }

    @Search(queryName = "searchBySubjectIdentifier")
    public Bundle searchBySubjectIdentifier(HttpServletRequest theRequest,
                                 HttpServletResponse theResponse,
                                 @RequiredParam(name="subject-identifier") TokenParam subject,
                                 @OptionalParam(name = QuestionnaireResponse.SP_AUTHORED) DateRangeParam authored) {
        try{
            logger.debug("searchBySubjectIdentifier:" +  subject.getValueAsQueryToken(fhirContext));
            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.OUTCOME_SERVICE_URL.getValue(), fhirContext);
            StringBuilder builder = new StringBuilder(ServiceVariables.OUTCOME_SERVICE_URL.getValue()+"/QuestionnaireResponse?_query=searchBySubjectIdentifier&subject-identifier="
                    +subject.getValueAsQueryToken(fhirContext));
            if(authored != null && !authored.isEmpty()){
                for (DateParam authoredParam : authored.getValuesAsQueryTokens()){
                    builder.append("&")
                            .append(QuestionnaireResponse.SP_AUTHORED)
                            .append("=")
                            .append(authoredParam.getValueAsQueryToken(fhirContext));
                }
            }

            return client.search().byUrl(builder.toString()).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        }
    }
}
