package dk.s4.microservices.employeebff.provider.stub;

import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.server.IResourceProvider;
import dk.s4.microservices.employeebff.ServiceVariables;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Practitioner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FHIRPractitionerResourceProviderStub implements IResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(FHIRPractitionerResourceProviderStub.class);

    private Practitioner practitioner = null;

    public FHIRPractitionerResourceProviderStub() { createStubData(); }

    private void createStubData() {
        this.practitioner = new Practitioner()
            .addIdentifier(new Identifier()
                    .setSystem(ServiceVariables.OFFICIAL_OBSERVATIONDEFINITION_IDENTIFIER_SYSTEM.getValue())
                    .setValue("empl0105"))
            .addName(new HumanName()
                    .addGiven("Nancy")
                    .addGiven("Ann")
                    .addGiven("Test")
                    .setFamily("Berggreen")
            );
    }

    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<Practitioner> getResourceType() {
        return Practitioner.class;
    }

    /**
     * Returns a FHIR Practitioner resource containing information about the currently logged in user.
     *
     * If no user is currently logged in, an error will be thrown
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response

     * @return A FHIR Practitioner resource
     */
    @Search(queryName = "getMe")
    public Bundle getMe(HttpServletRequest theRequest,
                        HttpServletResponse theResponse) {
        Bundle retVal = new Bundle().addEntry(new Bundle.BundleEntryComponent().setResource(this.practitioner));
        return retVal;
    }
}
