package dk.s4.microservices.employeebff.provider.stub;

import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.param.DateParam;
import ca.uhn.fhir.rest.param.ReferenceParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FHIRObservationResourceProviderStub implements IResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(FHIRObservationResourceProviderStub.class);

    private List<Observation> observations = new ArrayList<>();

    public FHIRObservationResourceProviderStub() {
        createStubData();
    }

    private void createStubData() {
        Observation o1 = new Observation().setValue(new StringType("Test 1"));
        o1.setId("test1");
        Observation o2 = new Observation().setValue(new StringType("Test 2"));
        o2.setId("test2");

        observations.add(o1);
        observations.add(o2);
    }

    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<Observation> getResourceType() {
        return Observation.class;
    }

    /**
     * Creates a Observation resource - a template for the telemedical care for a group of patients sharing a certain
     * set of symptoms or a particular condition.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
     * @param thePlanDef The Observation to create
     * @return MethodOutcome
     */
    @Create
    public MethodOutcome create(HttpServletRequest theRequest,
                                              HttpServletResponse theResponse,
                                              @ResourceParam Observation thePlanDef) {
        logger.debug("create");

        String uuid = UUID.randomUUID().toString();
        String identifierStr = "urn:ietf:rfc:3986" + "|" + "urn:uuid:" + uuid;
        thePlanDef.addIdentifier().setSystem("urn:ietf:rfc:3986").setValue("urn:uuid:" + uuid);

        MethodOutcome retVal = new MethodOutcome();
        retVal.setCreated(true);
        retVal.setResource(thePlanDef);

        retVal.setId(new IdType(identifierStr));

        OperationOutcome operationOutcome = new OperationOutcome();
        operationOutcome.addIssue().
                setDiagnostics("Successfully updated ressource with identifier = " + identifierStr).
                setSeverity(OperationOutcome.IssueSeverity.INFORMATION);
        retVal.setOperationOutcome(operationOutcome);

        theResponse.addHeader("Location", identifierStr);
        theResponse.setStatus(201); //Created

        return retVal;

    }

    /**
     * Updates an existing Observation resource.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http request
     * @param theObservation Observation to update. Must have an identifier.
     * @param conditional Required conditional url pointing to the identifier of the resource to update. Must have form
     *                    {@literal Observation?identifier=<system>|<value>}.
     * @return MethodOutcome
     */
    @Update
    public MethodOutcome update(HttpServletRequest theRequest,
                                           HttpServletResponse theResponse,
                                           @ResourceParam Observation theObservation,
                                           @ConditionalUrlParam String conditional) {
        logger.debug("update, conditional = " + conditional);

        MethodOutcome retVal = new MethodOutcome();
        retVal.setResource(theObservation);

        Identifier questIdent = theObservation.getIdentifier().stream()
                .filter(identifier -> !identifier.getSystem().isEmpty())
                .findAny()
                .orElse(null);
        if (questIdent == null) {
            throw new UnprocessableEntityException("Observation must have an identifier");
        }

        String location = questIdent.getSystem() + "|" + questIdent.getValue();
        retVal.setId(new IdType(location));

        OperationOutcome operationOutcome = new OperationOutcome();
        operationOutcome.addIssue().
                setDiagnostics("Successfully updated ressource with identifier = " + location).
                setSeverity(OperationOutcome.IssueSeverity.INFORMATION);
        retVal.setOperationOutcome(operationOutcome);

        theResponse.addHeader("Location", location);
        theResponse.setStatus(200);

        return retVal;
    }

    /**
     * Returns Bundle of Observations associated with the department of the currently logged in user.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http request
     * @param subject Filter search by subject of the questionnaire response (optional). If you specify this and don't
     *                specify {@code basedOn} you will get all responses - also potentially from inactive CarePlans.
     * @param based_on Filter search by CarePlan that response was submitted in the context of
     * @param code of the observation type. Speficy in IEEE MDC system, urn:iso:std:iso:11073:10101
     * @param date Filter search by the date the response was made
     * @param interpretation Filter by interpretation. Use ObservationInterpretationCodes with one of the following
     *                       values: normal, abnormal, pathological.
     *
     * @return Bundle of Observation resources
     *
     * @see <a href="https://rtmms.nist.gov/rtmms/index.htm#!rosetta">IEEE MDC</a>
     * @see <a href="http://hl7.org/fhir/2018Sep/valueset-observation-interpretation.html">ObservationInterpretationCodes</a>
     */
    @Search(queryName = "search")
    public Bundle search(HttpServletRequest theRequest,
                                  HttpServletResponse theResponse,
                                  @OptionalParam(name=Observation.SP_SUBJECT) ReferenceParam subject,
                                  @OptionalParam(name=Observation.SP_BASED_ON) ReferenceParam based_on,
                                  @OptionalParam(name=Observation.SP_CODE) TokenParam code,
                                  @OptionalParam(name=Observation.SP_DATE) DateParam date,
                                  @OptionalParam(name="interpretation") TokenParam interpretation) {
        logger.debug("search");

        Bundle bundle = new Bundle();
        bundle.addEntry().setResource(observations.get(0));
        bundle.addEntry().setResource(observations.get(1));

        return bundle;
    }

    /**
     * Get Observation resource by Identifier.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http request
     * @param identifier Identifier of an Observation to search for (required)
     * @return Observation with matching Identifier or null if not found
     */
    @Search(queryName = "getByIdentifier")
    public Observation getByIdentifier(HttpServletRequest theRequest,
                                       HttpServletResponse theResponse,
                                       @RequiredParam(name=Observation.SP_IDENTIFIER) TokenParam identifier) {

        return observations.get(0);
    }

}
