package dk.s4.microservices.employeebff.provider.stub;

import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.DateParam;
import ca.uhn.fhir.rest.param.ReferenceParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.QuestionnaireResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

public class FHIRQuestionnaireResponseResourceProviderStub implements IResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(FHIRQuestionnaireResourceProviderStub.class);

    private List<QuestionnaireResponse> questionnaireResponseList = new ArrayList<>();

    public FHIRQuestionnaireResponseResourceProviderStub() {
        createStubData();
    }

    private void createStubData() {
        QuestionnaireResponse qr1 = new QuestionnaireResponse();
        qr1.setId("test1");
        qr1.setStatus(QuestionnaireResponse.QuestionnaireResponseStatus.COMPLETED);
        QuestionnaireResponse qr2 = new QuestionnaireResponse();
        qr2.setId("test2");
        qr2.setStatus(QuestionnaireResponse.QuestionnaireResponseStatus.COMPLETED);
        questionnaireResponseList.add(qr1);
        questionnaireResponseList.add(qr2);
    }

    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<QuestionnaireResponse> getResourceType() {
        return QuestionnaireResponse.class;
    }

    /**
     * Returns Bundle of QuestionnaireResponses associated with the department of the currently logged in user.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
     * @param acknowledgement_status Filter search by acknowledgement status of questionnaire response (optional).
     *                              Parameter is a TaskStatus code with possible values restricted to: "requested",
     *                              "in-progress", "completed", "entered-in-error"
     * @param subject Filter search by subject of the questionnaire response (optional). If you specify this and don't
     *                specify {@code basedOn} you will get all responses - also potentially from inactive CarePlans.
     * @param based_on Filter search by CarePlan that response was submitted in the context of
     * @param authored Filter search by the date the response was made
     *
     * @return Bundle of QuestionnaireResponse resources
     *
     */
    @Search(queryName = "search")
    public Bundle search(HttpServletRequest theRequest,
                                              HttpServletResponse theResponse,
                                              @OptionalParam(name="acknowledgement-status") TokenParam acknowledgement_status,
                                              @OptionalParam(name=QuestionnaireResponse.SP_SUBJECT) ReferenceParam subject,
                                              @OptionalParam(name=QuestionnaireResponse.SP_BASED_ON) ReferenceParam based_on,
                                              @OptionalParam(name=QuestionnaireResponse.SP_AUTHORED)DateParam authored) {
        logger.debug("search");

        Bundle bundle = new Bundle();
        bundle.addEntry().setResource(questionnaireResponseList.get(0));
        bundle.addEntry().setResource(questionnaireResponseList.get(1));

        return bundle;

    }

    /**
     * Search for QuestionnaireResponse by Identifier.
     *
     * Example invocation: http://fhir.example.com/QuestionnaireResponse?identifier=urn:oid:1.2.208.176.1.1|373241000016009
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
     * @param identifier Identifier of a QuestionnaireResponse to search for.
     * @return QuestionnaireResponse with matching Identifier or null if not found
     */
    @Search(queryName = "getByidentifier")
    public QuestionnaireResponse getByIdentifier(HttpServletRequest theRequest,
                                                    HttpServletResponse theResponse,
                                                    @RequiredParam(name=QuestionnaireResponse.SP_IDENTIFIER)TokenParam identifier) {
        logger.debug("getByIdentifier");

        return questionnaireResponseList.get(0);
    }

    /**
     * Searches for the latest questionnaire response received from a patient
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
     * @param subject Filter search by Patient (required)
     *
     * @return QuestionnaireResponse resource
     */
    @Search(queryName = "getLatest")
    public QuestionnaireResponse getLatest(HttpServletRequest theRequest,
                                                                HttpServletResponse theResponse,
                                                                @RequiredParam(name=QuestionnaireResponse.SP_SUBJECT) ReferenceParam subject) {
        logger.debug("getByIdentifier");

        return  questionnaireResponseList.get(0);
    }
}
