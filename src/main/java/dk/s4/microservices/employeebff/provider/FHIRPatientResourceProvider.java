package dk.s4.microservices.employeebff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.exceptions.FhirClientConnectionException;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.employeebff.utils.OrganizationQueryParameter;
import dk.s4.microservices.messaging.ReplyEventProcessor;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.fhir.FhirBaseBFFResourceProvider;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Patient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Resource provider for the FHIR Patient resources,
 *
 */
public class FHIRPatientResourceProvider extends FhirBaseBFFResourceProvider {
    private static final Logger logger = LoggerFactory.getLogger(FHIRPatientResourceProvider.class);
    private OrganizationQueryParameter parameters;

    public FHIRPatientResourceProvider(FhirContext fhirContext,
            UserContextResolverInterface userContextResolver,
            MyFhirClientFactory clientFactory,
            KafkaEventProducer kafkaCommandProducer,
            ReplyEventProcessor replyEventProcessor,
                                       OrganizationQueryParameter parameters) {
        super(fhirContext, userContextResolver, clientFactory, kafkaCommandProducer, replyEventProcessor);
        this.parameters = parameters;
    }

    @Override
    protected Identifier identifierForResource(IBaseResource resource) {
        return ((Patient)resource).getIdentifier().stream()
                .filter(ident -> ident.getSystem().equals(ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue()))
                .findAny()
                .orElse(null);
    }

    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<Patient> getResourceType() {
        return Patient.class;
    }

    @Create
    public MethodOutcome createPatient(HttpServletRequest theRequest,
                                       HttpServletResponse theResponse,
                                       @ResourceParam Patient thePatient) {
        logger.debug("Create Patient");
        return createOrUpdate(theRequest,theResponse,thePatient,Topic.Operation.Create);
    }

    @Update
    public MethodOutcome updatePatient(HttpServletRequest theRequest,
                                       HttpServletResponse theResponse,
                                       @ResourceParam Patient thePatient) {
        logger.debug("Update Patient");
        return createOrUpdate(theRequest,theResponse,thePatient,Topic.Operation.Update);
    }

    /**
     * Get patients assigned to department of the employee currently logged in.
     *
     * @return a Bundle of Patient resources
     */
    @Search()
    public Bundle getMyPatients(HttpServletRequest theRequest,
                                HttpServletResponse theResponse) {
        try {
            logger.debug("getMyPatients");
            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.PATIENTCARE_SERVICE_URL.getValue(), fhirContext);

            String searchUrl = ServiceVariables.PATIENTCARE_SERVICE_URL.getValue() + "/Patient?_query=getConnectedToOrganization&authorIdentifier="
                    + parameters.createFamilyFrom(clientFactory,userContextResolver.getFHIRUserOrganization(theRequest));

            return client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        } catch (UserContextResolverInterface.UserContextResolverException e) {
            logger.error(e.getMessage(), e);
            throw new AuthenticationException(e.getMessage(), e);
        }
    }

    /**
     * Search for Patient by Identifier.
     *
     * Example invokation: http://fhir.example.com/Patient?identifier=urn:oid:1.2.208.176.1.2|2512489996
     *
     * @param theIdentifier Identifier of a Patient to search for.
     * @return Patient with matching Identifier or null if not found
     */
    @Search(queryName = "getPatientByIdentifier")
    public Bundle getPatientByIdentifier(@RequiredParam(name=Patient.SP_IDENTIFIER) TokenParam theIdentifier) {
        try {
            logger.debug("getPatientByIdentifier");
            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.PATIENTCARE_SERVICE_URL.getValue(), fhirContext);
            String searchUrl = ServiceVariables.PATIENTCARE_SERVICE_URL.getValue() + "/Patient?_query=searchByIdentifier&identifier=" + theIdentifier.getValue();
            Bundle bundle = client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
            return bundle;
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        }
    }
}
