package dk.s4.microservices.employeebff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.exceptions.FhirClientConnectionException;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.param.DateParam;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.employeebff.utils.OrganizationQueryParameter;
import dk.s4.microservices.messaging.ReplyEventProcessor;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.fhir.FhirBaseBFFResourceProvider;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface.UserContextResolverException;
import org.apache.commons.text.StringEscapeUtils;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class that represents a resource provider for the FHIR Observation resource,
 * which supports the operations for:
 * 
 * 1) get a list of observations that belongs to a given patient
 * 2) get a specific observation resource 
 * 3) get a list of historical observations since a given start date (NOTE! Not tested)
 * 4) get a list of observations with a given MDC code.
 * 
 */
public class FHIRObservationResourceProvider extends FhirBaseBFFResourceProvider {
	private static final Logger logger = LoggerFactory.getLogger(FHIRObservationResourceProvider.class);
	private final OrganizationQueryParameter parameters;


	/**
	 * Constructor
	 *
	 * @param fhirContext The (typically singleton) {@link ca.uhn.fhir.context.FhirContext} instance of the server
	 * @param userContextResolver For resolving user fhirContext information.
	 * @param clientFactory Factory for getting HAPI FHIR client instances
	 */
	public FHIRObservationResourceProvider(FhirContext fhirContext,
			UserContextResolverInterface userContextResolver,
			MyFhirClientFactory clientFactory,
			KafkaEventProducer kafkaCommandProducer,
			ReplyEventProcessor replyEventProcessor,
										   OrganizationQueryParameter parameters) {
		super(fhirContext, userContextResolver, clientFactory, kafkaCommandProducer, replyEventProcessor);
		this.parameters = parameters;
	}

	@Override
	protected Identifier identifierForResource(IBaseResource resource) {
		return ((Observation) resource).getIdentifier().stream()
				.filter(identifier -> identifier.getSystem().equals(ServiceVariables.OFFICIAL_OBSERVATION_IDENTIFIER_SYSTEM.getValue()))
				.findAny()
				.orElse(null);
	}

	/**
	 * Returns the type of resource this provider provides.
	 */
	@Override
	public Class<Observation> getResourceType() {
		return Observation.class;
	}

	/**
	 * Updates an existing Observation resource. The status field is required to be 'amended'.
	 *
	 * @param theRequest Incoming http request
	 * @param theResponse outgoing http response
	 * @param theObservation Observation resource to update
	 * @return MethodOutcome
	 */
	@Update
	public MethodOutcome update(HttpServletRequest theRequest,
								HttpServletResponse theResponse,
								@ResourceParam Observation theObservation) {
		logger.debug("update Observation");

		if (!isAmended(theObservation)){
			throw ExceptionUtils.fatalFormatException("The Observation must have status a amended");
		}
		MethodOutcome methodOutcome = createOrUpdate(theRequest, theResponse, theObservation, Topic.Operation.Update);
		return methodOutcome;
	}

	boolean isAmended(Observation theObservation){
		Observation.ObservationStatus status = theObservation.getStatus();
		return status != null && status.equals(Observation.ObservationStatus.AMENDED);
	}

	/**
	 *  Returns all observations pertaining to a given care plan.
	 * @param theRequest
	 * @param theResponse
	 * @param basedOn
	 * @return
	 */


	@Search(queryName = "searchByBasedOnIdentifier")
	public Bundle searchByBasedOnIdentifier(HttpServletRequest theRequest,
											HttpServletResponse theResponse,
											@RequiredParam(name="basedOn-identifier") TokenParam basedOn) {
		try{
			logger.debug("searchByBasedOnIdentifier:" +  basedOn.getValueAsQueryToken(fhirContext));
			IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.OUTCOME_SERVICE_URL.getValue(), fhirContext);
			StringBuilder builder = new StringBuilder(ServiceVariables.OUTCOME_SERVICE_URL.getValue()+"/Observation?_query=searchByBasedOnIdentifier&based-on="
					+basedOn.getValueAsQueryToken(fhirContext));

			return client.search().byUrl(builder.toString()).returnBundle(Bundle.class).execute();
		} catch (FhirClientConnectionException e) {
			throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
		}
	}


	/**
	 * Returns all observations pertaining to a given patient, for the specified period. If no period is specified, every observation will be returned.
	 *
	 * @param theRequest the request.
	 * @param theResponse the response.
	 * @param subject the identifier of the patient.
	 * @param date the period from within which one wish to see observations.
	 * @return Bundle of observations.
	 */
	@Search(queryName = "getForPatient")
	public Bundle getForPatient(HttpServletRequest theRequest,
								HttpServletResponse theResponse,
								@RequiredParam(name= Observation.SP_SUBJECT) TokenParam subject,
								@OptionalParam(name = Observation.SP_DATE) DateRangeParam date) {
		logger.trace("getForPatient");

		try {
			IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.OUTCOME_SERVICE_URL.getValue(), fhirContext);
			StringBuilder builder = new StringBuilder(ServiceVariables.OUTCOME_SERVICE_URL.getValue()+"/Observation?_query=searchBySubjectIdentifier&subject-identifier="
					+subject.getValueAsQueryToken(fhirContext));
			if(date != null && !date.isEmpty()){
				for (DateParam dateParam : date.getValuesAsQueryTokens()){
					builder.append("&")
							.append(Observation.SP_DATE)
							.append("=")
							.append(dateParam.getValueAsQueryToken(fhirContext));
				}
			}
			IQuery iQuery = client.search().byUrl(builder.toString());
			iQuery.returnBundle(Bundle.class);
			return (Bundle) iQuery.returnBundle(Bundle.class).execute();
		} catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
		}
	}

	/**
	 * Returns all observations of a certain type pertaining to a given patient, for the specified period. If no period is specified, every observation will be returned.
	 *
	 * @param theRequest the request.
	 * @param theResponse the response.
	 * @param subject the identifier of the patient.
	 * @param code the MDC code specifying which type of observation one is interested in.
	 * @param date the period from within which one wish to see observations .
	 * @return Bundle of observations.
	 */

	@Search(queryName = "getForPatientWithCode")
	public Bundle getForPatientWithCode(HttpServletRequest theRequest,
								HttpServletResponse theResponse,
								@RequiredParam(name= Observation.SP_SUBJECT) TokenParam subject,
								@RequiredParam(name = Observation.SP_CODE) TokenParam code,
								@OptionalParam(name = Observation.SP_DATE) DateRangeParam date) {
		logger.trace("getForPatient");

		try {
			IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.OUTCOME_SERVICE_URL.getValue(), fhirContext);
			StringBuilder builder = new StringBuilder(ServiceVariables.OUTCOME_SERVICE_URL.getValue()+"/Observation?_query=searchBySubjectIdentifier" +
					"&subject-identifier="+subject.getValueAsQueryToken(fhirContext)
					+"&code=" + code.getValueAsQueryToken(fhirContext));
			if(date != null && !date.isEmpty()){
				for (DateParam dateParam : date.getValuesAsQueryTokens()){
					builder.append("&")
							.append(Observation.SP_DATE)
							.append("=")
							.append(dateParam.getValueAsQueryToken(fhirContext));
				}
			}
			return client.search().byUrl(builder.toString()).returnBundle(Bundle.class).execute();
		} catch (FhirClientConnectionException e) {
			throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
		}
	}

	@Search(queryName = "getByIdentifier")
	public Bundle getByIdentifier(HttpServletRequest theRequest,
								  HttpServletResponse theResponse,
								  @RequiredParam(name= Observation.SP_IDENTIFIER) TokenParam identifier) {
		logger.trace("getByIdentifier");

		try{
			IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.OUTCOME_SERVICE_URL.getValue(), fhirContext);
			String url = ServiceVariables.OUTCOME_SERVICE_URL.getValue()+"/Observation?_query=getByIdentifier" +
					"&identifier="+identifier.getValueAsQueryToken(fhirContext);
			return client.search().byUrl(url).returnBundle(Bundle.class).execute();
		} catch (FhirClientConnectionException e) {
			throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
		}
	}

	/**
	 * Returns bundle of Observations, that are referenced by any questionnaires that are the focus of an acknowledgement task
	 * with the status provided.
	 *
	 * Example invocation:
	 * http://fhir.example.com/Observation?_query=getForAcknowledgement?status=requested
	 *
	 * @param status the status of the task
	 * @return A bundle containing all observations that match the query.
	 */

	@Search(queryName = "getForAcknowledgement")
	public Bundle getForAcknowledgement(HttpServletRequest theRequest,
								  HttpServletResponse theResponse,
								  @RequiredParam(name= Observation.SP_STATUS) TokenParam status) {
		logger.trace("getForAcknowledgement");

		try{
			IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.OUTCOME_SERVICE_URL.getValue(), fhirContext);
			String searchUrl =
				String.format("/Observation?_query=getObservationsForAcknowledgement&status=%s" +
						"&owner=%s",
					status.getValue(),
					parameters.createFamilyFrom(clientFactory,userContextResolver.getFHIRUserOrganization(theRequest)));
			return client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
		} catch (FhirClientConnectionException | UserContextResolverException e) {
			throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
		}
	}

	/**
	 * Returns bundle of Observations that are the focus of tasks with the code and the status provided as
	 * arguments.
	 *
	 * Example invocation:
	 * http://fhir.example.com/Observation?_query=getFocusForTasksWithCodeAndStatus?code=acknowledgement&status=requested
	 *
	 * @param code The code value of the Tasks resources to search for (required)
	 * @param status The status of the Tasks resources to search for(required)
	 *
	 * @return A bundle containing all Observations that match the query.
	 */
	@Search(queryName = "getFocusForTasksWithCodeAndStatus")
	public Bundle getFocusForTasksWithCodeAndStatus(
			HttpServletRequest request,
			HttpServletResponse response,
			RequestDetails requestDetails,
			@RequiredParam(name= Task.SP_CODE) TokenParam code,
			@RequiredParam(name=Task.SP_STATUS) TokenParam status
	) {
		try {
			logger.debug("get Observations with code {} and status {}", code.getValue(), status.getValue());

			// Perform a search for a list of questionnaire response resources
			IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.OUTCOME_SERVICE_URL.getValue(), fhirContext);

			String searchUrl =
					String.format("/Observation?_query=getFocusForTasksWithCodeAndStatus&code=%s&status=%s" +
									"&owner=%s",
							code.getValue(),
							status.getValue(),
							parameters.createFamilyFrom(clientFactory,userContextResolver.getFHIRUserOrganization(request)));

			Bundle result = client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
			if (result.hasLink()) {
				for (Bundle.BundleLinkComponent link: result.getLink()) {
					if (link.hasUrl()) {
						String url = StringEscapeUtils
								.unescapeHtml4(ServiceVariables.SERVER_BASE_URL.getValue() +
													   "/" + getResourceType().getSimpleName() +
													   "/" + request.getQueryString()
											  );

						link.setUrl(url);
					}
				}
			}

			logger.debug("Observation resources returned from the Outcome service: Number of resources found = {}",
						 result.getEntry().size());

			return result;
		} catch (FhirClientConnectionException e) {
			throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
		} catch (UserContextResolverInterface.UserContextResolverException e) {
			logger.error(e.getMessage(), e);
			throw new AuthenticationException(e.getMessage(), e);
		}
	}
}
