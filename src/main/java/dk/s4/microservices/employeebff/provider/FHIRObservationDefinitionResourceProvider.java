package dk.s4.microservices.employeebff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.exceptions.FhirClientConnectionException;
import ca.uhn.fhir.rest.param.TokenParam;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.messaging.ReplyEventProcessor;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.fhir.FhirBaseBFFResourceProvider;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.ObservationDefinition;
import org.hl7.fhir.r4.model.Questionnaire;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FHIRObservationDefinitionResourceProvider  extends FhirBaseBFFResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(FHIRObservationDefinitionResourceProvider.class);

    /**
     * Instantiate FHIRResource Provider for a certain class of resource.
     *
     */
    public FHIRObservationDefinitionResourceProvider(FhirContext fhirContext,
            UserContextResolverInterface userContextResolver,
            MyFhirClientFactory clientFactory,
            KafkaEventProducer kafkaCommandProducer,
            ReplyEventProcessor replyEventProcessor) {
        super(fhirContext, userContextResolver, clientFactory, kafkaCommandProducer, replyEventProcessor);
    }

    @Override
    protected Identifier identifierForResource(IBaseResource resource) {
        return ((ObservationDefinition) resource).getIdentifier().stream()
                .filter(identifier -> identifier.getSystem().equals(ServiceVariables.OFFICIAL_OBSERVATIONDEFINITION_IDENTIFIER_SYSTEM.getValue()))
                .findAny()
                .orElse(null);
    }


    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<ObservationDefinition> getResourceType() {
        return ObservationDefinition.class;
    }

    /**
     * Updates an existing Questionnaire resource. Only the status field is allowed to change.
     *
     * @param theRequest Incoming http request
     * @param theResponse outgoing http response
     * @param theObservationDefinition the ObservationDefinition resource to update
     * @param conditional {@literal Required conditional url pointing to the identifier of the resource to update. Must have form
     *                    Questionnaire?identifier=<system>|<value>.}
     * @return MethodOutcome
     */
    @Update
    public MethodOutcome update(HttpServletRequest theRequest,
                                HttpServletResponse theResponse,
                                @ResourceParam ObservationDefinition theObservationDefinition,
                                @ConditionalUrlParam String conditional) {
        logger.debug("update, conditional = " + conditional);


        MethodOutcome methodOutcome = createOrUpdate(theRequest, theResponse, theObservationDefinition, Topic.Operation.Update);
        return methodOutcome;

    }

    /**
     * Creates a questionnaire resource on the object-definition-service and returns a MethodOutcome resource indicating whether the
     * action was successful.
     *
     * @param theRequest The incoming request
     * @param theResponse The outgoing response
     * @param theObservationDefinition The questionnaire to create
     *
     * @return A MethodOutcome resource indicating whether the action was successful.
     */
    @Create
    public MethodOutcome create(HttpServletRequest theRequest,
                                HttpServletResponse theResponse,
                                @ResourceParam ObservationDefinition theObservationDefinition) {

        MethodOutcome methodOutcome = createOrUpdate(theRequest, theResponse, theObservationDefinition, Topic.Operation.Create);
        return methodOutcome;

    }

    @Search(queryName = "getById")
    public Bundle getById(HttpServletRequest theRequest,
                          HttpServletResponse theResponse,
                          @RequiredParam(name="_id") TokenParam id) {
        try {
            logger.debug("getById");

            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.OUTCOMEDEFINITION_SERVICE_URL.getValue(), fhirContext);
            String searchUrl = ServiceVariables.OUTCOMEDEFINITION_SERVICE_URL.getValue() +"/ObservationDefinition?_id=" + id.getValue();
            Bundle bundle = client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();

            return bundle;
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        }
    }

    /**
     * Search for ObservationDefinition by Identifier.
     * <p>
     * Example invocation: http://fhir.example.com/ObservationDefinition?identifier=urn:ietf:rfc:3986|cb0a63f8-b595-4882-a1d2-71eba429d56e
     *
     * @param identifier Identifier of a ObservationDefinition to search for.
     * @return ObservationDefinition with matching Identifier or null if not found
     */
    @Search(queryName = "getByIdentifier")
    public Bundle getByIdentifier(HttpServletRequest theRequest,
                                         HttpServletResponse theResponse,
                                         @RequiredParam(name = Questionnaire.SP_IDENTIFIER) TokenParam identifier) {
        try {
            logger.debug("getByIdentifier");

            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.OUTCOMEDEFINITION_SERVICE_URL.getValue(), fhirContext);
            String searchUrl = ServiceVariables.OUTCOMEDEFINITION_SERVICE_URL.getValue() + "/ObservationDefinition?_identifier=" + identifier.getSystem() + "|" + identifier.getValue();
            return client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();

        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        }
    }
}
