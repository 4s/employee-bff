package dk.s4.microservices.employeebff.provider.stub;

import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.mock.MockKafkaEventProducer;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import dk.s4.microservices.messaging.MessagingUtils;
import dk.s4.microservices.messaging.ReplyFuture;
import dk.s4.microservices.employeebff.utils.Utils;
import dk.s4.microservices.employeebff.servlet.EmployeeBff;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
public class FHIRPlanDefinitionResourceProviderStub implements IResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(FHIRPlanDefinitionResourceProviderStub.class);

    private List<PlanDefinition> planDefList = new ArrayList<>();
    private Organization organization;
    private MockKafkaEventProducer mockKafkaCommandProducer;

    public FHIRPlanDefinitionResourceProviderStub() throws KafkaInitializationException, IOException {
        createStubData();
        mockKafkaCommandProducer = new MockKafkaEventProducer(ServiceVariables.SERVICE_NAME.getValue());
    }

    private void createStubData() throws IOException {
        String resourceString = Utils.createResourceString("PlanDefinition.json");PlanDefinition pd1 = EmployeeBff.getMyFhirContext().newJsonParser().parseResource(PlanDefinition.class, resourceString);
        PlanDefinition pd2 = (PlanDefinition) new PlanDefinition().setTitle("PPROM").setId("test2");
        planDefList.add(pd1);
        planDefList.add(pd2);
    }

    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<PlanDefinition> getResourceType() {
        return PlanDefinition.class;
    }

    /**
     * Creates a PlanDefinition resource - a template for the telemedical care for a group of patients sharing a certain
     * set of symptoms or a particular condition.
     * 
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
     * @param thePlanDef The PlanDefinition to create
     * @return MethodOutcome
     */
    @Create
    public MethodOutcome create(HttpServletRequest theRequest,
                                              HttpServletResponse theResponse,
                                              @ResourceParam PlanDefinition thePlanDef) {
        
        String correlationId = MessagingUtils.verifyOrCreateId(null);
        String transactionId = UUID.randomUUID().toString();
        MDC.put("correlationId", correlationId);
        MDC.put("transactionId", transactionId);logger.debug("create");
        
        Identifier thePlanDefIdentifier = thePlanDef.getIdentifier().stream()
                .filter(identifier -> !identifier.getSystem().isEmpty())
                .findAny()
                .orElse(null);
        if (thePlanDefIdentifier == null) {
            throw new UnprocessableEntityException("PlanDefinition must have an identifier with system = " +
            "urn:ietf:rfc:3986");
        }

        ReplyFuture replyFuture = EmployeeBff.replyEventProcessor.addReplyFuture(transactionId);
	    Message message = new Message().
                setSender(ServiceVariables.SERVICE_NAME.getValue()).
                setBodyCategory(Message.BodyCategory.FHIR).
				setBodyType("PlanDefinition").
                setContentVersion(ServiceVariables.FHIR_VERSION.getValue()).
                setCorrelationId(correlationId).
                setTransactionId(transactionId).
                setSecurity(theRequest.getHeader("Authorization")).
                setBody(EmployeeBff.getMyFhirContext().newJsonParser().encodeResourceToString(thePlanDef));

        String preferHeader = theRequest.getHeader("Prefer");
        if (preferHeader != null) {
            message.setPrefer(Message.preferFromString(preferHeader));
        }

        Topic topic = new Topic().
                setOperation(Topic.Operation.Create).
                setDataCategory(Topic.Category.FHIR).
                setDataType(thePlanDef.fhirType());
        mockKafkaCommandProducer.sendMessage(topic, message);

        MethodOutcome retVal = new MethodOutcome();
        try {
            Message reply = replyFuture.get(Long.parseLong(ServiceVariables.ASYNC_RESPONSE_TIMEOUT_MS.getValue()),
                    TimeUnit.MILLISECONDS);

            theResponse.addHeader("Location", thePlanDefIdentifier.getSystem() + "|" + thePlanDefIdentifier.getValue());
            theResponse.setStatus(201); //Created

            if (reply.getBodyType() != null) {
                if (reply.getBodyType().equals("OperationOutcome")) {
                    OperationOutcome operationOutcome = EmployeeBff.getMyFhirContext().newJsonParser().
                            parseResource(OperationOutcome.class, reply.getBody());
                    if (operationOutcome.getIssue().get(0).getSeverity() == OperationOutcome.IssueSeverity.INFORMATION ||
                            operationOutcome.getIssue().get(0).getSeverity() == OperationOutcome.IssueSeverity.WARNING) {

                        operationOutcome.getIssue().get(0).setDiagnostics("Successfully created ressource with identifier = " +
                                thePlanDefIdentifier.getSystem() + "|" + thePlanDefIdentifier.getValue());
                        retVal.setCreated(true);
                        retVal.setOperationOutcome(operationOutcome);
                        //We must use identifiers - not logical id's - when communicating locations/references to resources
                        // externally:
                        retVal.setId(new IdType(thePlanDefIdentifier.getSystem() + "|" + thePlanDefIdentifier.getValue()));

                        return retVal;
                    }
                    else {
                        retVal.setOperationOutcome(operationOutcome);
                        logger.error("Create PlanDefinition failed: " + operationOutcome.getIssue().get(0).getDiagnostics());
                        throw new InternalErrorException("Create PlanDefinition failed", operationOutcome);
                    }
                } else if (reply.getBodyType().equals("PlanDefinition")){
                    PlanDefinition PlanDefinition = EmployeeBff.getMyFhirContext().newJsonParser().parseResource(PlanDefinition.class,
                            reply.getBody());
                    retVal.setCreated(true);
                    retVal.setResource(PlanDefinition);
                    //We must use identifiers - not logical id's - when communicating locations/references to resources
                    // externally:
                    retVal.setId(new IdType(thePlanDefIdentifier.getSystem() + "|" + thePlanDefIdentifier.getValue()));

                    return retVal;
                }
                else {
                    String msg = "Create resulted in unexpected resource type: " + reply.getBodyType();
                    logger.error(msg);
                    OperationOutcome operationOutcome = new OperationOutcome();
                    operationOutcome.addIssue().
                            setDiagnostics(msg).
                            setSeverity(OperationOutcome.IssueSeverity.ERROR);
                    throw new InternalErrorException(msg, operationOutcome);
                }
            }
            else {
                //Notice: We set a non-null retVal no matter what the "Prefer" header says. Although, if "Prefer:
                // return=minimal" was stated we ought not to return a body. However, this is not supported by HAPI.
                OperationOutcome operationOutcome = new OperationOutcome();
                operationOutcome.addIssue().
                        setDiagnostics("Successfully created resource with identifier = " +
                            thePlanDefIdentifier.getSystem() + "|" + thePlanDefIdentifier.getValue()).
                        setSeverity(OperationOutcome.IssueSeverity.INFORMATION);

                retVal.setOperationOutcome(operationOutcome);
                retVal.setCreated(true);
                //We must use identifiers - not logical id's - when communicating locations/references to resources
                // externally:
                retVal.setId(new IdType(thePlanDefIdentifier.getSystem() + "|" + thePlanDefIdentifier.getValue()));

                return retVal;
            }

        } catch (TimeoutException | InterruptedException e) {
            logger.error("Create PlanDefinition timed out or was interrupted", e);
            OperationOutcome operationOutcome = new OperationOutcome();
            operationOutcome.addIssue().
                    setDiagnostics("Timeout or interrupt getting response from internal services").
                    setSeverity(OperationOutcome.IssueSeverity.ERROR);
            throw new InternalErrorException("Create PlanDefinition failed", operationOutcome);
        } finally {
            //Remove the future when we are done processing
            EmployeeBff.replyEventProcessor.removeReplyFuture(transactionId);
        }

    }

    /**
     *
     * Updates an existing PlanDefinition resource.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http request
     * @param thePlanDefinition PlanDefinition to update. Must have an identifier.
     * @param conditional Required conditional url pointing to the identifier of the resource to update. Must have form
     *                    {@literal PlanDefinition?identifier=<system>|<value>}.
     * @return MethodOutcome
     */
    @Update
    public MethodOutcome update(HttpServletRequest theRequest,
                                             HttpServletResponse theResponse,
                                             @ResourceParam PlanDefinition thePlanDefinition,
                                             @ConditionalUrlParam String conditional) {
        logger.debug("update, conditional = " + conditional);

        //TODO: If client caller ever supplies a correlation id we should extract it from HttpServletRequest
        String correlationId = MessagingUtils.verifyOrCreateId(null);
        String transactionId = UUID.randomUUID().toString();
        MDC.put("correlationId", correlationId);
        MDC.put("transactionId", transactionId);


        Identifier planDefinitionIdentifier = thePlanDefinition.getIdentifier().stream()
                .filter(identifier -> !identifier.getSystem().isEmpty())
                .findAny()
                .orElse(null);
        if (planDefinitionIdentifier == null) {
            throw new UnprocessableEntityException("PlanDefinition must have an identifier");
        }

                //Start listening for replies. Important: Do this BEFORE you send the message!
	    ReplyFuture replyFuture = EmployeeBff.replyEventProcessor.addReplyFuture(transactionId);
	    Message message = new Message().
                setSender(ServiceVariables.SERVICE_NAME.getValue()).
                setBodyCategory(Message.BodyCategory.FHIR).
				setBodyType("PlanDefinition").
                setContentVersion(ServiceVariables.FHIR_VERSION.getValue()).
                setCorrelationId(correlationId).
                setTransactionId(transactionId).
                setSecurity(theRequest.getHeader("Authorization")).
                setBody(EmployeeBff.getMyFhirContext().newJsonParser().encodeResourceToString(thePlanDefinition));

        String preferHeader = theRequest.getHeader("Prefer");
        if (preferHeader != null) {
            message.setPrefer(Message.preferFromString(preferHeader));
        }

        Topic topic = new Topic().
                setOperation(Topic.Operation.Update).
                setDataCategory(Topic.Category.FHIR).
                setDataType(thePlanDefinition.fhirType());
        mockKafkaCommandProducer.sendMessage(topic, message);

        MethodOutcome retVal = new MethodOutcome();
        try {
            Message reply = replyFuture.get(Long.parseLong(ServiceVariables.ASYNC_RESPONSE_TIMEOUT_MS.getValue()),
                    TimeUnit.MILLISECONDS);

            theResponse.addHeader("Location", planDefinitionIdentifier.getSystem() + "|" + planDefinitionIdentifier.getValue());
            theResponse.setStatus(200); //Created

            if (reply.getBodyType() != null) {
                if (reply.getBodyType().equals("OperationOutcome")) {
                    OperationOutcome operationOutcome = EmployeeBff.getMyFhirContext().newJsonParser().
                            parseResource(OperationOutcome.class, reply.getBody());
                    if (operationOutcome.getIssue().get(0).getSeverity() == OperationOutcome.IssueSeverity.INFORMATION ||
                            operationOutcome.getIssue().get(0).getSeverity() == OperationOutcome.IssueSeverity.WARNING) {

                        operationOutcome.getIssue().get(0).setDiagnostics("Successfully updated ressource with identifier = " +
                                planDefinitionIdentifier.getSystem() + "|" + planDefinitionIdentifier.getValue());
                        

                        retVal.setOperationOutcome(operationOutcome);
                        //We must use identifiers - not logical id's - when communicating locations/references to resources
                        // externally:
                        retVal.setId(new IdType(planDefinitionIdentifier.getSystem() + "|" + planDefinitionIdentifier.getValue()));

                        return retVal;
                    }
                    else {
                        retVal.setOperationOutcome(operationOutcome);
                        logger.error("Update PlanDefinition failed: " + operationOutcome.getIssue().get(0).getDiagnostics());
                        throw new InternalErrorException("Update PlanDefinition failed", operationOutcome);
                    }
                } else if (reply.getBodyType().equals("PlanDefinition")){
                    PlanDefinition PlanDefinition = EmployeeBff.getMyFhirContext().newJsonParser().parseResource(PlanDefinition.class,
                            reply.getBody());

                    retVal.setResource(PlanDefinition);
                    //We must use identifiers - not logical id's - when communicating locations/references to resources
                    // externally:
                    retVal.setId(new IdType(planDefinitionIdentifier.getSystem() + "|" + planDefinitionIdentifier.getValue()));

                    return retVal;
                }
                else {
                    String msg = "Update resulted in unexpected resource type: " + reply.getBodyType();
                    logger.error(msg);
                    OperationOutcome operationOutcome = new OperationOutcome();
                    operationOutcome.addIssue().
                            setDiagnostics(msg).
                            setSeverity(OperationOutcome.IssueSeverity.ERROR);
                    throw new InternalErrorException(msg, operationOutcome);
                }
            }
            else {
                //Notice: We set a non-null retVal no matter what the "Prefer" header says. Although, if "Prefer:
                // return=minimal" was stated we ought not to return a body. However, this is not supported by HAPI.
                OperationOutcome operationOutcome = new OperationOutcome();
                operationOutcome.addIssue().
                        setDiagnostics("Successfully updated resource with identifier = " +
                                planDefinitionIdentifier.getSystem() + "|" + planDefinitionIdentifier.getValue()).
                        setSeverity(OperationOutcome.IssueSeverity.INFORMATION);

                retVal.setOperationOutcome(operationOutcome);
                //We must use identifiers - not logical id's - when communicating locations/references to resources
                // externally:
                retVal.setId(new IdType(planDefinitionIdentifier.getSystem() + "|" + planDefinitionIdentifier.getValue()));

                return retVal;
            }

        } catch (TimeoutException | InterruptedException e) {
            logger.error("Update PlanDefinition timed out or was interrupted", e);
            OperationOutcome operationOutcome = new OperationOutcome();
            operationOutcome.addIssue().
                    setDiagnostics("Timeout or interrupt getting response from internal services").
                    setSeverity(OperationOutcome.IssueSeverity.ERROR);
            throw new InternalErrorException("Update PlanDefinition failed", operationOutcome);
        } finally {
            //Remove the future when we are done processing
            EmployeeBff.replyEventProcessor.removeReplyFuture(transactionId);
        }
    }

    /**
     * Deletes a PlanDefinition based on a conditional url pointing out the identifier of the PlanDefinition to
     * delete. If the PlanDefinition is referred to by any active CarePlans an error is thrown.
     *
     * @param theId Not used, will be ignored.
     * @param theConditionalUrl Must state the identifier of the PlanDefinition to delete on the form
     *                          {@literal PlanDefinition?identifier=<system>|<value>}
     */
    @Delete()
    public void delete(@IdParam IdType theId, @ConditionalUrlParam String theConditionalUrl) {
        logger.debug("delete theConditionalUrl = " + theConditionalUrl);

        return; // can also return MethodOutcome
    }

    /**
     * Search in PlanDefinition resources assigned to the department of the employee currently logged in. If no
     * parameters are given the search returns all PlanDefinition resources assigned to the department.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
     * @param title Filter search by whole or part of title (optional)
     * @param status Filter search by Publication status (optional). Can be either "active" or "retired" from the
     *               PublicationStatus ValueSet
     * @param context Filter search by useContext.​value as CodeableConcept, i.e. as system and code (optional).
     *                    System is typically ICD10
     * @param contextText Filter search by useContext.value as CodeableConcept.text, i.e. as plain text (optional).
     * @param jurisdiction Filter search by Organizational scope of usage of the PlanDefinition. Coded with Danish
     *                     SOR register as system, OID 1.2.208.176.1.1.
     *
     * @return a Bundle of PlanDefinition resources
     *
     * @see <a href="http://hl7.org/fhir/2018Sep/valueset-publication-status.html">PublicationStatus</a>
     * @see <a href="http://hl7.org/fhir/2018Sep/metadatatypes.html#UsageContext">UsageContext</a>
     * @see <a href="https://issuetracker4s.atlassian.net/wiki/spaces/FDM/pages/47317015/FHIR+PlanDefinition">FHIR PlanDefinition</a>
     */
    //TODO: Should you be able to search for PlanDef's referencing particular questionnaires? (via action.definition)
    @Search(queryName = "search")
    public Bundle search(HttpServletRequest theRequest,
                                     HttpServletResponse theResponse,
                                     @OptionalParam(name=PlanDefinition.SP_TITLE) StringParam title,
                                     @OptionalParam(name=PlanDefinition.SP_STATUS) TokenParam status,
                                     @OptionalParam(name="context") TokenParam context,
                                     @OptionalParam(name="contextText") StringParam contextText,
                                     @OptionalParam(name=PlanDefinition.SP_JURISDICTION) TokenParam jurisdiction) {
        logger.debug("search");

        Bundle bundle = new Bundle();
        bundle.addEntry().setResource(planDefList.get(0));
        bundle.addEntry().setResource(planDefList.get(1));

        return bundle;
    }


    @Search
    public Bundle getMyPlanDefinitions(HttpServletRequest theRequest,
                                     HttpServletResponse theResponse) {

        Bundle bundle = new Bundle();
        bundle.addEntry().setResource(planDefList.get(0));
        bundle.addEntry().setResource(planDefList.get(1));

        return bundle;
    }

}
