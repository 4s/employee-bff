package dk.s4.microservices.employeebff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.api.annotation.Description;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.api.SearchTotalModeEnum;
import ca.uhn.fhir.rest.api.SortSpec;
import ca.uhn.fhir.rest.api.SummaryEnum;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.exceptions.FhirClientConnectionException;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.ReferenceAndListParam;
import ca.uhn.fhir.rest.param.TokenAndListParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.employeebff.utils.CopiedParameters;
import dk.s4.microservices.employeebff.utils.OrganizationQueryParameter;
import dk.s4.microservices.messaging.ReplyEventProcessor;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.fhir.FhirBaseBFFResourceProvider;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.Identifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

public class FHIRCarePlanResourceProvider extends FhirBaseBFFResourceProvider {
    private static final Logger logger = LoggerFactory.getLogger(FHIRCarePlanResourceProvider.class);
    private final OrganizationQueryParameter parameters;

    public FHIRCarePlanResourceProvider(FhirContext fhirContext,
            UserContextResolverInterface userContextResolver,
            MyFhirClientFactory clientFactory,
            KafkaEventProducer kafkaCommandProducer,
            ReplyEventProcessor replyEventProcessor,
                                        OrganizationQueryParameter parameters) {
        super(fhirContext, userContextResolver, clientFactory, kafkaCommandProducer, replyEventProcessor);
        this.parameters = parameters;
    }

    @Override
    protected Identifier identifierForResource(IBaseResource resource) {
        return ((CarePlan) resource).getIdentifier().stream()
                .filter(ident -> ident.getSystem().equals(ServiceVariables.OFFICIAL_CAREPLAN_IDENTIFIER_FIRST_SYSTEM.getValue()))
                .findAny()
                .orElse(null);
        //TODO: There's two
    }

    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<CarePlan> getResourceType() {
        return CarePlan.class;
    }

    /**
     * Creates a CarePlan resource
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
     * @param theCarePlan The CarePlan to create
     * @return MethodOutcome
     */
    @Create
    public MethodOutcome create(HttpServletRequest theRequest,
                                HttpServletResponse theResponse,
                                @ResourceParam CarePlan theCarePlan) {
        logger.debug("create a CarePlan");

        verifyOrCreateIdentifier(theCarePlan);

        //create the CarePlan
        MethodOutcome methodOutcome = createOrUpdate(theRequest,theResponse,theCarePlan, Topic.Operation.Create);
        return methodOutcome;

    }

    /**
     *
     * Updates an existing CarePlan resource.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http request
     * @param theCarePlan CarePlan to update. Must have an identifier.
     * @param conditional Required conditional url pointing to the identifier of the resource to update. Must have form
     *                    {@literal CarePlan?identifier=<system>|<value>}.
     * @return MethodOutcome
     */
    @Update
    public MethodOutcome update(HttpServletRequest theRequest,
                                HttpServletResponse theResponse,
                                @ResourceParam CarePlan theCarePlan,
                                @ConditionalUrlParam String conditional) {
        logger.debug("updateCarePlan, conditional = " + conditional);
        MethodOutcome methodOutcome = createOrUpdate(theRequest,theResponse,theCarePlan, Topic.Operation.Update);
        return methodOutcome;
    }

    /**
     * Retrieves the active CarePlan for Patient that is the subject of this CarePlan.
     *
     * Searches only in CarePlans that belong to the department of the currently logged in employee.
     * If no active CarePlan is found an error is returned.
     *
     * The search must be on the form:
     *
     * {@literal http://example.org/CarePlan?_query=getActiveForPatient&subject=<system>|<value>}
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
     * @param subject Who care plan is for. Must contain the identifier of Patient (required)
     *
     * @return Active CarePlan for Patient.
     */
    @Search(queryName = "getActiveForPatient")
    public Bundle getActiveForPatient(HttpServletRequest theRequest,
                                        HttpServletResponse theResponse,
                                        @RequiredParam(name=CarePlan.SP_SUBJECT) TokenParam subject) {
        try {
            logger.debug("getActiveForPatient");

            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.PATIENTCARE_SERVICE_URL.getValue(), fhirContext);
            String system = subject.getSystem();
            String value = subject.getValue();
            String searchUrl = ServiceVariables.PATIENTCARE_SERVICE_URL.getValue()+"/CarePlan?_query=getActiveForPatient&identifier="+system+"|"+value +
                    "&author=" +                     parameters.createFamilyFrom(clientFactory,userContextResolver.getFHIRUserOrganization(theRequest));
            Bundle bundle = client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();

            return bundle;
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        } catch (UserContextResolverInterface.UserContextResolverException e) {
            logger.error(e.getMessage(), e);
            throw new AuthenticationException(e.getMessage(), e);
        }
    }

    /**
     * Retrieves CarePlans pertaining to a patient.
     *
     * Searches only in CarePlans that belong to the department of the currently logged in employee.
     * If no CarePlan is found an error is returned.
     *
     * The search must be on the form:
     *
     * {@literal http://example.org/CarePlan?_query=getAllForPatient&subject=<system>|<value>}
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
     * @param subject Who care plan is for. Must contain the identifier of Patient (required)
     *
     * @return Active CarePlan for Patient.
     */
    @Search(queryName = "getAllForPatient")
    public Bundle getAllForPatient(HttpServletRequest theRequest,
                                      HttpServletResponse theResponse,
                                      @RequiredParam(name=CarePlan.SP_SUBJECT) TokenParam subject) {
        try {
            logger.debug("getAllForPatient");

            String hostname = ServiceVariables.PATIENTCARE_SERVICE_URL.getValue();
            IGenericClient client = clientFactory.getClientFromBaseUrl(hostname, fhirContext);
            String searchUrl = hostname + "/CarePlan?_query=getAllForPatient&identifier="+subject.getValueAsQueryToken(fhirContext) +
                    "&author=" + parameters.createFamilyFrom(clientFactory,userContextResolver.getFHIRUserOrganization(theRequest));

            return client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        } catch (UserContextResolverInterface.UserContextResolverException e) {
            logger.error(e.getMessage(), e);
            throw new AuthenticationException(e.getMessage(), e);
        }
    }

    /**
     * Retrieves all CarePlans that belong to the department of the currently logged in employee
     *
     * The search must be on the form:
     *
     * {@literal http://example.org/CarePlan?_query=getMyCarePlans}
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
     *
     * @return All CarePlans that belong to the department of the currently logged in employee
     */
    @Search(queryName = "getMyCarePlans")
    public Bundle getMyCarePlans(HttpServletRequest theRequest,
                                   HttpServletResponse theResponse) {
        try {
            logger.debug("getMyCarePlans");

            String hostname = ServiceVariables.PATIENTCARE_SERVICE_URL.getValue();
            IGenericClient client = clientFactory.getClientFromBaseUrl(hostname, fhirContext);
            String searchUrl = hostname + "/CarePlan?authorIdentifier=" +
                    parameters.createFamilyFrom(clientFactory,userContextResolver.getFHIRUserOrganization(theRequest));
            return client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        } catch (UserContextResolverInterface.UserContextResolverException e) {
            logger.error(e.getMessage(), e);
            throw new AuthenticationException(e.getMessage(), e);
        }
    }

    /**
     * Generic search API for retrieval of Bundles of CarePlans stored by this service
     * @param theServletRequest
     * @param theServletResponse
     * @param theRequestDetails
     * @param the_id The ID of the resource
     * @param theIdentifier the Identifier of this plan
     * @param theInstantiates_canonical Instantiates FHIR protocol or definition
     * @param theIntent proposal | plan | order | option
     * @param theStatus draft | active | suspended | completed | entered-in-error | cancelled | unknown
     * @param theLastUpdated Only return resources which were last updated as specified by the given range
     * @param theSort sort order
     * @param theCount
     * @param theSummaryMode
     * @param theSearchTotalMode
     * @return
     */

    @Search()
    public Bundle search(
            javax.servlet.http.HttpServletRequest theServletRequest,
            javax.servlet.http.HttpServletResponse theServletResponse,
            ca.uhn.fhir.rest.api.server.RequestDetails theRequestDetails,
            @Description(shortDefinition="The ID of the resource")
            @OptionalParam(name="_id") TokenAndListParam the_id,
            @Description(shortDefinition="The identifier of this plan")
            @OptionalParam(name="identifier") TokenAndListParam theIdentifier,
            @Description(shortDefinition="Instantiates FHIR protocol or definition")
            @OptionalParam(name="instantiates-canonical", targetTypes={  } ) ReferenceAndListParam theInstantiates_canonical,
            @Description(shortDefinition="proposal | plan | order | option")
            @OptionalParam(name="intent") TokenAndListParam theIntent,
            @Description(shortDefinition="draft | active | suspended | completed | entered-in-error | cancelled | unknown")
            @OptionalParam(name="status") TokenAndListParam theStatus,
            @Description(shortDefinition="Only return resources which were last updated as specified by the given range")
            @OptionalParam(name="_lastUpdated") DateRangeParam theLastUpdated,
            @Description(shortDefinition="Which organization does this care plan belong to")
            @OptionalParam(name = "subjectIdentifier") TokenParam subjectIdentifier,
            @Description(shortDefinition="Who the care plan is for")
            @OptionalParam(name = "authorIdentifier") TokenParam authorIdentifier,
            @Sort SortSpec theSort,
            @Count Integer theCount,
            SummaryEnum theSummaryMode,
            SearchTotalModeEnum theSearchTotalMode

    ) {
        try {
            logger.debug("search");

            String hostname = ServiceVariables.PATIENTCARE_SERVICE_URL.getValue();
            IGenericClient client = clientFactory.getClientFromBaseUrl(hostname, fhirContext);
            String searchUrl = constructUrlFromRequestDetails(hostname, theRequestDetails);

            return client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        }
    }

    private void verifyOrCreateIdentifier(CarePlan carePlan) {
        boolean hasRequiredIdentifier = false;
        for (Identifier identifier : carePlan.getIdentifier()) {
            if (identifier.hasSystem() && identifier.getSystem().equals(ServiceVariables.OFFICIAL_CAREPLAN_IDENTIFIER_FIRST_SYSTEM.getValue()))
                hasRequiredIdentifier = true;
        }
        if (!hasRequiredIdentifier)
            carePlan.addIdentifier(createUuidIdentifier());
    }

    private Identifier createUuidIdentifier() {
        Identifier uuid = new Identifier();
        uuid.setSystem(ServiceVariables.OFFICIAL_CAREPLAN_IDENTIFIER_FIRST_SYSTEM.getValue());
        uuid.setValue(UUID.randomUUID().toString());
        return uuid;
    }

    String constructUrlFromRequestDetails(String hostname, RequestDetails requestDetails){
        return hostname + "/CarePlan" + CopiedParameters.from(requestDetails).get();
    }

}
