package dk.s4.microservices.employeebff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.api.Include;
import ca.uhn.fhir.model.api.annotation.Description;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.api.SearchTotalModeEnum;
import ca.uhn.fhir.rest.api.SortSpec;
import ca.uhn.fhir.rest.api.SummaryEnum;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.exceptions.FhirClientConnectionException;
import ca.uhn.fhir.rest.param.*;
import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.employeebff.utils.OrganizationQueryParameter;
import dk.s4.microservices.employeebff.utils.Utils;
import dk.s4.microservices.messaging.ReplyEventProcessor;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.fhir.FhirBaseBFFResourceProvider;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Questionnaire;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FHIRQuestionnaireResourceProvider extends FhirBaseBFFResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(FHIRQuestionnaireResourceProvider.class);
    private final OrganizationQueryParameter parameters;

    public FHIRQuestionnaireResourceProvider(FhirContext fhirContext,
                                             UserContextResolverInterface userContextResolver,
                                             MyFhirClientFactory clientFactory,
                                             KafkaEventProducer kafkaCommandProducer,
                                             ReplyEventProcessor replyEventProcessor,
                                             OrganizationQueryParameter parameters) {
        super(fhirContext, userContextResolver, clientFactory, kafkaCommandProducer, replyEventProcessor);
        this.parameters = parameters;
    }

    protected Identifier identifierForResource(IBaseResource resource) {
        return ((Questionnaire) resource).getIdentifier().stream()
                .filter(identifier -> identifier.getSystem().equals(ServiceVariables.OFFICIAL_QUESTIONNAIRE_IDENTIFIER_SYSTEM.getValue()))
                .findAny()
                .orElse(null);
    }

    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<Questionnaire> getResourceType() {
        return Questionnaire.class;
    }

    /**
     * Updates an existing Questionnaire resource. Only the status field is allowed to change.
     *
     * @param theRequest       Incoming http request
     * @param theResponse      outgoing http response
     * @param theQuestionnaire Questionnaire resource to update
     * @param conditional      {@literal Required conditional url pointing to the identifier of the resource to update. Must have form
     *                         Questionnaire?identifier=<system>|<value>.}
     * @return MethodOutcome
     */
    @Update
    public MethodOutcome update(HttpServletRequest theRequest,
                                HttpServletResponse theResponse,
                                @ResourceParam Questionnaire theQuestionnaire,
                                @ConditionalUrlParam String conditional) {
        logger.debug("update, conditional = " + conditional);


        MethodOutcome methodOutcome = createOrUpdate(theRequest, theResponse, theQuestionnaire, Topic.Operation.Update);
        return methodOutcome;

    }

    /**
     * Creates a questionnaire resource on the object-definition-service and returns a MethodOutcome resource indicating whether the
     * action was successful.
     *
     * @param theRequest       The incoming request
     * @param theResponse      The outgoing response
     * @param theQuestionnaire The questionnaire to create
     * @return A MethodOutcome resource indicating whether the action was successful.
     */
    @Create
    public MethodOutcome create(HttpServletRequest theRequest,
                                HttpServletResponse theResponse,
                                @ResourceParam Questionnaire theQuestionnaire) {

        MethodOutcome methodOutcome = createOrUpdate(theRequest, theResponse, theQuestionnaire, Topic.Operation.Create);
        return methodOutcome;

    }


    @Search(allowUnknownParams = true)
    public Bundle search(
            javax.servlet.http.HttpServletRequest theServletRequest,
            javax.servlet.http.HttpServletResponse theServletResponse,
            ca.uhn.fhir.rest.api.server.RequestDetails theRequestDetails,
            @Description(shortDefinition = "The ID of the resource")
            @OptionalParam(name = "_id")
                    TokenAndListParam the_id,
            @Description(shortDefinition = "The questionnaire publication date")
            @OptionalParam(name = "date")
                    DateRangeParam theDate,
            @Description(shortDefinition = "External identifier for the questionnaire")
            @OptionalParam(name = "identifier")
                    TokenAndListParam theIdentifier,
            @Description(shortDefinition = "Intended jurisdiction for the questionnaire")
            @OptionalParam(name = "jurisdiction")
                    TokenAndListParam theJurisdiction,
            @Description(shortDefinition = "Computationally friendly name of the questionnaire")
            @OptionalParam(name = "name")
                    StringAndListParam theName,
            @Description(shortDefinition = "The current status of the questionnaire")
            @OptionalParam(name = "status")
                    TokenAndListParam theStatus,
            @Description(shortDefinition = "Resource that can be subject of QuestionnaireResponse")
            @OptionalParam(name = "subject-type")
                    TokenAndListParam theSubject_type,
            @Description(shortDefinition = "The human-friendly name of the questionnaire")
            @OptionalParam(name = "title")
                    StringAndListParam theTitle,
            @Description(shortDefinition = "The uri that identifies the questionnaire")
            @OptionalParam(name = "url")
                    UriAndListParam theUrl,
            @Description(shortDefinition = "The business version of the questionnaire")
            @OptionalParam(name = "version")
                    TokenAndListParam theVersion,
            @RawParam
                    Map<String, List<String>> theAdditionalRawParams,
            @IncludeParam(reverse = true)
                    Set<Include> theRevIncludes,
            @Description(shortDefinition = "Only return resources which were last updated as specified by the given range")
            @OptionalParam(name = "_lastUpdated")
                    DateRangeParam theLastUpdated,
            @IncludeParam(allow = {
                    "*"
            })
                    Set<Include> theIncludes,
            @Sort
                    SortSpec theSort,
            @ca.uhn.fhir.rest.annotation.Count
                    Integer theCount,
            SummaryEnum theSummaryMode,
            SearchTotalModeEnum theSearchTotalMode
    ) {
        logger.debug("search");
        try {
            String hostname = ServiceVariables.OUTCOMEDEFINITION_SERVICE_URL.getValue();
            IGenericClient client = clientFactory.getClientFromBaseUrl(hostname, fhirContext);
            String searchUrl = Utils.constructUrlFromRequestDetails(hostname, "Questionnaire", theRequestDetails);

            return client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        }
    }

    /**
     * Retrieves a bundle containing all Questionnaires with a certain status
     *
     * @param theRequest  Incoming http request
     * @param theResponse Outgoing http response
     * @param status      the status with which the returned Questionnaires should have
     * @return Bundle of Questionnaires
     */

    @Search(queryName = "getWithStatus")
    public Bundle getWithStatus(HttpServletRequest theRequest,
                                HttpServletResponse theResponse,
                                @RequiredParam(name = Questionnaire.SP_STATUS) TokenParam status) {
        try {
            logger.debug("getQuestionnaireWithStatus");

            IGenericClient client = clientFactory
                    .getClientFromBaseUrl(ServiceVariables.OUTCOMEDEFINITION_SERVICE_URL.getValue(), fhirContext);
            String searchUrl =
                    ServiceVariables.OUTCOMEDEFINITION_SERVICE_URL.getValue() + "/Questionnaire?_query=getWithStatus&status="
                            + status.getValue();
            searchUrl = Utils.addOrganizationParameters(searchUrl, "jurisdiction", theRequest, parameters, clientFactory, userContextResolver);
            Bundle bundle = client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();

            return bundle;
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        } catch (UserContextResolverInterface.UserContextResolverException e) {
            logger.error(e.getMessage(), e);
            throw new AuthenticationException(e.getMessage(), e);
        }
    }

    /**
     * Search for Questionnaire by Identifier.
     * <p>
     * Example invocation: http://fhir.example.com/Questionnaire?identifier=urn:ietf:rfc:3986|cb0a63f8-b595-4882-a1d2-71eba429d56e
     *
     * @param identifier Identifier of a Questionnaire to search for.
     * @return Questionnaire with matching Identifier or null if not found
     */
    @Search(queryName = "getByIdentifier")
    public Questionnaire getByIdentifier(HttpServletRequest theRequest,
                                         HttpServletResponse theResponse,
                                         @RequiredParam(name = Questionnaire.SP_IDENTIFIER) TokenParam identifier) {
        try {
            logger.debug("getByIdentifier");

            IGenericClient client = clientFactory.getClientFromBaseUrl(ServiceVariables.OUTCOMEDEFINITION_SERVICE_URL.getValue(), fhirContext);
            String searchUrl = ServiceVariables.OUTCOMEDEFINITION_SERVICE_URL.getValue() + "/Questionnaire?_query=getByIdentifier&identifier=" + identifier.getSystem() + "|" + identifier.getValue();
            searchUrl = Utils.addOrganizationParameters(searchUrl, "jurisdiction", theRequest, parameters, clientFactory, userContextResolver);
            Bundle bundle = client.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();

            return (Questionnaire) bundle.getEntryFirstRep().getResource();
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        } catch (UserContextResolverInterface.UserContextResolverException e) {
            logger.error(e.getMessage(), e);
            throw new AuthenticationException(e.getMessage(), e);
        }
    }
}
