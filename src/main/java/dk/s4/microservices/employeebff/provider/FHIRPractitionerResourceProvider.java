package dk.s4.microservices.employeebff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.client.exceptions.FhirClientConnectionException;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Practitioner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FHIRPractitionerResourceProvider implements IResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(FHIRPractitionerResourceProvider.class);

    protected FhirContext fhirContext;
    protected UserContextResolverInterface userContextResolver;
    protected MyFhirClientFactory clientFactory;

    /**
     * Constructor
     *
     * @param fhirContext The (typically singleton) {@link ca.uhn.fhir.context.FhirContext} instance of the server
     * @param userContextResolver For resolving user fhirContext information.
     * @param clientFactory Factory for getting HAPI FHIR client instances
     */
    public FHIRPractitionerResourceProvider(FhirContext fhirContext,
                                            UserContextResolverInterface userContextResolver,
                                            MyFhirClientFactory clientFactory) {
        this.fhirContext = fhirContext;
        this.userContextResolver = userContextResolver;
        this.clientFactory = clientFactory;
    }


    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<Practitioner> getResourceType() {
        return Practitioner.class;
    }

    /**
     * Returns a FHIR Practitioner resource containing information about the currently logged in user.
     *
     * If no user is currently logged in, an error will be thrown
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response

     * @return A FHIR Practitioner resource
     */
    @Search(queryName = "getMe")
    public Bundle getMe(HttpServletRequest theRequest,
                        HttpServletResponse theResponse) {
        try {
            logger.debug("getMe");

            Bundle retVal = new Bundle();

            // Get username identifier from UserContextResolver
            Identifier identifier = userContextResolver
                    .getFHIRUserId(theRequest);

            // Get name of user from UserContextResolver
            HumanName name = userContextResolver
                    .getFHIRNameOfUser(theRequest);

            // Create Practitioner containing user context data
            Practitioner me = new Practitioner();
            if (identifier != null) {
                identifier.setSystem(ServiceVariables.PRACTITIONER_USERNAME_IDENTIFIER_SYSTEM.getValue());
                me.addIdentifier(identifier);
            }
            if (name != null) {
                me.addName(name);
            }

            retVal.addEntry(
                    new Bundle.BundleEntryComponent()
                            .setResource(me)
            );

            return retVal;
        } catch (FhirClientConnectionException e) {
            throw ExceptionUtils.fatalInternalErrorException(e.getMessage(), e);
        } catch (UserContextResolverInterface.UserContextResolverException e) {
            logger.error(e.getMessage(), e);
            throw new AuthenticationException(e.getMessage(), e);
        }
    }
}
