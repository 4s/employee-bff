package dk.s4.microservices.employeebff.provider.stub;

import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import dk.s4.microservices.employeebff.utils.ThresholdSet;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class FHIRThresholdSetProviderStub implements IResourceProvider {

    private static final Logger logger = LoggerFactory.getLogger(FHIRThresholdSetProviderStub.class);

    private List<ThresholdSet> thresholdSets = new ArrayList<>();

    public FHIRThresholdSetProviderStub() {
        createStubData();
    }

    private void createStubData() {
        ThresholdSet pd1 = (ThresholdSet) new ThresholdSet().setApprovalDate(new Date()).setId("test1");
        ThresholdSet pd2 = (ThresholdSet) new ThresholdSet().setApprovalDate(new Date()).setId("test2");

        thresholdSets.add(pd1);
        thresholdSets.add(pd2);
    }

    /**
     * Returns the type of resource this provider provides.
     */
    @Override
    public Class<ThresholdSet> getResourceType() {
        return ThresholdSet.class;
    }

    /**
     * Creates a ThresholdSet resource - a template for the telemedical care for a group of patients sharing a certain
     * set of symptoms or a particular condition.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http response
     * @param thePlanDef The ThresholdSet to create
     * @return MethodOutcome
     */
    @Create
    public MethodOutcome create(HttpServletRequest theRequest,
                                              HttpServletResponse theResponse,
                                              @ResourceParam ThresholdSet thePlanDef) {
        logger.debug("create");

        String uuid = UUID.randomUUID().toString();
        String identifierStr = "urn:ietf:rfc:3986" + "|" + "urn:uuid:" + uuid;
        thePlanDef.addIdentifier().setSystem("urn:ietf:rfc:3986").setValue("urn:uuid:" + uuid);

        MethodOutcome retVal = new MethodOutcome();
        retVal.setCreated(true);
        retVal.setResource(thePlanDef);

        retVal.setId(new IdType(identifierStr));

        OperationOutcome operationOutcome = new OperationOutcome();
        operationOutcome.addIssue().
                setDiagnostics("Successfully updated ressource with identifier = " + identifierStr).
                setSeverity(OperationOutcome.IssueSeverity.INFORMATION);
        retVal.setOperationOutcome(operationOutcome);

        theResponse.addHeader("Location", identifierStr);
        theResponse.setStatus(201); //Created

        return retVal;

    }

    /**
     *
     * Updates an existing ThresholdSet resource.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http request
     * @param theThresholdSet ThresholdSet to update. Must have an identifier.
     * @param conditional Required conditional url pointing to the identifier of the resource to update. Must have form
     *                    {@literal ThresholdSet?identifier=<system>|<value>}.
     * @return MethodOutcome
     */
    @Update
    public MethodOutcome update(HttpServletRequest theRequest,
                                             HttpServletResponse theResponse,
                                             @ResourceParam ThresholdSet theThresholdSet,
                                             @ConditionalUrlParam String conditional) {
        logger.debug("update, conditional = " + conditional);

        MethodOutcome retVal = new MethodOutcome();
        retVal.setResource(theThresholdSet);

        Identifier questIdent = theThresholdSet.getIdentifier().stream()
                .filter(identifier -> !identifier.getSystem().isEmpty())
                .findAny()
                .orElse(null);
        if (questIdent == null) {
            throw new UnprocessableEntityException("ThresholdSet must have an identifier");
        }

        String location = questIdent.getSystem() + "|" + questIdent.getValue();
        retVal.setId(new IdType(location));

        OperationOutcome operationOutcome = new OperationOutcome();
        operationOutcome.addIssue().
                setDiagnostics("Successfully updated ressource with identifier = " + location).
                setSeverity(OperationOutcome.IssueSeverity.INFORMATION);
        retVal.setOperationOutcome(operationOutcome);

        theResponse.addHeader("Location", location);
        theResponse.setStatus(200);

        return retVal;
    }

    /**
     * Get ThresholdSet resource by Identifier.
     *
     * @param theRequest Incoming http request
     * @param theResponse Outgoing http request
     * @param identifier Identifier of a ThresholdSet to search for
     *
     * @return ThresholdSet with matching Identifier or null if not found
     *
     * @see <a href="https://issuetracker4s.atlassian.net/wiki/spaces/FDM/pages/47251467/FHIR+ThresholdSet">FHIR ThresholdSet</a>
     */
    @Search(queryName = "getByIdentifier")
    public ThresholdSet getByIdentifier(HttpServletRequest theRequest,
                                       HttpServletResponse theResponse,
                                       @RequiredParam(name= ThresholdSet.SP_IDENTIFIER) TokenParam identifier) {

        return thresholdSets.get(0);
    }

}
