package dk.s4.microservices.employeebff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.IUntypedQuery;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.TokenParam;
import dk.s4.microservices.employeebff.utils.OrganizationQueryParameter;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.QuestionnaireResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FHIRQuestionnaireResponseResourceProviderTest {
    private static final String ORGIDENTIFIER = "orgIdentifier1234";
    private FHIRQuestionnaireResponseResourceProvider provider;
    private String url;
    private HttpServletRequest request;

    @Before
    public void setup() throws IOException, UserContextResolverInterface.UserContextResolverException {
        EnvironmentVariables variables = new EnvironmentVariables();
        TestUtils.readEnvironment("deploy.env", variables);
        TestUtils.readEnvironment("service.env", variables);

        FhirContext context = mock(FhirContext.class);
        UserContextResolverInterface userContextResolverInterface = mock(UserContextResolverInterface.class);
        MyFhirClientFactory clientFactory = mock(MyFhirClientFactory.class);
        IGenericClient client = mock(IGenericClient.class);
        IUntypedQuery query = mock(IUntypedQuery.class);
        IQuery iQuery = mock(IQuery.class);
        request = mock(HttpServletRequest.class);
        org.hl7.fhir.r4.model.Bundle returnBundle = mock(Bundle.class);
        List bundleEntries = new ArrayList<>();
        OrganizationQueryParameter parameters = mock(OrganizationQueryParameter.class);

        when(parameters.createFamilyFrom(anyObject(),anyObject())).thenReturn("authorArgument");



        when(clientFactory.getClientFromBaseUrl(anyObject(), anyObject())).then((invocation) -> client);
        when(client.search()).then((invocation) -> query);
        when(query.byUrl(anyObject())).then((invocation) -> {
            url = (String) invocation.getArguments()[0];
            return iQuery;
        });
        when (iQuery.returnBundle(anyObject())).then((invocation) -> iQuery);
        when (iQuery.execute()).then((invocation) -> returnBundle);
        when(returnBundle.getEntry()).then((invocationOnMock -> bundleEntries));
        provider = new FHIRQuestionnaireResponseResourceProvider(context,
                userContextResolverInterface,
                clientFactory, null, null, parameters);
        when(userContextResolverInterface.getFHIRUserOrganization(request)).thenReturn(new Identifier().setValue(ORGIDENTIFIER));

    }

    @Test
    public void testGetFocusForTasksWithCodeAndStatus() {
        TokenParam code = new TokenParam().setValue("codeValue");
        TokenParam status = new TokenParam().setValue("statusValue");
        provider.getFocusForTasksWithCodeAndStatus(request, code, status);

        assertThat(url).isNotNull();
        assertThat(url).contains("/QuestionnaireResponse?_query=getFocusForTasksWithCodeAndStatus");
        assertThat(url).contains("&code=codeValue");
        assertThat(url).contains("&status=statusValue");
        assertThat(url).contains("&owner=");
        assertThat(url).contains("authorArgument");
    }

    @Test
    public void searchByBasedOnIdentifierYieldsExpectedSearchUrlWithoutAuthored() {
        TokenParam identifier = new TokenParam().setValue("value").setSystem("system");
        provider.searchByBasedOnIdentifier(request, mock(HttpServletResponse.class), identifier, null);
        assertThat(url).isNotNull();
        assertThat(url).contains("_query=searchByBasedOnIdentifier&based-on=system|value");
    }

    @Test
    public void searchBySubjectIdentifierYieldsExpectedSearchUrlWithoutAuthored() {
        TokenParam identifier = new TokenParam().setValue("value").setSystem("system");
        provider.searchBySubjectIdentifier(request, mock(HttpServletResponse.class), identifier, null);
        assertThat(url).isNotNull();
        assertThat(url).contains("_query=searchBySubjectIdentifier&subject-identifier=system|value");
    }

    @Test
    public void searchByBasedOnIdentifierYieldsExpectedSearchUrlWithAuthored() {
        TokenParam identifier = new TokenParam().setValue("value").setSystem("system");
        DateRangeParam dateRangeParam = new DateRangeParam();
        dateRangeParam.setRangeFromDatesInclusive("1000-11-11", "1111-11-11");
        provider.searchByBasedOnIdentifier(request, mock(HttpServletResponse.class), identifier, dateRangeParam);
        assertThat(url).isNotNull();
        assertThat(url).contains("_query=searchByBasedOnIdentifier&based-on=system|value&authored=ge1000-11-11&authored=le1111-11-11");
    }

    @Test
    public void searchBySubjectIdentifierYieldsExpectedSearchUrlWithAuthored() {
        TokenParam identifier = new TokenParam().setValue("value").setSystem("system");
        DateRangeParam dateRangeParam = new DateRangeParam();
        dateRangeParam.setRangeFromDatesInclusive("1000-11-11", "1111-11-11");
        provider.searchBySubjectIdentifier(request, mock(HttpServletResponse.class), identifier, dateRangeParam);
        assertThat(url).isNotNull();
        assertThat(url).contains("_query=searchBySubjectIdentifier&subject-identifier=system|value&authored=ge1000-11-11&authored=le1111-11-11");
    }

    @Test
    public void testIsAmended(){
        QuestionnaireResponse questionnaireResponse = new QuestionnaireResponse().setStatus(QuestionnaireResponse.QuestionnaireResponseStatus.AMENDED);

        assertThat(provider.isAmended(questionnaireResponse)).isEqualTo(true);
    }

    @Test
    public void testIsAmendedInvalidStatus(){
        QuestionnaireResponse questionnaireResponse = new QuestionnaireResponse().setStatus(QuestionnaireResponse.QuestionnaireResponseStatus.INPROGRESS);

        assertThat(provider.isAmended(questionnaireResponse)).isEqualTo(false);
    }
}
