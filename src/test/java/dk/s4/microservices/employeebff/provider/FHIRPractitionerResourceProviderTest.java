package dk.s4.microservices.employeebff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.IUntypedQuery;
import ca.uhn.fhir.rest.param.TokenParam;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Practitioner;
import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FHIRPractitionerResourceProviderTest {
    private FHIRPractitionerResourceProvider provider;
    private static final String USER_ID = "user1234";
    private static final String[] USER_GIVENNAME = new String[]{"Nancy", "Ann", "Test"};
    private static final String USER_FAMILYNAME = "Berggreen";
    private FhirContext context;
    private UserContextResolverInterface userContextResolver;
    private MyFhirClientFactory clientFactory;
    private HttpServletRequest request;
    private HttpServletResponse response;

    @Before
    public void setup() throws Exception {

        EnvironmentVariables variables = new EnvironmentVariables();
        TestUtils.readEnvironment("deploy.env", variables);
        TestUtils.readEnvironment("service.env", variables);

        context = mock(FhirContext.class);
        userContextResolver = mock(UserContextResolverInterface.class);
        clientFactory = mock(MyFhirClientFactory.class);
    }

    @Test
    public void testGetMeHappyPath() throws UserContextResolverInterface.UserContextResolverException {
        when(userContextResolver.getFHIRUserId(request)).thenReturn(new Identifier().setValue(USER_ID));
        when(userContextResolver.getFHIRNameOfUser(request)).thenReturn(
                new HumanName()
                        .addGiven(USER_GIVENNAME[0])
                        .addGiven(USER_GIVENNAME[1])
                        .addGiven(USER_GIVENNAME[2])
                        .setFamily(USER_FAMILYNAME)
        );

        provider = new FHIRPractitionerResourceProvider(context,
                userContextResolver,
                clientFactory);

        Bundle bundle = provider.getMe(request, response);
        Practitioner me = (Practitioner) bundle.getEntryFirstRep().getResource();
        assertThat(me.getIdentifierFirstRep().getSystem()).isEqualTo(ServiceVariables.PRACTITIONER_USERNAME_IDENTIFIER_SYSTEM.getValue());
        assertThat(me.getIdentifierFirstRep().getValue()).isEqualTo(USER_ID);
        assertThat(me.getNameFirstRep().getGivenAsSingleString()).isEqualTo(USER_GIVENNAME[0] + " " + USER_GIVENNAME[1] + " " + USER_GIVENNAME[2]);
        assertThat(me.getNameFirstRep().getFamily()).isEqualTo(USER_FAMILYNAME);
    }

    @Test
    public void testGetMeNoUserId() throws UserContextResolverInterface.UserContextResolverException {
        when(userContextResolver.getFHIRUserId(request)).thenReturn(null);
        when(userContextResolver.getFHIRNameOfUser(request)).thenReturn(
                new HumanName()
                        .addGiven(USER_GIVENNAME[0])
                        .addGiven(USER_GIVENNAME[1])
                        .addGiven(USER_GIVENNAME[2])
                        .setFamily(USER_FAMILYNAME)
        );

        provider = new FHIRPractitionerResourceProvider(context,
                userContextResolver,
                clientFactory);

        Bundle bundle = provider.getMe(request, response);
        Practitioner me = (Practitioner) bundle.getEntryFirstRep().getResource();
        assertThat(me.getIdentifier()).isEmpty();
        assertThat(me.getNameFirstRep().getGivenAsSingleString()).isEqualTo(USER_GIVENNAME[0] + " " + USER_GIVENNAME[1] + " " + USER_GIVENNAME[2]);
        assertThat(me.getNameFirstRep().getFamily()).isEqualTo(USER_FAMILYNAME);
    }

    @Test
    public void testGetMeNoName() throws UserContextResolverInterface.UserContextResolverException {
        when(userContextResolver.getFHIRUserId(request)).thenReturn(new Identifier().setValue(USER_ID));
        when(userContextResolver.getFHIRNameOfUser(request)).thenReturn(null);

        provider = new FHIRPractitionerResourceProvider(context,
                userContextResolver,
                clientFactory);

        Bundle bundle = provider.getMe(request, response);
        Practitioner me = (Practitioner) bundle.getEntryFirstRep().getResource();
        assertThat(me.getIdentifierFirstRep().getSystem()).isEqualTo(ServiceVariables.PRACTITIONER_USERNAME_IDENTIFIER_SYSTEM.getValue());
        assertThat(me.getIdentifierFirstRep().getValue()).isEqualTo(USER_ID);
        assertThat(me.getName()).isEmpty();
    }

    @Test
    public void testGetMeNoGivenNames() throws UserContextResolverInterface.UserContextResolverException {
        when(userContextResolver.getFHIRUserId(request)).thenReturn(new Identifier().setValue(USER_ID));
        when(userContextResolver.getFHIRNameOfUser(request)).thenReturn(
                new HumanName().setFamily(USER_FAMILYNAME)
        );

        provider = new FHIRPractitionerResourceProvider(context,
                userContextResolver,
                clientFactory);

        Bundle bundle = provider.getMe(request, response);
        Practitioner me = (Practitioner) bundle.getEntryFirstRep().getResource();
        assertThat(me.getIdentifierFirstRep().getSystem()).isEqualTo(ServiceVariables.PRACTITIONER_USERNAME_IDENTIFIER_SYSTEM.getValue());
        assertThat(me.getIdentifierFirstRep().getValue()).isEqualTo(USER_ID);
        assertThat(me.getNameFirstRep().getGiven()).isEmpty();
        assertThat(me.getNameFirstRep().getFamily()).isEqualTo(USER_FAMILYNAME);
    }

    @Test
    public void testGetMeNoFamilyName() throws UserContextResolverInterface.UserContextResolverException {
        when(userContextResolver.getFHIRUserId(request)).thenReturn(new Identifier().setValue(USER_ID));
        when(userContextResolver.getFHIRNameOfUser(request)).thenReturn(
                new HumanName()
                        .addGiven(USER_GIVENNAME[0])
                        .addGiven(USER_GIVENNAME[1])
                        .addGiven(USER_GIVENNAME[2])
        );

        provider = new FHIRPractitionerResourceProvider(context,
                userContextResolver,
                clientFactory);

        Bundle bundle = provider.getMe(request, response);
        Practitioner me = (Practitioner) bundle.getEntryFirstRep().getResource();
        assertThat(me.getIdentifierFirstRep().getSystem()).isEqualTo(ServiceVariables.PRACTITIONER_USERNAME_IDENTIFIER_SYSTEM.getValue());
        assertThat(me.getIdentifierFirstRep().getValue()).isEqualTo(USER_ID);
        assertThat(me.getNameFirstRep().getGivenAsSingleString()).isEqualTo(USER_GIVENNAME[0] + " " + USER_GIVENNAME[1] + " " + USER_GIVENNAME[2]);
        assertThat(me.getNameFirstRep().getFamily()).isNull();
    }
}
