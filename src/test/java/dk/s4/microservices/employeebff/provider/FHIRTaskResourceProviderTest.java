package dk.s4.microservices.employeebff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.IUntypedQuery;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.employeebff.utils.OrganizationQueryParameter;
import dk.s4.microservices.messaging.ReplyEventProcessor;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.instance.model.api.IBaseBundle;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FHIRTaskResourceProviderTest {
    private FhirContext context;
    private UserContextResolverInterface userContextResolverInterface;
    private MyFhirClientFactory clientFactory;
    private KafkaEventProducer kafkaEventProducer;
    private ReplyEventProcessor replyEventProcessor;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private List<String> searchUrls;
    private List<String> clientUrls;
    private RequestDetails details;
    private IQuery query;
    private FHIRTaskResourceProvider provider;
    private static final String ORGIDENTIFIER = "orgIdentifier1234";

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @BeforeClass
    public static void beforeClass() throws Exception {
        TestUtils.readEnvironment("service.env", environmentVariables);
    }

    @Before
    public void setUp() throws IOException, UserContextResolverInterface.UserContextResolverException {
        EnvironmentVariables variables = new EnvironmentVariables();
        TestUtils.readEnvironment("deploy.env", variables);
        TestUtils.readEnvironment("service.env", variables);

        context = mock(FhirContext.class);
        userContextResolverInterface = mock(UserContextResolverInterface.class);
        clientFactory = mock(MyFhirClientFactory.class);
        kafkaEventProducer = mock(KafkaEventProducer.class);
        replyEventProcessor = mock(ReplyEventProcessor.class);
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        details = mock(RequestDetails.class);
        clientUrls = new ArrayList<>();
        searchUrls = new ArrayList<>();
        OrganizationQueryParameter parameters = mock(OrganizationQueryParameter.class);

        when(parameters.createFamilyFrom(anyObject(),anyObject())).thenReturn("authorArgument");
        IGenericClient client = mock(IGenericClient.class);

        @SuppressWarnings("unchecked")
        IUntypedQuery<IBaseBundle> search = (IUntypedQuery<IBaseBundle>) mock(IUntypedQuery.class);
        query = mock(IQuery.class);

        when(query.returnBundle(Bundle.class)).thenReturn(query);

        when(search.byUrl(anyString())).thenAnswer((invocation) -> {
            String url = (String) invocation.getArguments()[0];
            searchUrls.add(url);
            return query;
        });

        when(client.search()).thenReturn(search);

        clientFactory = mock(MyFhirClientFactory.class);
        when(clientFactory.getClientFromBaseUrl(anyString(), any())).thenAnswer((invocation) -> {
            clientUrls.add((String) invocation.getArguments()[0]);
            return client;
        });
        when(details.getCompleteUrl()).thenAnswer((invocation) -> "example.com/Task?this=is&an=example");

        when(userContextResolverInterface.getFHIRUserOrganization(request)).thenReturn(new Identifier().setValue(ORGIDENTIFIER));

        provider = new FHIRTaskResourceProvider(context,userContextResolverInterface,
                clientFactory,kafkaEventProducer,replyEventProcessor, parameters);
    }


    @Test
    public void testSearch() throws UserContextResolverInterface.UserContextResolverException {
        provider.search(request,response,details,null,null,null,null,null,
                null);
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(searchUrls.get(0)).contains("Task?this=is&an=example");
        assertThat(searchUrls.get(0)).contains(ServiceVariables.OUTCOME_SERVICE_URL.getValue());
    }
}