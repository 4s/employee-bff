package dk.s4.microservices.employeebff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.IUntypedQuery;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.TokenParam;
import dk.s4.microservices.employeebff.utils.OrganizationQueryParameter;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Observation;
import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FHIRObservationResourceProviderTest {
    private FHIRObservationResourceProvider provider;
    private UserContextResolverInterface userContextResolverInterface;
    private String url;
    private static final String ORGIDENTIFIER = "orgIdentifier1234";
    private HttpServletRequest request;


    @Before
    public void setup() throws IOException, UserContextResolverInterface.UserContextResolverException {

        EnvironmentVariables variables = new EnvironmentVariables();
        TestUtils.readEnvironment("deploy.env", variables);
        TestUtils.readEnvironment("service.env", variables);

        FhirContext context = mock(FhirContext.class);
        userContextResolverInterface = mock(UserContextResolverInterface.class);
        MyFhirClientFactory clientFactory = mock(MyFhirClientFactory.class);
        IGenericClient client = mock(IGenericClient.class);
        IUntypedQuery query = mock(IUntypedQuery.class);
        request = mock(HttpServletRequest.class);
        OrganizationQueryParameter parameters = mock(OrganizationQueryParameter.class);

        when(parameters.createFamilyFrom(anyObject(),anyObject())).thenReturn("authorArgument");

        IQuery iQuery = mock(IQuery.class);
        when(clientFactory.getClientFromBaseUrl(anyObject(), anyObject())).then((invocation) -> client);
        when(client.search()).then((invocation) -> query);
        when(query.byUrl(anyObject())).then((invocation) -> {
            url = (String) invocation.getArguments()[0];
            return iQuery;
        });
        when (iQuery.returnBundle(anyObject())).then((invocation) -> iQuery);
        when (iQuery.execute()).then((invocation) -> null);
        provider = new FHIRObservationResourceProvider(context,
                userContextResolverInterface,
                clientFactory,
                null, null,
                parameters);
        when(userContextResolverInterface.getFHIRUserOrganization(request)).thenReturn(new Identifier().setValue(ORGIDENTIFIER));
    }

    @Test
    public void searchByBasedOnIdentifierYieldsExpectedSearchUrlWithoutAuthored() {
        TokenParam identifier = new TokenParam().setValue("value").setSystem("system");
        provider.searchByBasedOnIdentifier(request, mock(HttpServletResponse.class), identifier);
        assertThat(url).isNotNull();
        assertThat(url).contains("_query=searchByBasedOnIdentifier&based-on=system|value");
    }

    @Test
    public void getForPatientYieldsExpectedSearchUrl() {
        TokenParam identifier = new TokenParam().setValue("value").setSystem("system");
        DateRangeParam dateRangeParam = new DateRangeParam();
        dateRangeParam.setRangeFromDatesInclusive("1000-11-11", "1111-11-11");
        provider.getForPatient(request, mock(HttpServletResponse.class), identifier, dateRangeParam);
        assertThat(url).isNotNull();
        assertThat(url).contains("_query=searchBySubjectIdentifier&subject-identifier=system|value&date=ge1000-11-11&date=le1111-11-11");
    }

    @Test
    public void getForPatientWithCodeYieldsExpectedSearchUrl() {
        TokenParam identifier = new TokenParam().setValue("value").setSystem("system");
        TokenParam code = new TokenParam().setSystem("codeSystem").setValue("codeValue");
        DateRangeParam dateRangeParam = new DateRangeParam();
        dateRangeParam.setRangeFromDatesInclusive("1000-11-11", "1111-11-11");
        provider.getForPatientWithCode(request, mock(HttpServletResponse.class), identifier, code, dateRangeParam);
        assertThat(url).isNotNull();
        assertThat(url).contains("searchBySubjectIdentifier&subject-identifier=system|value&code=codeSystem|codeValue&date=ge1000-11-11&date=le1111-11-11");
    }
    @Test
    public void getForPatientWithCodeYieldsExpectedSearchUrl_onlyUpperBound() {
        TokenParam identifier = new TokenParam().setValue("value").setSystem("system");
        TokenParam code = new TokenParam().setSystem("codeSystem").setValue("codeValue");
        DateRangeParam dateRangeParam = new DateRangeParam();
        dateRangeParam.setUpperBound( "1111-11-11");
        provider.getForPatientWithCode(request, mock(HttpServletResponse.class), identifier, code, dateRangeParam);
        assertThat(url).isNotNull();
        assertThat(url).contains("searchBySubjectIdentifier&subject-identifier=system|value&code=codeSystem|codeValue&date=le1111-11-11");
    }

    @Test
    public void getForPatientWithCodeYieldsExpectedSearchUrl_OnlyLowerBound() {
        TokenParam identifier = new TokenParam().setValue("value").setSystem("system");
        TokenParam code = new TokenParam().setSystem("codeSystem").setValue("codeValue");
        DateRangeParam dateRangeParam = new DateRangeParam();
        dateRangeParam.setLowerBound("1000-11-11");
        provider.getForPatientWithCode(request, mock(HttpServletResponse.class), identifier, code, dateRangeParam);
        assertThat(url).isNotNull();
        assertThat(url).contains("searchBySubjectIdentifier&subject-identifier=system|value&code=codeSystem|codeValue&date=ge1000-11-11");
    }

    @Test
    public void getByIdentifierYieldsExpectedSearchUrl() {
        TokenParam identifier = new TokenParam().setValue("value").setSystem("system");
        provider.getByIdentifier(request, mock(HttpServletResponse.class), identifier);
        assertThat(url).isNotNull();
        assertThat(url).contains("_query=getByIdentifier&identifier=system|value");
    }


    @Test
    public void getForAcknowledgementYieldsExpectedSearchUrl() {
        TokenParam status = new TokenParam().setValue("value");
        provider.getForAcknowledgement(request, mock(HttpServletResponse.class), status);
        assertThat(url).isNotNull();
        assertThat(url).contains("Observation?_query=getObservationsForAcknowledgement&status=value&owner=authorArgument");
    }

    @Test
    public void testIsAmended(){
        Observation observation = new Observation().setStatus(Observation.ObservationStatus.AMENDED);

        assertThat(provider.isAmended(observation)).isEqualTo(true);
    }

    @Test
    public void testIsAmendedInvalidStatus(){
        Observation observation = new Observation().setStatus(Observation.ObservationStatus.CORRECTED);

        assertThat(provider.isAmended(observation)).isEqualTo(false);
    }
}
