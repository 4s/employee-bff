package dk.s4.microservices.employeebff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.IUntypedQuery;
import ca.uhn.fhir.rest.param.TokenParam;
import dk.s4.microservices.employeebff.utils.OrganizationQueryParameter;
import dk.s4.microservices.messaging.ReplyEventProcessor;
import dk.s4.microservices.messaging.Topic.Operation;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.FhirBaseBFFResourceProvider;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.instance.model.api.IBaseBundle;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.Identifier;
import org.junit.*;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FHIRCarePlanResourceProviderTest {
    private FhirContext context;
    private UserContextResolverInterface userContextResolver;
    private MyFhirClientFactory clientFactory;
    private KafkaEventProducer kafkaEventProducer;
    private ReplyEventProcessor replyEventProcessor;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private MethodOutcome methodOutcome;
    private List<String> searchUrls;
    private List<String> clientUrls;
    private RequestDetails details;
    private IQuery query;
    private Identifier organizationIdentifier;
    private OrganizationQueryParameter parameters;

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @BeforeClass
    public static void beforeClass() throws Exception {
        TestUtils.readEnvironment("service.env", environmentVariables);
    }

    @Before
    public void setUp() throws Exception {

        EnvironmentVariables variables = new EnvironmentVariables();
        TestUtils.readEnvironment("deploy.env", variables);
        TestUtils.readEnvironment("service.env", variables);

        context = mock(FhirContext.class);
        userContextResolver = mock(UserContextResolverInterface.class);
        clientFactory = mock(MyFhirClientFactory.class);
        kafkaEventProducer = mock(KafkaEventProducer.class);
        replyEventProcessor = mock(ReplyEventProcessor.class);
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        details = mock(RequestDetails.class);
        methodOutcome = mock(MethodOutcome.class);
        clientUrls = new ArrayList<>();
        searchUrls = new ArrayList<>();
        parameters = mock(OrganizationQueryParameter.class);

        when(parameters.createFamilyFrom(anyObject(),anyObject())).thenReturn("authorArgument");

        IGenericClient client = mock(IGenericClient.class);

        @SuppressWarnings("unchecked")
        IUntypedQuery<IBaseBundle> search = (IUntypedQuery<IBaseBundle>) mock(IUntypedQuery.class);
        query = mock(IQuery.class);

        when(query.returnBundle(Bundle.class)).thenReturn(query);

        when(search.byUrl(anyString())).thenAnswer((invocation) -> {
            String url = (String) invocation.getArguments()[0];
            searchUrls.add(url);
            return query;
        });

        when(client.search()).thenReturn(search);

        clientFactory = mock(MyFhirClientFactory.class);
        when(clientFactory.getClientFromBaseUrl(anyString(), any())).thenAnswer((invocation) -> {
            clientUrls.add((String) invocation.getArguments()[0]);
            return client;
        });
        when(details.getCompleteUrl()).thenAnswer((invocation) -> "example.com" + exampleUrl());

        organizationIdentifier = new Identifier().setValue("organizationValue");

        when(userContextResolver.getFHIRUserOrganization(request)).thenReturn(organizationIdentifier);
    }

    @Test
    public void create() throws Exception {
        CarePlan carePlan = new CarePlan();
        FHIRCarePlanResourceProvider carePlanProvider = Mockito.spy(new FHIRCarePlanResourceProvider(context, userContextResolver, clientFactory, kafkaEventProducer, replyEventProcessor, parameters));
        Mockito.doReturn(methodOutcome).when((FhirBaseBFFResourceProvider)carePlanProvider).createOrUpdate(request, response, carePlan, Operation.Create);
        carePlanProvider.create(request, response, carePlan);

        Assert.assertTrue(carePlan.getIdentifier().get(0).getSystem().equals("urn:ietf:rfc:3986"));
    }

    @Test
    public void update() {
        //Nothing to test here - FHIRCarePlanResourceProvider.update just calls createOrUpdate which should be tested
        //test of FhirBaseBFFResourceProvider
    }

    @Test
    public void getActiveForPatient() {
        FHIRCarePlanResourceProvider carePlanProvider = Mockito.spy(new FHIRCarePlanResourceProvider(context, userContextResolver, clientFactory, kafkaEventProducer, replyEventProcessor, parameters));
        carePlanProvider.getActiveForPatient(request,response,new TokenParam().setValue("patientValue").setSystem("patientSystem"));
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(searchUrls.get(0)).contains("getActiveForPatient");
        assertThat(searchUrls.get(0)).contains("&identifier=");
        assertThat(searchUrls.get(0)).contains("patientValue");
        assertThat(searchUrls.get(0)).contains("patientSystem");
        assertThat(searchUrls.get(0)).contains("&author=");
        assertThat(searchUrls.get(0)).contains("authorArgument");
    }

    @Test
    public void getAllForPatient() {
        FHIRCarePlanResourceProvider carePlanProvider = Mockito.spy(new FHIRCarePlanResourceProvider(context, userContextResolver, clientFactory, kafkaEventProducer, replyEventProcessor, parameters));
        carePlanProvider.getAllForPatient(request,response,new TokenParam().setValue("patientValue").setSystem("patientSystem"));
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(searchUrls.get(0)).contains("getAllForPatient");
        assertThat(searchUrls.get(0)).contains("&identifier=");
        assertThat(searchUrls.get(0)).contains("patientValue");
        assertThat(searchUrls.get(0)).contains("patientSystem");
        assertThat(searchUrls.get(0)).contains("&author=");
        assertThat(searchUrls.get(0)).contains("authorArgument");
    }

    @Test
    public void search() {
        FHIRCarePlanResourceProvider carePlanProvider = Mockito.spy(new FHIRCarePlanResourceProvider(context, userContextResolver, clientFactory, kafkaEventProducer, replyEventProcessor, parameters));
        carePlanProvider.search(request,response,details,null,null,null,null,null,
                null,null,null,null,null,null,null);
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(searchUrls.get(0)).contains(exampleUrl());
    }

    @Test
    public void testConstructUrlFromRequestDetails(){
        FHIRCarePlanResourceProvider carePlanProvider = Mockito.spy(new FHIRCarePlanResourceProvider(context, userContextResolver, clientFactory, kafkaEventProducer, replyEventProcessor, parameters));
        String constructedString = carePlanProvider.constructUrlFromRequestDetails("example.com",details);
        assertThat(constructedString).isEqualTo("example.com" + exampleUrl());
    }

    private String exampleUrl(){
        return "/CarePlan?this=is&an-example";
    }
}