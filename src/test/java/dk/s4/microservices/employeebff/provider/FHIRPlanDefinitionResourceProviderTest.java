package dk.s4.microservices.employeebff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.IUntypedQuery;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.param.UriParam;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.employeebff.utils.OrganizationQueryParameter;
import dk.s4.microservices.messaging.ReplyEventProcessor;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.PlanDefinition;
import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FHIRPlanDefinitionResourceProviderTest {
    private static final String ORGIDENTIFIER = "orgIdentifier1234";
    private FHIRPlanDefinitionResourceProvider provider;
    private static String url = "URL";
    private static final String USERID = "user1234";
    private static final String USERSYSTEM = "system1234";
    private FhirContext context;
    private UserContextResolverInterface userContextResolver;
    private MyFhirClientFactory clientFactory;
    private KafkaEventProducer kafkaEventProducer;
    private ReplyEventProcessor replyEventProcessor;
    private IGenericClient client;
    private IUntypedQuery query;
    private IQuery iQuery;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private RequestDetails details;
    private List<String> clientUrls;
    private List<String> searchUrls;

    @Before
    public void setup() throws Exception {
        clientUrls = new ArrayList<>();
        searchUrls = new ArrayList<>();

        EnvironmentVariables variables = new EnvironmentVariables();
        TestUtils.readEnvironment("deploy.env", variables);
        TestUtils.readEnvironment("service.env", variables);

        context = mock(FhirContext.class);
        userContextResolver = mock(UserContextResolverInterface.class);
        clientFactory = mock(MyFhirClientFactory.class);
        kafkaEventProducer = mock(KafkaEventProducer.class);
        replyEventProcessor = mock(ReplyEventProcessor.class);
        client = mock(IGenericClient.class);
        query = mock(IUntypedQuery.class);
        iQuery = mock(IQuery.class);
        details = mock(RequestDetails.class);
        OrganizationQueryParameter parameters = mock(OrganizationQueryParameter.class);

        when(parameters.createFamilyFrom(anyObject(),anyObject())).thenReturn("authorArgument");
        when(details.getCompleteUrl()).thenReturn("example.com" + exampleUrl());

        when(clientFactory.getClientFromBaseUrl(anyObject(), anyObject())).then((invocation) -> {
            clientUrls.add((String) invocation.getArguments()[0]);
            return client;
        });
        when(client.search()).then((invocation) -> query);
        when(query.byUrl(anyObject())).then((invocation) -> {
            searchUrls.add((String) invocation.getArguments()[0]);
            return iQuery;
        });
        when (iQuery.returnBundle(anyObject())).then((invocation) -> iQuery);
        when (iQuery.execute()).then((invocation) -> null);
        when(userContextResolver.getFHIRUserOrganization(request)).thenReturn(new Identifier().setValue(ORGIDENTIFIER));

        provider = new FHIRPlanDefinitionResourceProvider(context,
                userContextResolver,
                clientFactory,
                kafkaEventProducer,
                replyEventProcessor,
                parameters);
    }

    @Test
    public void getMyPlanDefinitions() {
        provider.getMyPlanDefinitions(request,response);
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(clientUrls.get(0)).isEqualTo(ServiceVariables.PLANDEFINITION_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains("PlanDefinition?");
        assertThat(searchUrls.get(0)).contains("?_query=");
        assertThat(searchUrls.get(0)).contains("=getPlanDefinitionByOrganization&organization=");
        assertThat(searchUrls.get(0)).contains("authorArgument");
    }

    @Test
    public void getByIdentifier() {
        Bundle bundle = mock(Bundle.class);
        Bundle.BundleEntryComponent bundleEntryComponent = mock(Bundle.BundleEntryComponent.class);
        when(iQuery.execute()).thenReturn(bundle);
        when(bundle.getEntryFirstRep()).thenReturn(bundleEntryComponent);
        when(bundleEntryComponent.getResource()).thenReturn(new PlanDefinition());

        provider.getByIdentifier(request,response, new TokenParam().setValue(USERID).setSystem(USERSYSTEM));
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(clientUrls.get(0)).isEqualTo(ServiceVariables.PLANDEFINITION_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains("PlanDefinition?");
        assertThat(searchUrls.get(0)).contains("?_query=");
        assertThat(searchUrls.get(0)).contains("=getByIdentifier&identifier=");
        assertThat(searchUrls.get(0)).contains(USERSYSTEM + "|");
        assertThat(searchUrls.get(0)).contains("|" + USERID);
    }

    @Test
    public void getByUrl() throws UserContextResolverInterface.UserContextResolverException {
        UriParam uriParam = new UriParam(url);
        provider.getByUrl(request,response,uriParam);
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(clientUrls.get(0)).isEqualTo(ServiceVariables.PLANDEFINITION_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains("PlanDefinition?");
        assertThat(searchUrls.get(0)).contains("?_query=");
        assertThat(searchUrls.get(0)).contains("=getByUrl&url=");
        assertThat(searchUrls.get(0)).contains(url);
        assertThat(searchUrls.get(0)).contains("&author=");
        assertThat(searchUrls.get(0)).contains("authorArgument");
    }

    @Test
    public void search() {
        provider.search(request,response,details,null,null,null,null,
                null,null,null,null,null,null,null,null,null
                ,null,null,null,null);
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(searchUrls.get(0)).contains(exampleUrl());
        assertThat(searchUrls.get(0)).contains(ServiceVariables.PLANDEFINITION_SERVICE_URL.getValue());
    }

    private String exampleUrl(){
        return "/PlanDefinition?this=is&an-example";
    }

}