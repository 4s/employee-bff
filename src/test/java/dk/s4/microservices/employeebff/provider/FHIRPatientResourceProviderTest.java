package dk.s4.microservices.employeebff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.IUntypedQuery;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.employeebff.utils.OrganizationQueryParameter;
import dk.s4.microservices.messaging.ReplyEventProcessor;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.r4.model.Identifier;
import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FHIRPatientResourceProviderTest {
    private FHIRPatientResourceProvider provider;
    private String url;
    private static final String USERID = "user1234";
    private FhirContext context;
    private UserContextResolverInterface userContextResolver;
    private MyFhirClientFactory clientFactory;
    private KafkaEventProducer kafkaEventProducer;
    private ReplyEventProcessor replyEventProcessor;
    private IGenericClient client;
    private IUntypedQuery query;
    private IQuery iQuery;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private List<String> clientUrls;
    private List<String> searchUrls;

    @Before
    public void setup() throws Exception {
        clientUrls = new ArrayList<>();
        searchUrls = new ArrayList<>();

        EnvironmentVariables variables = new EnvironmentVariables();
        TestUtils.readEnvironment("deploy.env", variables);
        TestUtils.readEnvironment("service.env", variables);

        context = mock(FhirContext.class);
        userContextResolver = mock(UserContextResolverInterface.class);
        clientFactory = mock(MyFhirClientFactory.class);
        kafkaEventProducer = mock(KafkaEventProducer.class);
        replyEventProcessor = mock(ReplyEventProcessor.class);
        client = mock(IGenericClient.class);
        query = mock(IUntypedQuery.class);
        iQuery = mock(IQuery.class);
        OrganizationQueryParameter parameters = mock(OrganizationQueryParameter.class);

        when(parameters.createFamilyFrom(anyObject(),anyObject())).thenReturn("authorArgument");
        when(clientFactory.getClientFromBaseUrl(anyObject(), anyObject())).then((invocation) -> {
            clientUrls.add((String) invocation.getArguments()[0]);
            return client;
        });
        when(client.search()).then((invocation) -> query);
        when(query.byUrl(anyObject())).then((invocation) -> {
            searchUrls.add((String) invocation.getArguments()[0]);
            return iQuery;
        });
        when (iQuery.returnBundle(anyObject())).then((invocation) -> iQuery);
        when (iQuery.execute()).then((invocation) -> null);
        when(userContextResolver.getFHIRUserOrganization(request)).thenReturn(new Identifier().setValue(USERID));

        provider = new FHIRPatientResourceProvider(context,
                userContextResolver,
                clientFactory,
                kafkaEventProducer,
                replyEventProcessor,
                parameters);
    }

    @Test
    public void getMyPatientsYieldExpectedUrl() {
        provider.getMyPatients(request,response);
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(clientUrls.get(0)).isEqualTo(ServiceVariables.PATIENTCARE_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains("Patient");
        assertThat(searchUrls.get(0)).contains("getConnectedToOrganization");
        assertThat(searchUrls.get(0)).contains("&authorIdentifier=");
        assertThat(searchUrls.get(0)).contains("authorArgument");
    }
}
