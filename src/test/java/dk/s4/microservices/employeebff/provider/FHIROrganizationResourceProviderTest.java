package dk.s4.microservices.employeebff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.IUntypedQuery;
import ca.uhn.fhir.rest.param.TokenParam;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.r4.model.Identifier;
import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FHIROrganizationResourceProviderTest {
    private FHIROrganizationResourceProvider provider;
    private String url;
    private static final String USERID = "user1234";
    private static final String USERORG = "org1234";
    private FhirContext context;
    private UserContextResolverInterface userContextResolver;
    private MyFhirClientFactory clientFactory;
    private IGenericClient client;
    private IUntypedQuery query;
    private IQuery iQuery;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private List<String> clientUrls;
    private List<String> searchUrls;

    @Before
    public void setup() throws Exception {
        clientUrls = new ArrayList<>();
        searchUrls = new ArrayList<>();

        EnvironmentVariables variables = new EnvironmentVariables();
        TestUtils.readEnvironment("deploy.env", variables);
        TestUtils.readEnvironment("service.env", variables);

        context = mock(FhirContext.class);
        userContextResolver = mock(UserContextResolverInterface.class);
        clientFactory = mock(MyFhirClientFactory.class);
        client = mock(IGenericClient.class);
        query = mock(IUntypedQuery.class);
        iQuery = mock(IQuery.class);

        when(clientFactory.getClientFromBaseUrl(anyObject(), anyObject())).then((invocation) -> {
            clientUrls.add((String) invocation.getArguments()[0]);
            return client;
        });
        when(client.search()).then((invocation) -> query);
        when(query.byUrl(anyObject())).then((invocation) -> {
            searchUrls.add((String) invocation.getArguments()[0]);
            return iQuery;
        });
        when (iQuery.returnBundle(anyObject())).then((invocation) -> iQuery);
        when (iQuery.execute()).then((invocation) -> null);
        when(userContextResolver.getFHIRUserId(request)).thenReturn(new Identifier().setValue(USERID));
        when(userContextResolver.getFHIRUserOrganization(request)).thenReturn(new Identifier().setValue(USERORG));

        provider = new FHIROrganizationResourceProvider(context,
                userContextResolver,
                clientFactory);
    }

    @Test
    public void testGetMyOrganizationsYieldsExpectedUrl() {
        provider.getMyOrganization(request, response);
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(clientUrls.get(0)).isEqualTo(ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains(ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains(USERORG);
        assertThat(searchUrls.get(0)).contains(ServiceVariables.OFFICIAL_ORGANIZATION_IDENTIFIER_SYSTEM.getValue());
        assertThat(searchUrls.get(0)).contains("Organization");
        assertThat(searchUrls.get(0)).contains("getByIdentifier");
        assertThat(searchUrls.get(0)).contains("&identifier=");

    }

    @Test
    public void testGetByIdentifierYieldsExpectedUrl() {
        TokenParam tokenParam = new TokenParam().setValue("identifierValue").setSystem("identifierSystem");
        provider.getByIdentifier(mock(HttpServletRequest.class), mock(HttpServletResponse.class), tokenParam);
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(clientUrls.get(0)).isEqualTo(ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains(ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains("Organization");
        assertThat(searchUrls.get(0)).contains("getByIdentifier");
        assertThat(searchUrls.get(0)).contains("&identifier=" +
                "identifierSystem" +
                "|" +
                "identifierValue");
    }

    @Test
    public void testGetMyChildrenYieldsExpectedUrl() {
        provider.getMyChildren(request, mock(HttpServletResponse.class));
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(clientUrls.get(0)).isEqualTo(ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains(ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains("Organization");
        assertThat(searchUrls.get(0)).contains("?_query=getChildren"+
                        "&partOfIdentifier=" +
                ServiceVariables.OFFICIAL_ORGANIZATION_IDENTIFIER_SYSTEM.getValue() +
                "|" +
                USERORG);
    }

    @Test
    public void testGetChildrenYieldsExpectedUrl() {
        TokenParam tokenParam = new TokenParam().setValue("identifierValue").setSystem("identifierSystem");
        provider.getChildren(mock(HttpServletRequest.class), mock(HttpServletResponse.class), tokenParam);
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(clientUrls.get(0)).isEqualTo(ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains(ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains("Organization");
        assertThat(searchUrls.get(0)).contains("_query=getChildren&partOfIdentifier=" +
                "identifierSystem" +
                "|" +
                "identifierValue");
    }

    @Test
    public void testGetDescendantsYieldsExpectedUrl() {
        TokenParam tokenParam = new TokenParam().setValue("identifierValue").setSystem("identifierSystem");
        provider.getDescendants(mock(HttpServletRequest.class), mock(HttpServletResponse.class), tokenParam);
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(clientUrls.get(0)).isEqualTo(ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains(ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains("Organization");
        assertThat(searchUrls.get(0)).contains("_query=getDescendants&partOfIdentifier=" +
                "identifierSystem" +
                "|" +
                "identifierValue");
    }
}
