package dk.s4.microservices.employeebff.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.IUntypedQuery;
import ca.uhn.fhir.rest.param.TokenParam;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.messaging.ReplyEventProcessor;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.junit.*;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FHIRObservationDefinitionResourceProviderTest {

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();
    private static final String ORGIDENTIFIER = "orgIdentifier1234";

    private FhirContext context;
    private UserContextResolverInterface useContext;
    private MyFhirClientFactory clientFactory;
    private KafkaEventProducer kafkaEventProducer;
    private ReplyEventProcessor replyEventProcessor;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private List<String> searchUrls;
    private List<String> clientUrls;
    private IGenericClient client;
    private IUntypedQuery query;
    private IQuery iQuery;

    @BeforeClass
    public static void beforeClass() throws Exception {
        TestUtils.readEnvironment("service.env", environmentVariables);
    }
    
    @Before
    public void setup() throws IOException, UserContextResolverInterface.UserContextResolverException {
        clientUrls = new ArrayList<>();
        searchUrls = new ArrayList<>();

        EnvironmentVariables variables = new EnvironmentVariables();
        TestUtils.readEnvironment("deploy.env", variables);
        TestUtils.readEnvironment("service.env", variables);

        context = mock(FhirContext.class);
        useContext = mock(UserContextResolverInterface.class);
        clientFactory = mock(MyFhirClientFactory.class);
        kafkaEventProducer = mock(KafkaEventProducer.class);
        replyEventProcessor = mock(ReplyEventProcessor.class);

        client = mock(IGenericClient.class);
        query = mock(IUntypedQuery.class);
        iQuery = mock(IQuery.class);

        when(clientFactory.getClientFromBaseUrl(anyObject(), anyObject())).then((invocation) -> {
            clientUrls.add((String) invocation.getArguments()[0]);
            return client;
        });
        when(client.search()).then((invocation) -> query);
        when(query.byUrl(anyObject())).then((invocation) -> {
            searchUrls.add((String) invocation.getArguments()[0]);
            return iQuery;
        });
        when (iQuery.returnBundle(anyObject())).then((invocation) -> iQuery);
        when (iQuery.execute()).then((invocation) -> new Bundle());
        when(useContext.getFHIRUserOrganization(request)).thenReturn(new Identifier().setValue(ORGIDENTIFIER));
    }

    @Test
    public void getById() {
        FHIRObservationDefinitionResourceProvider observationDefinitionProvider 
                = Mockito.spy(new FHIRObservationDefinitionResourceProvider(context,useContext,clientFactory,kafkaEventProducer, replyEventProcessor));
        TokenParam id = new TokenParam().setSystem("theSystem").setValue("theID");
        observationDefinitionProvider.getById(request,response,id);
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(clientUrls.get(0)).isEqualTo(ServiceVariables.OUTCOMEDEFINITION_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains("/ObservationDefinition?_id=");
        assertThat(searchUrls.get(0)).contains("theID");
    }

    @Test
    public void getByIdentifier() {
        FHIRObservationDefinitionResourceProvider observationDefinitionProvider
                = Mockito.spy(new FHIRObservationDefinitionResourceProvider(context,useContext,clientFactory,kafkaEventProducer, replyEventProcessor));
        TokenParam identifier = new TokenParam().setSystem("theSystem").setValue("theIdentifier");
        observationDefinitionProvider.getByIdentifier(request,response,identifier);
        assertThat(clientUrls).hasSize(1);
        assertThat(searchUrls).hasSize(1);
        assertThat(clientUrls.get(0)).isEqualTo(ServiceVariables.OUTCOMEDEFINITION_SERVICE_URL.getValue());
        assertThat(searchUrls.get(0)).contains("/ObservationDefinition?_identifier=");
        assertThat(searchUrls.get(0)).contains("theIdentifier");
        assertThat(searchUrls.get(0)).contains("theSystem");
    }
}