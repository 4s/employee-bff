package dk.s4.microservices.employeebff;

import ca.uhn.fhir.rest.client.api.IClientInterceptor;
import ca.uhn.fhir.rest.client.api.IHttpRequest;
import ca.uhn.fhir.rest.client.api.IHttpResponse;

import java.io.IOException;

public class CorrelationIdInterceptor implements IClientInterceptor {

    public static final String HEADER_CORRELATION_ID = "CorrelationId";

    private String correlationId;

    public CorrelationIdInterceptor(String correlationId) {
        super();
        this.correlationId = correlationId;
    }

    @Override
    public void interceptRequest(IHttpRequest theRequest) {
        if(correlationId != null) theRequest.addHeader(HEADER_CORRELATION_ID, correlationId);
    }

    @Override
    public void interceptResponse(IHttpResponse theResponse) throws IOException {
        // nothing
    }
}
