package dk.s4.microservices.employeebff.utils;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.IUntypedQuery;
import ca.uhn.fhir.rest.param.TokenOrListParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import dk.s4.microservices.employeebff.ServiceVariables;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import org.hl7.fhir.instance.model.api.IBaseBundle;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Organization;
import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DescendantCacheTest {

    public static final int CACHE_LIMIT = 10;
    public static final String THE_CLIENT_BASE_URL = "theClientBaseUrl";
    private List<String> searchUrls;
    private Identifier descendantIdentifier;
    private Bundle returnBundle;
    private Identifier parentIdentifier;
    private DescendantCache cache;
    private MyFhirClientFactory clientFactory;
    private IGenericClient client;

    @Before
    public void setup() {
        EnvironmentVariables variables = new EnvironmentVariables();
        variables.set(ServiceVariables.DESCENDANT_CACHE_TIMEOUT_MINUTES.getKey(), "30");
        variables.set(ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getKey(), "theKey");
        variables.set(ServiceVariables.OFFICIAL_ORGANIZATION_IDENTIFIER_SYSTEM.getKey(), "descendantSystem");


        client = mock(IGenericClient.class);
        clientFactory = mock(MyFhirClientFactory.class);
        FhirContext fhirContext = mock(FhirContext.class);
        cache = new DescendantCache(fhirContext, THE_CLIENT_BASE_URL, CACHE_LIMIT, 30);
        descendantIdentifier = new Identifier().setSystem("descendantSystem").setValue("descendantValue");
        parentIdentifier = new Identifier().setValue("parentValue").setSystem("parentSystem");

        searchUrls = new ArrayList<>();

        Organization organization = new Organization();
        organization.addIdentifier(new Identifier().setSystem("descendantSystem").setValue("descendantValue"));

        returnBundle = new Bundle();
        returnBundle.addEntry().setResource(organization);


        IUntypedQuery<IBaseBundle> search = (IUntypedQuery<IBaseBundle>) mock(IUntypedQuery.class);
        IQuery query = mock(IQuery.class);

        when(query.returnBundle(Bundle.class)).thenReturn(query);

        when(search.byUrl(anyString())).thenAnswer((invocation) -> {
            String url = (String) invocation.getArguments()[0];
            searchUrls.add(url);
            return query;
        });
        when(query.execute()).thenReturn(returnBundle);

        when(client.search()).thenReturn(search);
        when(clientFactory.getClientFromBaseUrl(any(), any())).thenReturn(client);
    }

    @Test
    public void testQueryDescendantsForThatShouldNotUpdate() {
        TokenOrListParam tokenOrListParam = new TokenOrListParam();
        tokenOrListParam.add("someSystem", "someValue");
        cache.getCache().put(parentIdentifier, new TimeStampedTokenOrListParam(LocalDateTime.now(), tokenOrListParam));

        TokenOrListParam newTokenOrListParam = cache.queryDescendantsFor(parentIdentifier, clientFactory);

        assertThat(newTokenOrListParam.getValuesAsQueryTokens().get(0).getValue()).isEqualTo("someValue");
        assertThat(newTokenOrListParam.getValuesAsQueryTokens().get(0).getSystem()).isEqualTo("someSystem");
    }

    @Test
    public void testQueryDescendantsForThatShouldUpdate() {
        TokenOrListParam tokenOrListParam = new TokenOrListParam();
        tokenOrListParam.add("someSystem", "someValue");
        cache.getCache().put(parentIdentifier, new TimeStampedTokenOrListParam(LocalDateTime.now().minusMinutes(100), tokenOrListParam));

        TokenOrListParam newTokenOrListParam = cache.queryDescendantsFor(parentIdentifier, clientFactory);


        assertThat(newTokenOrListParam.getValuesAsQueryTokens().get(0).getValue()).isEqualTo("descendantValue");
        assertThat(newTokenOrListParam.getValuesAsQueryTokens().get(0).getSystem()).isEqualTo("descendantSystem");
    }

    @Test
    public void testQueryDescendantsForThatShouldUpdateEmptyCache() {
        TokenOrListParam newTokenOrListParam = cache.queryDescendantsFor(parentIdentifier, clientFactory);

        assertThat(newTokenOrListParam.getValuesAsQueryTokens().get(0).getValue()).isEqualTo("descendantValue");
        assertThat(newTokenOrListParam.getValuesAsQueryTokens().get(0).getSystem()).isEqualTo("descendantSystem");
    }

    @Test
    public void testExceedingCacheSize() {
        Identifier firstIn = new Identifier();
        cache.getCache().put(firstIn, new TimeStampedTokenOrListParam(LocalDateTime.now(), new TokenOrListParam()));

        for (int i = 0; i < CACHE_LIMIT; i++) {
            cache.getCache().put(new Identifier().setValue(String.valueOf(i)), new TimeStampedTokenOrListParam(LocalDateTime.now(), new TokenOrListParam()));
        }
        assertThat(cache.getCache().get(firstIn)).isEqualTo(null);
    }

    @Test
    public void testExceedingCacheSizeUseAccessOrder() {
        Identifier firstIn = new Identifier();
        cache.getCache().put(firstIn, new TimeStampedTokenOrListParam(LocalDateTime.now(), new TokenOrListParam()));

        for (int i = 0; i < CACHE_LIMIT / 2; i++) {
            cache.getCache().put(new Identifier().setValue(String.valueOf(i)), new TimeStampedTokenOrListParam(LocalDateTime.now(), new TokenOrListParam()));
        }

        cache.getCache().get(firstIn);

        for (int i = CACHE_LIMIT / 2; i < CACHE_LIMIT; i++) {
            cache.getCache().put(new Identifier().setValue(String.valueOf(i)), new TimeStampedTokenOrListParam(LocalDateTime.now(), new TokenOrListParam()));
        }

        assertThat(cache.getCache().get(firstIn)).isNotNull();
    }

    @Test
    public void constructTokenOrListOfDescendants() {
        TokenOrListParam orListParam = cache.constructTokenOrListOfDescendants(descendantIdentifier, clientFactory);

        assertThat(orListParam.getValuesAsQueryTokens().get(0).getValue()).isEqualTo("descendantValue");
        assertThat(orListParam.getValuesAsQueryTokens().get(0).getSystem()).isEqualTo("descendantSystem");
    }

    @Test
    public void retrieveDescendants() {
        cache.retrieveDescendants(descendantIdentifier, clientFactory);

        assertThat(searchUrls.get(0)).contains("descendantSystem");
        assertThat(searchUrls.get(0)).contains("descendantValue");
        assertThat(searchUrls.get(0)).contains("/Organization?_query=getDescendants&partOfIdentifier=");
        assertThat(searchUrls.get(0)).contains(ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue());
    }

    @Test
    public void addToList() {
        TokenOrListParam orListParam = new TokenOrListParam();
        cache.addToList(returnBundle.getEntryFirstRep(), orListParam);

        assertThat(orListParam.getValuesAsQueryTokens().get(0).getValue()).isEqualTo("descendantValue");
        assertThat(orListParam.getValuesAsQueryTokens().get(0).getSystem()).isEqualTo("descendantSystem");
    }

    @Test(expected = IllegalArgumentException.class)
    public void addToListWrongEntry() {
        TokenOrListParam orListParam = new TokenOrListParam();
        Bundle wrongEntryBundle = new Bundle();
        wrongEntryBundle.addEntry().setResource(new CarePlan().addIdentifier(new Identifier()));
        cache.addToList(wrongEntryBundle.getEntryFirstRep(), orListParam);

        assertThat(orListParam.getValuesAsQueryTokens().get(0).getValue()).isEqualTo("descendantValue");
        assertThat(orListParam.getValuesAsQueryTokens().get(0).getSystem()).isEqualTo("descendantSystem");
    }

    @Test
    public void getTokenParamFromIdentifier() {
        TokenParam param = cache.getTokenParamFromIdentifier(descendantIdentifier);

        assertThat(param.getValue()).isEqualTo("descendantValue");
        assertThat(param.getSystem()).isEqualTo("descendantSystem");
    }

    @Test(expected = InternalErrorException.class)
    public void testQueryDescendantsShouldThrowException() {
        when(client.search()).thenThrow(new ResourceNotFoundException("404 error"));

        TokenOrListParam tokenOrListParam = new TokenOrListParam();
        tokenOrListParam.add("someSystem", "someValue");
        cache.getCache().put(parentIdentifier, new TimeStampedTokenOrListParam(LocalDateTime.now().minusMinutes(100), tokenOrListParam));

        TokenOrListParam newTokenOrListParam = cache.queryDescendantsFor(parentIdentifier, clientFactory);
    }
}