package dk.s4.microservices.employeebff.utils;

import ca.uhn.fhir.rest.api.server.RequestDetails;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import dk.s4.microservices.microservicecommon.security.UserContextResolverInterface;
import org.hl7.fhir.r4.model.Identifier;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UtilsTest {

    private RequestDetails details;
    private OrganizationQueryParameter parameter;
    private MyFhirClientFactory clientFactory;
    private HttpServletRequest request;
    private UserContextResolverInterface userContextResolver;
    private Identifier identifier;

    @Before
    public void setup() throws UserContextResolverInterface.UserContextResolverException {
        details = mock(RequestDetails.class);
        parameter = mock(OrganizationQueryParameter.class);
        clientFactory = mock(MyFhirClientFactory.class);
        request = mock(HttpServletRequest.class);
        userContextResolver = mock(UserContextResolverInterface.class);
        identifier = mock(Identifier.class);
        when(userContextResolver.getFHIRUserOrganization(request)).thenReturn(identifier);


        when(parameter.createFamilyFrom(clientFactory,identifier)).thenReturn("test");
    }

    @Test
    public void testConstructUrlFromRequestDetails(){
        when(details.getCompleteUrl()).thenAnswer((invocation) -> "example.com/Resource?something=test");
        String constructedString = Utils.constructUrlFromRequestDetails("example.com","Resource",details);
        assertThat(constructedString).isEqualTo("example.com/Resource?something=test");
    }

    @Test
    public void testAddOrganizationParametersWithQuestionMark() throws UserContextResolverInterface.UserContextResolverException {
        String organizationParameters = Utils.addOrganizationParameters("outcome-service/Task?id=something","parameterName",request,parameter,clientFactory,userContextResolver);
        assertThat(organizationParameters).isEqualTo("outcome-service/Task?id=something&parameterName=test");
    }
    @Test
    public void testAddOrganizationParametersWithoutQuestionMark() throws UserContextResolverInterface.UserContextResolverException {
        String organizationParameters = Utils.addOrganizationParameters("outcome-service/Task","parameterName",request,parameter,clientFactory,userContextResolver);
        assertThat(organizationParameters).isEqualTo("outcome-service/Task?parameterName=test");
    }
}