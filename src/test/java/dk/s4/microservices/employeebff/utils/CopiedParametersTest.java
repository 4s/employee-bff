package dk.s4.microservices.employeebff.utils;

import ca.uhn.fhir.rest.api.server.RequestDetails;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.mockito.Mockito;
public class CopiedParametersTest {

    @Test
    public void returnsExpectedParameters() {
        CopiedParameters parameters = CopiedParameters.from(aMockWithUrl("http://abc.com/test?dav=abc&hej=dav"));
        Assertions.assertThat(parameters.get()).isEqualTo("?dav=abc&hej=dav");
    }

    @Test
    public void returnsNoParameters() {
        CopiedParameters parameters = CopiedParameters.from(aMockWithUrl("http://abc.com/test?"));
        Assertions.assertThat(parameters.get()).isEqualTo("?");
    }

    @Test
    public void returnsNoParametersNoQuestionMark() {
        CopiedParameters parameters = CopiedParameters.from(aMockWithUrl("http://abc.com/test"));
        Assertions.assertThat(parameters.get()).isEqualTo("");
    }

    private RequestDetails aMockWithUrl(String url) {
        RequestDetails requestDetails = Mockito.mock(RequestDetails.class);
        Mockito.when(requestDetails.getCompleteUrl()).thenReturn(url);
        return requestDetails;
    }
}