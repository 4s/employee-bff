package dk.s4.microservices.employeebff.utils;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.param.TokenOrListParam;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import org.hl7.fhir.r4.model.Identifier;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OrganizationQueryParameterTest {

    private OrganizationQueryParameter organizationParameters;
    private Identifier descendantIdentifier;
    private Identifier parentIdentifier;
    private TokenOrListParam tokenOrListParam;
    private MyFhirClientFactory clientFactory;

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Before
    public void setup() {

        tokenOrListParam = new TokenOrListParam();
        tokenOrListParam.add("systemOne","valueOne");
        tokenOrListParam.add("systemTwo","valueTwo");

        DescendantCache cache = mock(DescendantCache.class);
        FhirContext fhirContext = mock(FhirContext.class);
        when(cache.queryDescendantsFor(any(),anyObject())).thenReturn(tokenOrListParam);

        organizationParameters = new OrganizationQueryParameter(fhirContext,cache);

        descendantIdentifier = new Identifier().setSystem("descendantSystem").setValue("descendantValue");
        parentIdentifier = new Identifier().setValue("parentValue").setSystem("parentSystem");

    }

    @Test
    public void constructParameter() {
        String resultingString = organizationParameters.createFamilyFrom(clientFactory,parentIdentifier);

        assertThat(resultingString).isEqualTo("systemOne|valueOne,systemTwo|valueTwo,parentSystem|parentValue");
    }

    @Test
    public void convertToString() {
        String resultingString = organizationParameters.convertToString(parentIdentifier,tokenOrListParam);

        assertThat(resultingString).isEqualTo("systemOne|valueOne,systemTwo|valueTwo,parentSystem|parentValue");
    }


    @Test
    public void convertToStringEmptyTokenOrListParam() {
        TokenOrListParam orListParam = new TokenOrListParam();
        String resultingString = organizationParameters.convertToString(descendantIdentifier,orListParam);

        assertThat(resultingString).isEqualTo("descendantSystem|descendantValue");
    }

    @Test
    public void testGetTokenParamFromIdentifierNoSystem(){
        EnvironmentVariables variables = new EnvironmentVariables();
        variables.set("OFFICIAL_ORGANIZATION_IDENTIFIER_SYSTEM","orgSystem");

        Identifier identifier = new Identifier().setValue("aValue");
        String string = organizationParameters.getTokenParamFromIdentifier(identifier).getValueAsQueryToken(mock(FhirContext.class));

        assertThat(string).isEqualTo("orgSystem|aValue");
    }
    @Test
    public void testGetTokenParamFromIdentifier(){
        EnvironmentVariables variables = new EnvironmentVariables();
        variables.set("OFFICIAL_ORGANIZATION_IDENTIFIER_SYSTEM","orgSystem");

        Identifier identifier = new Identifier().setValue("aValue").setSystem("someOtherSystem");
        String string = organizationParameters.getTokenParamFromIdentifier(identifier).getValueAsQueryToken(mock(FhirContext.class));

        assertThat(string).isEqualTo("someOtherSystem|aValue");
    }

}