# Employee Backend-for-Frontend:

| Summary                          |                                                                                                                     |
|----------------------------------|---------------------------------------------------------------------------------------------------------------------|
| Service name (computer friendly) | employee-bff                                                                                                        |
| Maturity level                   | Concept [ ] Prototype [ ] Demonstration [x] Product [ ]                                                                                                         |
| Used externally                  | Yes, used by the GMK web app for healthcare professionals                                                                   |
| Database                         | no                                                                                                                  |
| RESTful dependencies             | organizational-service, outcomedefinition-service, outcome-service, patient-care-service, plandefinintion-service   |
| Messaging dependencies           | Kafka                                                                                                               |
| Other dependencies               | In DIAS configuration: User context service (lookup user context info from session ID)                                                            |
| License                          | Apache 2.0                                                                                                          |

This service handles the business logic for healthcare professionals and is a socalled "Backend-for-Frontend" service. It is implemented as a FHIR RESTful server.

The employee-bff is a microservice intented for use in a microservice based system as the one described at 
[GMK - Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/README.md). The service is a 
_backend-for-frontend service_ as described in [GMK Backend Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/HFS.md) 
and provides a RESTful FHIR API for the web application for healthcare professionals.

## Documentation

  - [GMK - Architecture](https://bitbucket.org/4s/gmk-gravide-med-komplikationer/src/master/README.md)
  - Design, FHIR API and messaging API documentation: See [docs](docs//) folder.
  - Use of these FHIR resources and how they relate to other resources in the system on the pages about the [FHIR data model](https://issuetracker4s.atlassian.net/wiki/spaces/FDM/overview) 
  - Public classes and methods are documented with JavaDoc 
  - More on [DIAS](https://www.rm.dk/om-os/digitalisering/digitale-losninger-pa-region-midtjyllands-udviklingsplatform/).

## Prerequisites
To build the project you need:

  * OpenJDK 11+
  * Maven 3.1.1+
  * Docker 20+

The project depends on git submodule (ms-scripts). To initialize this you need to execute:

```
git submodule update --init
git submodule foreach git pull origin master
``` 

If you are running things using docker-compose (see below) you also need to have a docker network by the name of 
`opentele3net` setup:

`docker network create opentele3net` 

## Building the service
The simplest way to build the service is to use the `buildall.sh` script:

`/scripts/buildall.sh`

This will build the service as a WAR file and add this to a Jetty based docker image.

## Running the service
You can run the service just using ``docker run ...`` or you can run the service using the provided docker-compose files. 
You may of course also run the service in container-orchestration environments like Kubernetes or Docker Swarm.

### Running the service with docker-compose
With docker-compose you can run the service like this:

`docker-compose up`

This will start the service with a Jetty running on port 8080. To run the service with a different port mapping use:

`docker-compose -f docker-compose.yml -f docker-compose.portmapping.yml up`

## Logging
The service uses Logback for logging. At runtime Logback log levels can be configured via environment variables (see
below). 

_Notice:_ The service may output person sensitive information at log level `TRACE`. If log level is set to `DEBUG` or 
above no person sensitive information should be output from the service.

If you are running via docker-compose and you want use the fluentd logging driver (if you for instance have setup 
fluentd, elasticsearch and Kibana) instead of the standard json-file you may start the service like this:

```
docker-compose -f docker-compose.yml -f docker-compose.fluentd-logging.yml up
```

## Monitoring
On http the service exposes a health endpoint on the path `/info`. This will return `200 ok` if the service is basically
in a healthy state, meaning that the web service is up and running inside Jetty.

On http the service exposes Prometheus metrics on the path `/metrics`. 

If you set environment variable `DIAS_AUDIT_ENABLED` to `true` and point `DIAS_AUDIT_URL` to a [DIAS Auditlog compatible
service](https://telemed-test.rm.dk/leverandorportal/leverance/_logning/) the employee-bff will log audit events to such a service. 

## Environment variables

The files `service.env` and `deploy.env` define all environment variables used by this microservice. `deploy.env` defines
service URLs and log levels that depend on your particular deployment.

*Notice: In production settings database related environment (username/password etc.) will differ from the one
indicated below.* 

| Environment variable          | Required | Description                                                                                                                     |
|-------------------------------|----------|----------------------------------------------------------------------
| SERVICE_NAME                  | y | Computer friendly name of this service. 
| FHIR_VERSION                  | y | Version of HL7 FHIR supported by this service
| SERVER_BASE_URL               | n | The full address of the service as seen by clients. If the service for example is behind an API gateway, the service base URL must be set.
| *Log levels:*                 ||
| LOG_LEVEL                     | y | Set the log level for 3rd party libraries
| LOG_LEVEL_DK_S4               | y | Set the log level for this service and the 4S libraries used by this service
| *Authentication*              | |
| ENABLE_AUTHENTICATION         | n | Enable getting user context information (from http session header or otherwise)
| ENABLE_DIAS_AUTHENTICATION    | n | Enable DIAS specific authentication functionality
| USER_CONTEXT_SERVICE_URL      | n | URL of user context side care service. Required if ENABLE_DIAS_AUTHENTICATION=true
| *Messaging related:*          ||
| ENABLE_KAFKA                  | y | If `true Kafka communication is enabled for this service
| KAFKA_BOOTSTRAP_SERVER        | y | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_KEY_SERIALIZER          | y | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_VALUE_SERIALIZER        | y | Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_KEY_DESERIALIZER        | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_VALUE_DESERIALIZER      | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_ENABLE_AUTO_COMMIT      | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_AUTO_COMMIT_INTERVAL_MS | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_SESSION_TIMEOUT_MS      | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_GROUP_ID                | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_ACKS                    | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| KAFKA_RETRIES                 | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| ENABLE_HEALTH                 | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| HEALTH_FILE_PATH              | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| HEALTH_INTERVAL_MS            | n| Required if ENABLE_KAFKA=true, see https://bitbucket.org/4s/messaging/src/master/
| *Coding Systems*              ||
| OFFICIAL_ORGANIZATION_IDENTIFIER_SYSTEM           |  | The system string used in logical references to Organization FHIR resources
| OFFICIAL_CAREPLAN_IDENTIFIER_FIRST_SYSTEM         |  | The system string used in the first logical references to CarePlan FHIR resources
| OFFICIAL_CAREPLAN_IDENTIFIER_SECOND_SYSTEM        |  | The system string used in the second logical references to Careplan FHIR resources
| OFFICIAL_CAREPLAN_PATIENT_SYSTEM                  |  | The system string used in logical references to Patient resources in the CarePlan FHIR resource
| OFFICIAL_PRACTITIONERROLE_IDENTIFIER_SYSTEM       |  | The system string used in logical references to PractitionerRole FHIR resources
| OFFICIAL_QUESTIONNAIRE_IDENTIFIER_SYSTEM          |  | The system string used in logical references to Questionnaire FHIR resources
| OFFICIAL_OBSERVATIONDEFINITION_IDENTIFIER_SYSTEM  |  | The system string used in logical references to ObservationDefinition FHIR resources
| OFFICIAL_PLANDEFINITION_IDENTIFIER_SYSTEM         |  | The system string used in logical references to PlanDefinition FHIR resources
| PRACTITIONER_USERNAME_IDENTIFIER_SYSTEM           |  | The system string used in username identifiers in Practitioner FHIR resources
| *Service URLs*                ||
| ORGANIZATIONAL_SERVICE_URL     | y | The URL of the Organizational service:    https://bitbucket.org/4s/organizational-service/
| PLANDEFINITION_SERVICE_URL     | y | The URL of the PlanDefinition service:    https://bitbucket.org/4s/plandefinition-service/
| PATIENTCARE_SERVICE_URL        | y | The URL of the PatientCare service:       https://bitbucket.org/4s/patientcare-service/
| OUTCOMEDEFINITION_SERVICE_URL  | y | The URL of the OutcomeDefinition service:  https://bitbucket.org/4s/outcomedefinition-service/
| OUTCOME_SERVICE_URL            | y | The URL of the Outcome service:  https://bitbucket.org/4s/outcome-service/
| *Service URLs*                ||
| DIAS_AUDIT_URL                | n| URL of DIAS AuditLog service
| DIAS_AUDIT_ENABLED            | n| Set to `true` to enable audit logging to DIAS AuditLog Service 

## Test
To do a basic test of the service you may run

```
mvn clean verify
```

### Smoketest without authentication
Set `ENABLE_AUTHENTICATION=false` and `ENABLE_DIAS_AUTHENTICATION=false` and run the service like this:

`docker-compose -f docker-compose.yml -f docker-compose.portmapping.yml up`

``
curl http://localhost:8092/info/
``

The expected output of the curl command is a status code 200. The output should contain:

```
{"status":"all good"}
```

Smoketest the FHIR server part of the service by running:

`curl http://localhost:8092/baseR4/metadata`

This should outout a FHIR CapabilityStatement beginning starting with lines like the following:

``
{
  "resourceType": "CapabilityStatement",
  ...
``

### Smoketest with authentication
To smoketest the service with [DIAS](https://www.rm.dk/om-os/digitalisering/digitale-losninger-pa-region-midtjyllands-udviklingsplatform/) 
authentication turned on run Set `ENABLE_AUTHENTICATION=true` and `ENABLE_DIAS_AUTHENTICATION=true` and set 
`USER_CONTEXT_SERVICE_URL` to point to a sidecar service with a [getsessiondata](https://telemed-test.rm.dk/leverandorportal/services/sidevogne/getsessiondata/)
endpoint. After his you may test the service as described above, just adding a valid session id to the header of your
calls:

`curl -H "SESSION: my-valid-session-id" http://localhost:8092/baseR4/metadata`
